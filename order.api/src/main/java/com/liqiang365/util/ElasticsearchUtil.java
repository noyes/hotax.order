package com.liqiang365.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.liqiang365.util.valid.ValidUtil;
import hotax.http.util.HttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.core.config.ConfigurationSource;
import org.apache.logging.log4j.core.config.Configurator;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@Slf4j
public class ElasticsearchUtil {


    public static final String ALL_COURSE_NAME = "hotax_course";

    public final static String HOST = ApiConstants.ELASTICSEARCH_HOST;

    public static final String SEARCH_FILED = "keyword";

    public final static int PORT = 9300; // http请求的端口是9200，客户端是9300

    public static final int REST_PORT = 9200;

    private static TransportClient client = null;

    /**
     * 获取客户端连接信息
     *
     * @return void
     * @throws UnknownHostException
     * @Title: getConnect
     * @author sunt
     * @date 2017年11月23日
     */
    @SuppressWarnings({"resource", "unchecked"})
    public static void getConnect() throws UnknownHostException {
        if (client == null) {
            client = new PreBuiltTransportClient(Settings.EMPTY)
                    .addTransportAddresses(new TransportAddress(InetAddress.getByName(HOST), PORT));
        }
    }

    /**
     * 关闭连接
     *
     * @return void
     * @Title: closeConnect
     * @author sunt
     * @date 2017年11月23日
     */
    public static void closeConnect() {
        if (null != client) {
            //client.close();
        }
    }

    public static void createByRest(String indexName) {
        try {
            String json = "{\n" +
                    "    \"settings\" : {\n" +
                    "        \"index\" : {\n" +
                    "            \"analysis.analyzer.default.type\": \"ik_max_word\"\n" +
                    "        }\n" +
                    "    }\n" +
                    "}";
            Map<String, String> header = new HashMap<>();
            header.put("Content-Type", "application/json");
            HttpResponse response = HttpUtils.doPut("http://" + HOST + ":" + REST_PORT, "/" + indexName, "", header, null, json);
            log.info("搜索状态：" + response.getStatusLine());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void delete(String indexName) throws UnknownHostException {
        getConnect();
        client.admin().indices().prepareDelete(indexName).execute();
        closeConnect();
    }

    public static void addAllIndex(String indexName, List<Map<String, Object>> objectMap) throws IOException {
        getConnect();
        for (Map<String, Object> m : objectMap) {
            IndexResponse response = client.prepareIndex(indexName, "fulltext", m.get("id") + "")
                    .setSource(m)
                    .get();
            log.info("索引名称:" + response.getIndex() + "\n类型:" + response.getType() + "\n文档ID:"
                    + response.getId() + "\n当前实例状态:" + response.status());
        }
        closeConnect();
    }

    public static IndexResponse addIndex(String indexName, String id, Map<String, Object> objectMap) throws IOException {
        getConnect();
        IndexResponse response = client.prepareIndex(indexName, "fulltext", id)
                .setSource(objectMap)
                .get();
        log.info("索引名称:" + response.getIndex() + "\n类型:" + response.getType() + "\n文档ID:"
                + response.getId() + "\n当前实例状态:" + response.status());
        closeConnect();
        return response;
    }

    public static DeleteResponse deleteIndex(String indexName, String id) throws UnknownHostException, ExecutionException, InterruptedException {
        getConnect();
        DeleteResponse response = client.prepareDelete(indexName, "fulltext", id).execute().get();
        log.info("索引名称:" + response.getIndex() + "\n类型:" + response.getType() + "\n文档ID:"
                + response.getId() + "\n当前实例状态:" + response.status());
        closeConnect();
        return response;
    }

    public static Map<String, Object> query(String indexName, String keyword, Integer startPage, Integer pageSize, Integer type) throws Exception {
        Map<String, Object> resultMap = new HashMap<>();
        String typeStr = "";
        if (type == 1) {
            typeStr = "and";
        } else {
            typeStr = "or";
        }
        List<Map<String, Object>> result = new ArrayList<>();
        String path = "/" + indexName + "/fulltext/_search";
        String param = "{\n" +
                "\"from\" : " + startPage + ", \"size\" : " + pageSize + "," +
                "  \"query\": {\n" +
                "    \"simple_query_string\" : {\n" +
                "        \"query\": \"" + keyword + "\",\n" +
                "        \"fields\": [\"name\",\"suitable\",\"objectives\",\"keyword\",\"wareName\",\"teacherName\",\"chargeStr\",\"classifyCode\"],\n" +
                "        \"default_operator\": \"" + typeStr + "\"\n" +
                "    }\n" +
                "  }\n" +
                "}";
        Map<String, String> header = new HashMap<>();
        header.put("Content-Type", "application/json");
        HttpResponse response = HttpUtils.doPost("http://" + HOST + ":" + REST_PORT, path, "", header, null, param);
        int httpcode = response.getStatusLine().getStatusCode();
        if (httpcode == 200) {
            String responsejson = EntityUtils.toString(response.getEntity());
            if (ValidUtil.isNotEmpty(responsejson)) {
                Map<String, Object> resobj = JSON.parseObject(responsejson, Map.class);
                JSONObject jsonObject = (JSONObject) resobj.get("hits");
                if (ValidUtil.isNotEmpty(jsonObject)) {
                    Integer total = Integer.parseInt(jsonObject.get("total") + "");
                    resultMap.put("total", total);
                    if (total > 0) {
                        JSONArray hits = (JSONArray) jsonObject.get("hits");
                        for (Object jo : hits) {
                            String ojson = jo.toString();
                            Map<String, Object> stringObjectMap = JSON.parseObject(ojson, Map.class);
                            String sourcejson = stringObjectMap.get("_source") + "";
                            Map<String, Object> entityMap = JSON.parseObject(sourcejson, Map.class);
                            result.add(entityMap);
                        }
                    }

                }

            } else {
                return null;
            }
        }
        resultMap.put("list", result);
        return resultMap;
    }


    public static List<Map<String, Object>> queryForSpeaking(String indexName, String keyword, Integer startPage, Integer pageSize) throws Exception {
        List<Map<String, Object>> result = new ArrayList<>();
        String path = "/" + indexName + "/fulltext/_search";
        String param = "{\n" +
                "\"from\" : " + startPage + ", \"size\" : " + pageSize + "," +
                "  \"query\": {\n" +
                "    \"simple_query_string\" : {\n" +
                "        \"query\": \"" + keyword + "\",\n" +
                "        \"fields\": [\"keyword\",\"typearea\"],\n" +
                "        \"default_operator\": \"and\"\n" +
                "    }\n" +
                "  }\n" +
                "}";
        Map<String, String> header = new HashMap<>();
        header.put("Content-Type", "application/json");
        HttpResponse response = HttpUtils.doPost("http://" + HOST + ":" + REST_PORT, path, "", header, null, param);
        int httpcode = response.getStatusLine().getStatusCode();
        if (httpcode == 200) {
            String responsejson = EntityUtils.toString(response.getEntity());
            if (ValidUtil.isNotEmpty(responsejson)) {
                Map<String, Object> resobj = JSON.parseObject(responsejson, Map.class);
                JSONObject jsonObject = (JSONObject) resobj.get("hits");
                if (ValidUtil.isNotEmpty(jsonObject)) {
                    int total = Integer.parseInt(jsonObject.get("total") + "");
                    if (total > 0) {
                        JSONArray hits = (JSONArray) jsonObject.get("hits");
                        for (Object jo : hits) {
                            String ojson = jo.toString();
                            Map<String, Object> stringObjectMap = JSON.parseObject(ojson, Map.class);
                            String sourcejson = stringObjectMap.get("_source") + "";
                            Map<String, Object> entityMap = JSON.parseObject(sourcejson, Map.class);
                            result.add(entityMap);
                        }
                    }

                }

            } else {
                return null;
            }
        }
        return result;
    }


    public static void main(String[] args) throws Exception {
        File file = new File("D:/log4j2.xml");
        BufferedInputStream in = new BufferedInputStream(new FileInputStream(file));
        final ConfigurationSource source = new ConfigurationSource(in);
        Configurator.initialize(null, source);
        //String keyword, String indexName, String sortFieldName, SortOrder sortOrder, Integer startPage, Integer pageSize
        long start = System.currentTimeMillis();

        //System.out.println(query(YOUTH_INDEX_NAME, "初中 适应 生物 三年级", 11, 10));
        ;
//        SearchResponse response = search("初中 适应 生物 三年级", YOUTH_INDEX_NAME, null, null, 0, 10);
//        System.out.println("总记录数:" + response.getHits().getTotalHits());
//        for (SearchHit searchHit : response.getHits()) {
//            System.out.println(searchHit.getSourceAsString());
//        }
        long end = System.currentTimeMillis();
        System.out.println((end - start) / 1000.00);
//        getConnect();
//        SearchResponse response = client.prepareSearch(YOUTH_INDEX_NAME)
//                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
//                .setQuery(QueryBuilders.termQuery("keyword", "初中 一年级")).get();
//        System.out.println("总记录数:" + response.getHits().getTotalHits());
//        for (SearchHit searchHit : response.getHits()) {
//            System.out.println(searchHit.getSourceAsString());
//        }
//        closeConnect();
    }

}
