package com.liqiang365.util;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.HashMap;
import java.util.Map;

@Data
@Configuration
@PropertySource(value = {"file:config/hotax-${spring.profiles.active}.properties", "config/hotax-${spring.profiles.active}.properties", "file:config/hotax.properties", "config/hotax.properties"}, ignoreResourceNotFound = true, encoding = "UTF-8")
public class ApiConstants {

    public static final String SUCCESS_CODE = "100";
    public static final String SUCCESS_MSG = "请求成功";

    /**
     * session中存放用户信息的key值
     */
    public static final String SESSION_USER_INFO = "userInfo";
    public static final String SESSION_USER_PERMISSION = "userPermission";


    //视频上传进度(只作为admin 单点上传使用)
    public static Map<String, Double> UPLOAD_PROGRESS = new HashMap<String, Double>();

    //搜索服务地址
    public static String ELASTICSEARCH_HOST;

    /**
     * 融云请求的 key
     */
    public static String RONG_YUN_KEY;

    /**
     * 融云请求的秘钥
     */
    public static String RONG_YUN_SECRET;

    /**
     * 系统发送消息用户名
     */
    public static String IM_SYSTEM_USERID = "liqiang365";

}
