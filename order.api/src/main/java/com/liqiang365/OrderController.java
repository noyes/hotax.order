//package com.liqiang365;
//
//import com.liqiang365.service.OrderService;
//import hotax.core.ReturnBean;
//import hotax.core.ReturnMap;
//import io.swagger.annotations.ApiOperation;
//import io.swagger.annotations.ApiParam;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.MediaType;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//@RestController("/order")
//public class OrderController {
//
//    @Autowired
//    private OrderService orderService;
//
//    @ApiOperation(value = "下单", response = ReturnBean.class, produces = "application/json;charset=UTF-8")
//    @PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//    public ReturnBean updateStudyCodeLicense(@ApiParam(value = "用户id") @RequestParam String uid,
//                                             @ApiParam(value = "下单类型") @RequestParam String type) {
//        try {
//            return orderService.save(uid, type);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return ReturnMap.returnFail("");
//    }
//
//}
