package com.liqiang365.controller;

import com.liqiang365.service.TestService;
import hotax.core.ReturnBean;
import hotax.core.ReturnMap;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName: TestController
 * @Describe :
 * @Author: yancong
 * @Date 2020/7/16 15:18
 */
@RestController
@RequestMapping("test")
@Slf4j
@Api(tags = {"测试控制器"})
public class TestController {

    @Autowired
    private TestService testService;

    @ApiOperation(value = "测试异步任务1", response = ReturnBean.class, produces = "application/json;charset=UTF-8")
    @PostMapping(value = "/asyncTask1", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ReturnBean testAsyncTask(@ApiParam(value = "计算结果") @RequestParam int num) {
        try {
            log.info("==========1");
            testService.testAsync1(num);
            log.info("==========4");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ReturnMap.returnFail("");
    }


}
