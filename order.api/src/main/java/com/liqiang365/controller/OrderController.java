package com.liqiang365.controller;

import com.liqiang365.service.OrderService;
import hotax.core.ReturnBean;
import hotax.core.ReturnMap;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName: OrderController
 * @Describe :
 * @Author: yancong
 * @Date 2020/7/8 16:12
 */
@RestController("")
@RequestMapping("order")
@Api(tags = {"下单控制器"})
public class OrderController {

    @Autowired
    private OrderService orderService;

    @ApiOperation(value = "下单", response = ReturnBean.class, produces = "application/json;charset=UTF-8")
    @PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ReturnBean saveOrder(@ApiParam(value = "用户id") @RequestParam String uid,
                                             @ApiParam(value = "下单类型") @RequestParam String type) {
        try {
            return orderService.save(uid, type);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ReturnMap.returnFail("");
    }

}
