package com.liqiang365;

import com.liqiang365.service.mq.MqMessageReceiveHandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author 严聪
 * @description
 * @date 2019/10/25 14:34
 */
@MapperScan("com.liqiang365.dao.*")
@SpringBootApplication(scanBasePackages = {"hotax", "com.liqiang365"})
@EnableAsync(proxyTargetClass = true)
public class ApiBootStrap implements CommandLineRunner {

    @Autowired
    private MqMessageReceiveHandle mqMessageReceiveHandle;

    public static void main(String[] args) {
        SpringApplication.run(ApiBootStrap.class, args);
    }

    @Override
    public void run(String... args) {
        //注册消费者
        //mqMessageReceiveHandle.regListenMessage(Arrays.asList(MqMessageTagEnum.ANCHOR_SALE_COURSE.getValue()));
    }
}