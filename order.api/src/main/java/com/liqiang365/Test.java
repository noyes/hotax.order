package com.liqiang365;

import hotax.core.ReturnBean;
import liqiang365.util.http.HttpClient;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Test {


    public static void main(String[] args) throws IOException, InterruptedException {
        File file = new File("d://data//data.txt");
        FileReader reader = new FileReader(file);
        BufferedReader buff = new BufferedReader(reader);
        List<String> list = new ArrayList<>();
        String str;
        try {
            while ((str = buff.readLine()) != null) {
                list.add(str);
            }
            System.out.println("____________________________list的大小 =" + list.size());
            for (String orderId : list) {
                List<NameValuePair> params = new ArrayList<>();
                params.add(new BasicNameValuePair("pwd", "Liqiang365_initNationalDayRedPack_data"));
                params.add(new BasicNameValuePair("orderId", orderId));
                String post = HttpClient.post("https://api.liqiang365.com/test/toFamousTeacherCourse", params);
                params.clear();
                System.out.println("__________________________orderId" + orderId);
                System.out.println("________________________post=" + post);
                Thread.sleep(2000);
            }

        } finally {
            buff.close();
            reader.close();
        }


    }
}
