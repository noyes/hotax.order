package com.liqiang365.service;

import hotax.core.ReturnBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
/**
*@ClassName: OrderService
*@Describe :
*@Author: yancong
*@Date 2020/7/14 11:05
*/
@Service
@Slf4j
public class OrderService {

    public ReturnBean save(String uid, String type) {
        return OrderFactory.saveOrder(type, uid);
    }
}
