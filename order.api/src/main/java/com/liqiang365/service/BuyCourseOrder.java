package com.liqiang365.service;

import com.liqiang365.model.em.OrderTypeEnum;
import hotax.core.ReturnBean;
import hotax.core.ReturnMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class BuyCourseOrder implements OrderHandle, InitializingBean {
    @Override
    public ReturnBean save(Object... boj) {
        log.info("___________________________BuyCourseOrder  save  ");
        return ReturnMap.returnSucc("BuyCourseOrder save  success");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        OrderFactory.regOrderFactory(OrderTypeEnum.FAMOUSTEACHERCOURSE.getValue(), this);
    }
}
