package com.liqiang365.service;

import com.liqiang365.model.em.OrderTypeEnum;
import hotax.core.ReturnBean;
import hotax.core.ReturnMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * @ClassName: LevelUpOrder
 * @Describe :
 * @Author: yancong
 * @Date 2020/7/8 14:56
 */
@Component
@Slf4j
public class LevelUpOrder implements OrderHandle, InitializingBean {
    @Override
    public ReturnBean save(Object... boj) {
        log.info("___________________________LevelUpOrder  save  ");
        return ReturnMap.returnSucc("LevelUpOrder save  success");
    }


    @Override
    public void afterPropertiesSet() throws Exception {
        OrderFactory.regOrderFactory(OrderTypeEnum.UPLEVEL.getValue(), this);
    }
}
