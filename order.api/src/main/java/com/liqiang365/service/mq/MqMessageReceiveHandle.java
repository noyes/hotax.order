package com.liqiang365.service.mq;

import com.liqiang365.exception.ServiceException;
import com.liqiang365.util.ContextUtils;
import com.liqiang365.util.ValidUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Properties;

/**
 * @author 严聪
 */
@Service
@Slf4j
public class MqMessageReceiveHandle {

    public void regListenMessage(String consumerTag) {


    }

    private void handMessage(String msgTag, String message) throws ServiceException {
        log.info("handMessage       msgTag={},  message={}", msgTag, message);
        MqMessageReceiveInterface impl = (MqMessageReceiveInterface) ContextUtils.getBean(msgTag);
        if (ValidUtil.isEmpty(impl)) {
            log.error("没有对应的消息处理对象 消息分发失败   message=" + message);
        }
        impl.handMessage(message);

    }

}
