package com.liqiang365.service.mq;


import com.liqiang365.exception.ServiceException;

import javax.annotation.PreDestroy;

/**
 * @author admin
 */

public interface MqMessageReceiveInterface {


    /**
     * 处理消息
     *
     * @param
     * @return
     */
    Object handMessage(String tag) throws ServiceException;

    /**
     * 销毁消费者
     */
    @PreDestroy
    void consumerDestory();

}
