package com.liqiang365.service.mq;

import com.aliyun.openservices.ons.api.Consumer;
import com.liqiang365.util.valid.ValidUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author admin
 */

@Component("testMessage")
@Slf4j
public class TestMessageReceiveImpl implements MqMessageReceiveInterface {

    private Consumer consumer;

    @Override
    public Object handMessage(String message) {
        log.info("handMessage    Message={}", message);
        return null;
    }

    @Override
    public void consumerDestory() {
        if (ValidUtil.isNotEmpty(consumer)) {
            consumer.shutdown();
        }
    }
}
