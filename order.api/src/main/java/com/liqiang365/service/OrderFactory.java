package com.liqiang365.service;

import com.liqiang365.util.ValidUtil;
import hotax.core.ReturnBean;
import hotax.core.ReturnMap;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName: OrderFactory
 * @Describe :
 * @Author: yancong
 * @Date 2020/7/8 15:11
 */
public class OrderFactory {
    private static Map<String, OrderHandle> handleMap = new HashMap<>();

    private OrderFactory() {

    }

    public static void regOrderFactory(String type, OrderHandle orderHandle) {
        handleMap.put(type, orderHandle);
    }

    public static ReturnBean saveOrder(String type, Object... objects) {
        OrderHandle orderHandle = handleMap.get(type);
        if (ValidUtil.isEmpty(orderHandle)) {
            return ReturnMap.returnFail("下单失败，没找到对应的下单处理类");
        }
        return orderHandle.save(objects);
    }

}
