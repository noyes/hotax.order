#!/bin/sh
#
# Copyright (c) 2017-2018 博睿思远网络科技
#

cd "$(dirname "$0")"

PRG="$0"


PRGDIR=`dirname "$PRG"`

RootDir=`cd "$PRGDIR/." ; pwd`

API_HOME=`cd "$PRGDIR/.." ; pwd`

echo "$API_HOME"
export API_HOME

JAVA_OPTS_SCRIPT="-XX:+HeapDumpOnOutOfMemoryError -Djava.awt.headless=true"


JAVA_OPTS="$JAVA_OPTS -XX:+UseG1GC"

JAVA_OPTS="$JAVA_OPTS -XX:G1RSetUpdatingPauseTimePercent=5"

JAVA_OPTS="$JAVA_OPTS -XX:+PrintGCDetails"
JAVA_OPTS="$JAVA_OPTS -XX:+PrintGCDateStamps"
JAVA_OPTS="$JAVA_OPTS -XX:+PrintHeapAtGC"
JAVA_OPTS="$JAVA_OPTS -XX:+PrintTenuringDistribution"
JAVA_OPTS="$JAVA_OPTS -XX:+PrintGCApplicationStoppedTime"
JAVA_OPTS="$JAVA_OPTS -XX:+PrintPromotionFailure"
#JAVA_OPTS="$JAVA_OPTS -XX:PrintFLSStatistics=1"
#JAVA_OPTS="$JAVA_OPTS -Xloggc:/var/log/moquette/gc.log"
JAVA_OPTS="$JAVA_OPTS -XX:+UseGCLogFileRotation"
JAVA_OPTS="$JAVA_OPTS -XX:NumberOfGCLogFiles=10"
JAVA_OPTS="$JAVA_OPTS -XX:GCLogFileSize=10M"


java -server $JAVA_OPTS $JAVA_OPTS_SCRIPT -jar im-api-1.0.jar &
echo $! > $RootDir/pid