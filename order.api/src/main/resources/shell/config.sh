#!/bin/sh
#
# Copyright (c) 2017-2018 liqiang365
#

ip=$1
profiles=$2

dataCenterId=""
woekId=""
workdir="$(cd $(dirname $0); pwd)"


main()
{
	echo "我的ip $ip"
	echo "我的pro $profiles"
	echo "我的工作目录 $workdir"
	case $ip in
		"172.18.113.155")
			dataCenterId=0
			woekId=0
		;;
		"172.18.115.64")
			dataCenterId=0
			woekId=1
		;;
		"10.100.101.24")
			dataCenterId=0
			woekId=0
		;;
		"10.100.102.24")
			dataCenterId=0
			woekId=1
		;;
		"10.100.103.24")
			dataCenterId=0
			woekId=2
		;;
		"192.168.16.117")
			dataCenterId=8
			woekId=8
		;;
	esac

	dataCenterId="DatacenterId=${dataCenterId}"
	woekId="WorkerId=${woekId}"
	
	echo "ip:$ip"
	echo "dataid:$dataCenterId"
	echo "workid:$woekId"
	
	if [ "${profiles}" = "ope" ];then
		workdir="${workdir}/config/application-test.properties"
	elif [ "${profiles}" = "prod" ];then
		workdir="${workdir}/config/application-prod.properties"
	fi
	sed -i "s/^.*DatacenterId.*$/${dataCenterId}/" "$workdir"
	sed -i "s/^.*WorkerId.*$/$woekId/" "$workdir"
}

main




