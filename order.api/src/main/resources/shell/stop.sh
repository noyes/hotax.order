#!/bin/sh
#
# Copyright (c) 2017-2018 博睿思远网络科技
#
cd "$(dirname "$0")"

tpid=`cat pid | awk '{print $1}'`  
tpid=`ps -aef | grep $tpid | awk '{print $2}' |grep $tpid`  
if [ ${tpid} ]; then  
        kill -9 $tpid
        echo "系统已经停止!"
else
	echo "应用不存在或者没有运行!"
fi