package com.noyes.mediator;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Lists;
import com.google.common.collect.Multiset;
import org.apache.ibatis.logging.stdout.StdOutImpl;

import java.sql.SQLOutput;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @ClassName: Test
 * @Describe :
 * @Author: yancong
 * @Date 2020/7/14 14:17
 */
public class Test {
    public static void main(String[] args) {
        User user1 = new User("张三");
        User user2 = new User("李四");
        user1.sendMessage("你好 我是张三");
        user2.sendMessage("你好 我是李四");
        Integer num = 8;
        System.out.println(Optional.fromNullable(num).or(1));
        System.out.println(Optional.fromNullable(num).isPresent());

        List<String> list = Arrays.asList("1", "2");
        List<String> list2 = Arrays.asList("3", "4");



    }

}
