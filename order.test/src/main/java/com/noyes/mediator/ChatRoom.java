package com.noyes.mediator;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @ClassName: ChatRoom
 * @Describe :
 * @Author: yancong
 * @Date 2020/7/14 14:09
 */
public class ChatRoom {
    public static void showMessage(User user, String message) {
        System.out.println(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.now()) + " " + user.getUserName() + " showMessage: " + message);
    }
}
