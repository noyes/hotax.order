package com.noyes.visitor;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * @ClassName: Test
 * @Describe :
 * @Author: yancong
 * @Date 2020/7/14 11:17
 */
public class Test {
    /**
     * 生成红包最小值 1分
     */
    private static final int MIN_MONEY = 1;

    /**
     * 生成红包最大值 200人民币
     */
    private static final int MAX_MONEY = 100000000;

    /**
     * 小于最小值
     */
    private static final int LESS = -1;
    /**
     * 大于最大值
     */
    private static final int MORE = -2;

    /**
     * 正常值
     */
    private static final int OK = 1;

    /**
     * 最大的红包是平均值的 TIMES 倍，防止某一次分配红包较大
     */
    private static final double TIMES = 2.3F;

    private int recursiveCount = 0;

    public List<Integer> splitRedPacket(int money, int count) {
        List<Integer> moneys = new LinkedList<>();

        //金额检查，如果最大红包 * 个数 < 总金额；则需要调大最小红包 MAX_MONEY
        if (MAX_MONEY * count <= money) {
            System.err.println("请调大最小红包金额 MAX_MONEY=[" + MAX_MONEY + "]");
            return moneys;
        }


        //计算出最大红包
        int max = (int) ((money / count) * TIMES);
        max = max > MAX_MONEY ? MAX_MONEY : max;

        for (int i = 0; i < count; i++) {
            //随机获取红包
            int redPacket = randomRedPacket(money, MIN_MONEY, max, count - i);
            moneys.add(redPacket);
            //总金额每次减少
            money -= redPacket;
        }

        return moneys;
    }

    private int randomRedPacket(int totalMoney, int minMoney, int maxMoney, int count) {
        //只有一个红包直接返回
        if (count == 1) {
            return totalMoney;
        }

        if (minMoney == maxMoney) {
            return minMoney;
        }

        //如果最大金额大于了剩余金额 则用剩余金额 因为这个 money 每分配一次都会减小
        maxMoney = maxMoney > totalMoney ? totalMoney : maxMoney;

        //在 minMoney到maxMoney 生成一个随机红包
        int redPacket = (int) (Math.random() * (maxMoney - minMoney) + minMoney);

        int lastMoney = totalMoney - redPacket;

        int status = checkMoney(lastMoney, count - 1);

        //正常金额
        if (OK == status) {
            return redPacket;
        }

        //如果生成的金额不合法 则递归重新生成
        if (LESS == status) {
            recursiveCount++;
            System.out.println("recursiveCount==" + recursiveCount);
            return randomRedPacket(totalMoney, minMoney, redPacket, count);
        }

        if (MORE == status) {
            recursiveCount++;
            System.out.println("recursiveCount===" + recursiveCount);
            return randomRedPacket(totalMoney, redPacket, maxMoney, count);
        }

        return redPacket;
    }

    /**
     * 校验剩余的金额的平均值是否在 最小值和最大值这个范围内
     *
     * @param lastMoney
     * @param count
     * @return
     */
    private int checkMoney(int lastMoney, int count) {
        double avg = lastMoney / count;
        if (avg < MIN_MONEY) {
            return LESS;
        }

        if (avg > MAX_MONEY) {
            return MORE;
        }

        return OK;
    }

    public static void main(String[] args) {
//        Test redPacket = new Test();
//        List<Integer> redPackets = redPacket.splitRedPacket(1000, 100);
//        Collections.shuffle(redPackets);
//        System.out.println(redPackets);
//
//        int sum = 0;
//        for (Integer red : redPackets) {
//            sum += red;
//        }
//        System.out.println(sum);

//        System.out.println("_____________________" + DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(LocalDateTime.now()));
//
//        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//        LocalDateTime time = LocalDateTime.parse("2020-07-24 11:42:29", dateTimeFormatter);
//        LocalDateTime time2 = LocalDateTime.now();
//        System.out.println(time.compareTo(time2));
//
//        long time3 =time.toEpochSecond(ZoneOffset.of("+8"));
//        long time4 = time.toInstant(ZoneOffset.of("+8")).toEpochMilli();
//        long time5 = DateUtils.parseDate("2020-07-24 11:42:29").getTimeInMillis();
//
//        System.out.println("_____________________________________" + time3);
//        System.out.println("_____________________________________" + time4);
//        System.out.println("_____________________________________" + time5);

        String str = "Hello world";
        HashMap<Character, Integer> map = new HashMap<>(str.length());
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            map.compute(c, (k, v) -> {
                if (v == null) {
                    v = 1;
                } else {
                    v += 1;
                }
                return v;
            });
        }

        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (map.containsKey(c)) {
                map.put(c, map.get(c) + 1);
            } else {
                map.put(c, 1);
            }
        }


        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            Integer integer = map.putIfAbsent(c, 0);
            if (integer == null) {
                map.put(c, 1);
            } else {
                map.put(c, integer.intValue() + 1);
            }
        }


        System.out.println("________________________________________3> " + map);
        String s1 = "abc";
        String s2 = s1;
        System.out.println("---------------1------------" + (s1 == s2));

        String s3 = "abc";
        String s4 = new String("abc");
        System.out.println("---------------2------------" + (s3 == s4));

        String s5 = new String("abc");
        String s6 = new String("abc");
        System.out.println("---------------3------------" + (s5 == s6));

        String s7 = "a";
        String s8 = "b";
        String s9 = s7 + s8;
        String s10 = "ab";
        System.out.println("---------------4------------" + (s9 == s10));

        String s11 = "a" + "b";
        String s12 = "ab";
        System.out.println("---------------5------------" + (s11 == s12));

        String s13 = "abc";
        String s14 = new String("abc").intern();
        System.out.println("---------------6------------" + (s13 == s14));


        String s15 = s13.intern();
        String s16 = new String("abc");
        System.out.println("---------------7------------" + (s15 == s16));
    }


}
