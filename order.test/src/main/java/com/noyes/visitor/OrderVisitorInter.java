package com.noyes.visitor;
/**
*@ClassName: OrderVisitorInter
*@Describe :       
*@Author: yancong
*@Date 2020/7/14 11:41
*/
public class OrderVisitorInter implements OrderVisitorInterface {
    /**
     * @param courseOrderNotify
     * @ClassName: OrderVisitor
     * @Describe :
     * @Author: yancong
     * @Date 2020/7/14 11:15
     */
    @Override
    public void notifyOrder(CourseOrderNotifyInterface courseOrderNotify) {
        System.out.println("==================================>             courseOrderNotify           start");
    }

    /**
     * @param levelUpOrderNotify
     * @ClassName: OrderVisitor
     * @Describe :
     * @Author: yancong
     * @Date 2020/7/14 11:15
     */
    @Override
    public void notifyOrder(LevelUpOrderNotifyInterface levelUpOrderNotify) {
        System.out.println("==================================>             levelUpOrderNotify           start");
    }
}
