package com.noyes.visitor;

/**
 * @ClassName: OrderVisitor
 * @Describe :
 * @Author: yancong
 * @Date 2020/7/14 11:12
 */
public interface OrderVisitorInterface {

    /**
     * @ClassName: OrderVisitor
     * @Describe :
     * @Author: yancong
     * @Date 2020/7/14 11:15
     */
    void notifyOrder(CourseOrderNotifyInterface courseOrderNotify);

    /**
     * @ClassName: OrderVisitor
     * @Describe :
     * @Author: yancong
     * @Date 2020/7/14 11:15
     */
    void notifyOrder(LevelUpOrderNotifyInterface levelUpOrderNotify);
}
