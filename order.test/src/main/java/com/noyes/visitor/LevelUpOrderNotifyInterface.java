package com.noyes.visitor;
/**
*@ClassName: LevelUpOrderNotify
*@Describe :       
*@Author: yancong
*@Date 2020/7/14 11:13
*/
public class LevelUpOrderNotifyInterface implements OrderNotifyInterface {
    /**
     * @param orderVisitorInterface
     * @ClassName: notifyOrder
     * @Describe :
     * @Author: yancong
     * @Date 2020/7/14 11:12
     */
    @Override
    public void notifyOrder(OrderVisitorInterface orderVisitorInterface) {
        orderVisitorInterface.notifyOrder(this);
    }
}
