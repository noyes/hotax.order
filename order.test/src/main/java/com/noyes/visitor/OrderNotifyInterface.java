package com.noyes.visitor;

/**
 * @ClassName: OrderNotify
 * @Describe :
 * @Author: yancong
 * @Date 2020/7/14 11:06
 */
public interface OrderNotifyInterface {
    /**
     * @ClassName: notifyOrder
     * @Describe :
     * @Author: yancong
     * @Date 2020/7/14 11:12
     */
     void notifyOrder(OrderVisitorInterface orderVisitorInterface);
}
