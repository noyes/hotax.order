package com.noyes.template;

/**
 * @ClassName: Test
 * @Describe :
 * @Author: yancong
 * @Date 2020/7/14 11:44
 */
public class Test {
    public static void main(String[] args) {
        NotifyOrder notifyOrder = new LevelUpOrderNotifyOrder();
        notifyOrder.handNotifyOrder();
    }
}
