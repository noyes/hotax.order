package com.noyes.template;

public abstract class NotifyOrder {
    abstract void notifyOrder();

    abstract void addPayRecord();

    public final void handNotifyOrder() {
        notifyOrder();
        addPayRecord();
    }
}
