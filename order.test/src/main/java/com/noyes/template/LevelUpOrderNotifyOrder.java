package com.noyes.template;

public class LevelUpOrderNotifyOrder extends NotifyOrder {
    @Override
    void notifyOrder() {
        System.out.println("==========================>     LevelUpOrderNotify     notifyOrder start");
    }

    @Override
    void addPayRecord() {
        System.out.println("==========================>    LevelUpOrderNotify     addPayRecord     start");
    }
}
