package com.noyes.observer;

public class EndOrderObServer extends ObServer {
    public EndOrderObServer(Subject subject) {
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    public void update() {
        System.out.println("======================> EndOrderObServer start <=============");
    }
}
