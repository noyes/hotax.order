package com.noyes.observer;

import java.util.ArrayList;
import java.util.List;

public class Subject {

    private List<ObServer> obServers = new ArrayList<>();

    private int state;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public void attach(ObServer obServer){
        obServers.add(obServer);
    }

    public void  notifyAllObServer(){
        for (ObServer obServer : obServers) {
            obServer.update();
        }
    }

}
