package com.noyes.observer;

public abstract class ObServer {
    protected Subject subject;
    public abstract void update();
}
