package com.noyes.observer;

public class SaveOrderObServer extends ObServer {
    public SaveOrderObServer(Subject subject) {
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    public void update() {
        System.out.println("======================> SaveOrderObServer start <=============");
    }
}
