package com.noyes;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ZookeeperConfig {

    /**
     * 连接地址
     */
    private static final String CONNECTION_ADDRESS = "10.1.1.97:2181,10.1.1.97:2182,10.1.1.97:2183,10.1.1.97:2184";

    /**
     * 会话超时时间
     */
    private static final int SESSION_TIMEOUT = 60 * 1000;

    /**
     * 连接超时时间
     */
    private static final int CONNECTION_TIMEOUT = 180 * 1000;

    public static CuratorFramework client = null;

    public CuratorFramework getZookeeperConnection() {
        return client;
    }

    public ZookeeperConfig() {

    }

    public static CuratorFramework getZookeeperConnection(int i) {
        if (null == client) {
            ZookeeperConfig zookeeperConfig = new ZookeeperConfig();
            zookeeperConfig.init();
        }
        return client;
    }

    @Bean
    public void init() {
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 10);
        client = CuratorFrameworkFactory.builder()
                .connectString(CONNECTION_ADDRESS).connectionTimeoutMs(CONNECTION_TIMEOUT)
                .sessionTimeoutMs(SESSION_TIMEOUT)
                .retryPolicy(retryPolicy).namespace("super")
                .build();

        client.start();
    }


}
