package com.noyes;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("test")
@Slf4j
public class TestController {

    @Autowired
    private ZookeeperPuduct zookeeperPuduct;

    @Autowired
    private ZookeeperConsumer zookeeperConsumer;

    @GetMapping("/createData")
    public String createData(@RequestParam String key, @RequestParam String value) throws Exception {
        zookeeperPuduct.createData(key, value);
        return "ok";
    }

    @GetMapping("/updateData")
    public String updateData(@RequestParam String key, @RequestParam String value) throws Exception {
        zookeeperPuduct.updateData(key, value);
        return "ok";
    }

    @GetMapping("/deleteData")
    public String deleteData(@RequestParam String key, @RequestParam String value) throws Exception {
        zookeeperPuduct.deleteData(key);
        return "ok";
    }

    @GetMapping("/getData")
    public String getData(@RequestParam String key) throws Exception {
        return zookeeperConsumer.getData(key);
    }
}
