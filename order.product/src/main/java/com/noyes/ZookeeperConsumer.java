package com.noyes;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.api.GetDataBuilder;
import org.apache.curator.framework.recipes.cache.PathChildrenCache;
import org.apache.curator.framework.recipes.cache.TreeCache;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.data.Stat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class ZookeeperConsumer {
    @Autowired
    private ZookeeperConfig zookeeperConfig;

    public String getData(String elementName) throws Exception {
        CuratorFramework zookeeperConnection = zookeeperConfig.getZookeeperConnection();
        Stat stat = zookeeperConnection.checkExists().forPath(elementName);
        if (null == stat) {
            zookeeperConnection.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT).forPath(elementName);
        }
        zookeeperConnection.getData().usingWatcher((Watcher) event -> {
            Watcher.Event.EventType type = event.getType();
            if (type.equals(Watcher.Event.EventType.NodeDeleted)) {
                log.info("____________________elementName delete,path={}", event.getPath());
            }
            if (type.equals(Watcher.Event.EventType.NodeDataChanged)) {
                log.info("____________________elementName change,path={}", event.getPath());
            }
        });
        byte[] bytes = zookeeperConnection.getData().forPath(elementName);
        return new String(bytes);

    }
}