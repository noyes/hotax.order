package com.noyes;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.ZooKeeper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * @ClassName: StartApplication
 * @Describe :
 * @Author: yancong
 * @Date 2020/11/19 11:56
 */
@SpringBootApplication(scanBasePackages = {"com.noyes"})
public class StartApplication {

    public static void main(String[] args) {
        SpringApplication.run(StartApplication.class);


    }

}
