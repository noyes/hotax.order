package com.noyes;

import lombok.extern.slf4j.Slf4j;
import org.apache.curator.framework.CuratorFramework;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.data.ACL;
import org.apache.zookeeper.data.Stat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@Slf4j
public class ZookeeperPuduct {

    @Autowired
    private ZookeeperConfig zookeeperConfig;

    public void createData(String elementName, String value) throws Exception {
        ArrayList<ACL> acl = ZooDefs.Ids.OPEN_ACL_UNSAFE;
        CuratorFramework zookeeperConnection = zookeeperConfig.getZookeeperConnection();
        zookeeperConnection.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT).withACL(acl).forPath(elementName, value.getBytes());
    }

    public void updateData(String elementName, String value) throws Exception {
        Stat stat = new Stat();
        CuratorFramework zookeeperConnection = zookeeperConfig.getZookeeperConnection();
        zookeeperConnection.getData().storingStatIn(stat).forPath(elementName);
        Stat forPath2 = zookeeperConnection.setData().withVersion(stat.getVersion()).forPath(elementName, value.getBytes());
    }

    public void deleteData(String elementName) throws Exception {
        CuratorFramework zookeeperConnection = zookeeperConfig.getZookeeperConnection();
        zookeeperConnection.delete().forPath(elementName);
    }

}
