package com.liqiang365.dao;

import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 吴华龙
 * @classname InsertBatchMapper
 * @description 批量插入mapper
 * @date 2019/11/22 17:25
 */
@tk.mybatis.mapper.annotation.RegisterMapper
public interface InsertBatchMapper<T> {
    @InsertProvider(type = com.liqiang365.dao.InsertBatchProvider.class, method = "dynamicSQL")
    int insertBatch(@Param("list") List<T> list);
}