package com.liqiang365.util.date;


import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateBet {
    /**
     * 计算两个日期之间相差的天数
     *
     * @param smdate 较小的时间
     * @param bdate  较大的时间
     * @return 相差天数
     * @throws ParseException
     */
    public static int daysBetween(Date smdate, Date bdate)
            throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        smdate = sdf.parse(sdf.format(smdate));
        bdate = sdf.parse(sdf.format(bdate));
        Calendar cal = Calendar.getInstance();
        cal.setTime(smdate);
        long time1 = cal.getTimeInMillis();
        cal.setTime(bdate);
        long time2 = cal.getTimeInMillis();
        long between_days = (time2 - time1) / (1000 * 3600 * 24);

        return Integer.parseInt(String.valueOf(between_days));
    }

    /**
     * 字符串的日期格式的计算
     */
    public static int daysBetween(String smdate, String bdate)
            throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(sdf.parse(smdate));
        long time1 = cal.getTimeInMillis();
        cal.setTime(sdf.parse(bdate));
        long time2 = cal.getTimeInMillis();
        long between_days = (time2 - time1) / (1000 * 3600 * 24);

        return Integer.parseInt(String.valueOf(between_days));
    }

    /**
     * 当前日期减一天
     *
     * @throws ParseException
     */
    public static Date dateJianOne(String date) {
        SimpleDateFormat sdf = null;
        Date date1 = null;
        try {
            sdf = new SimpleDateFormat("yyyy-MM-dd");
            date1 = sdf.parse(date);
        } catch (ParseException e) {

            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date1);
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }

    /**
     * 当前日期减i天
     *
     * @throws ParseException
     */
    public static String dateJianI(String date, int i) {
        SimpleDateFormat sdf = null;
        Date date1 = null;
        try {
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date1 = sdf.parse(date);
        } catch (ParseException e) {

            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date1);
        cal.add(Calendar.DATE, -i);
        return sdf.format(cal.getTime());
    }

    /**
     * 当前日期加一年
     *
     * @param date 时间
     * @throws ParseException
     */
    public static Date dateAddOneYear(String date) {
        SimpleDateFormat sdf = null;
        Date date1 = null;
        try {
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date1 = sdf.parse(date);
        } catch (ParseException e) {

            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date1);

        /* modified by 严聪 at 2017.6.21 for 增加 2017年7月18号以后vip升级，vip有效期增加一年功能 begin */
        long time = System.currentTimeMillis();
        long updateTime = 1500307200000L;
        if (time >= updateTime) {
            cal.add(Calendar.YEAR, 1);
        } else {
            cal.add(Calendar.YEAR, 2);
        }
        /* modified by 严聪 at  2017.6.21 for 增加 2017年7月18号以后vip升级，vip有效期增加一年功能 end */
        return cal.getTime();
    }


    /**
     * 当前日期加一年
     *
     * @param userDate 时间
     * @throws ParseException
     */
    public static String dateAddDate(String userDate, int addDate) {
        SimpleDateFormat sdf = null;
        Date date = null;
        try {
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date = sdf.parse(userDate);
        } catch (ParseException e) {

            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, addDate);
        /* modified by 严聪 at  2017.6.21 for 增加 2017年7月18号以后vip升级，vip有效期增加一年功能 end */
        return sdf.format(cal.getTime());
    }

    /**
     * 当前日期加指定秒
     *
     * @param userDate 时间
     * @throws ParseException
     */
    public static Date dateAddSecond(String userDate, int second) {
        SimpleDateFormat sdf = null;
        Date date = null;
        try {
            sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date = sdf.parse(userDate);
        } catch (ParseException e) {

            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.SECOND, second);
        return cal.getTime();
    }

    /**
     * 增加一年和几个月
     *
     * @param date  当前时间或需要增加年限的时间
     * @param month 增加的月数
     * @return
     */
    public static Date dateAddOneYearAndMonth(String date, int month) {
        SimpleDateFormat sdf = null;
        Date date1 = null;
        try {
            sdf = new SimpleDateFormat("yyyy-MM-dd");
            date1 = sdf.parse(date);
        } catch (ParseException e) {

            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date1);
        cal.add(Calendar.YEAR, 1);
        cal.add(Calendar.MONTH, month);
        return cal.getTime();
    }

    public static Date dateAddMonth(String date, int month) {
        SimpleDateFormat sdf = null;
        Date date1 = null;
        try {
            sdf = new SimpleDateFormat("yyyy-MM-dd");
            date1 = sdf.parse(date);
        } catch (ParseException e) {

            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date1);
        cal.add(Calendar.MONTH, month);
        return cal.getTime();
    }

    public static Date dateAddDay(String date, int day) {
        SimpleDateFormat sdf = null;
        Date date1 = null;
        try {
            sdf = new SimpleDateFormat("yyyy-MM-dd");
            date1 = sdf.parse(date);
        } catch (ParseException e) {

            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date1);
        cal.add(Calendar.DATE, day);
        return cal.getTime();
    }

    /**
     * 字符串的日期格式只差(单位分钟)
     */
    public static long between_day_forSeconds(String smdate, String bdate)
            throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.setTime(sdf.parse(smdate));
        long time1 = cal.getTimeInMillis();
        cal.setTime(sdf.parse(bdate));
        long time2 = cal.getTimeInMillis();
        long between_seconds = (time2 - time1) / (1000 * 60);
        return between_seconds;
    }

    /**
     * 字符串的日期格式只差(单位秒)
     */
    public static long subSeconds(String smdate, String bdate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(sdf.parse(smdate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long time1 = cal.getTimeInMillis();
        try {
            cal.setTime(sdf.parse(bdate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long time2 = cal.getTimeInMillis();
        long between_seconds = (time2 - time1) / (1000);
        return between_seconds;
    }

    /**
     * 增加一年和几个月
     *
     * @param month 增加的月数
     * @return
     */
    public static Date nowAddMonth(int month) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MONTH, month);
        Date endtime = cal.getTime();
        return endtime;
    }

    public static void main(String[] args) {
        Long a = 23L;
        DecimalFormat df = new DecimalFormat("0.00");


        System.out.println(df.format(a.doubleValue() / 60));
        ;
//        try {
//            System.out.println(DateBet.between_day_forSeconds("2016-2-29 03:01:30", "2016-2-29 03:08:20"));
//        } catch (ParseException e) {
//
//            e.printStackTrace();
//        }

    }


}
