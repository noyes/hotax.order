package com.liqiang365.util.redis;

/**
 * @author 严聪
 * @description
 * @return
 * @date 2019/9/10 15:58
 */
public class RedisKey {

    public static String INIT_ORDER_SYSNC_DATA_FINISH = "initOrderSysncDataFinish";
    public static String INIT_USER_SYSNC_DATA_FINISH = "initUserSysncDataFinish";
    public static String INIT_MONEY_INCOME_SYSNC_DATA_FINISH = "initMoneyIncomeSysncDataFinish";
    public static String INIT_TEACHER_SYSNC_DATA_FINISH = "initTeacherSysncDataFinish";
    public static String INTI_TEACHER_COURSE_REF_FINISH = "initTeahcerCourseRefDataFinish";
    public static final String KV_USER_TOKEN = "kv_user_token_";//apptoken

    public static final String KV_HERO_VIDEO_CLICE = "kv_hero_video_alice_"; //90后英雄视频上传授权序号

    public static final String START_LIVE_UID = "start_live_uid_";

    public static final String ANCHOR_ANCHOR_UID = "anchor_anchor_uid_";

    public static final String REMOVE_CHAT_ROOM_USER = "remove_chat_room_user_";

    public static final String REMOVE_ROOM_MANAGE_SET_UID = "remove_room_manage_set_uid_";

    public static final String USER_ANCHORREPORT_UID = "user_anchorreport_uid_";

    public static final String BARRAGE_MESSAGE_UID = "barrage_message_uid_";

    /**
     * 直播间真实最大人数
     */
    public static final String LIVE_COUNT_HOT_LIVE_ID = "live_count_hot_live_id_";
    /**
     * 直播间机器人数
     */
    public static final String LIVE_COUNT_HOT_LIVE_ROBOT_ID = "live_count_hot_live_robot_id_";

    /**
     * 直播间公告集合
     */
    public static final String KV_ANNOUNCEMENT_LIVE_MAP = "kv_announcement_live_map";

    /**
     * 主播异常集合
     */
    public static final String KV_LIVE_EXCEPTION_MAP = "kv_live_exception_map";

    /**
     * 直播机器人增加数据存储集合
     */
    public static final String ROBOT_ADD_NUM_LIVE_ID = "robot_add_num_live_id_";

    /**
     * key 主播uid
     * value LiveExceptionRecord 主播异常记录实体类
     */
    public static final String KV_LIVE_ANCHOR_UID_DATA = "kv_live_anchor_uid_data";
    /**
     * 增加直播搜索历史记录 锁
     */
    public static final String LIVE_SEARCH_HISTORY_ADD_LOCK_KEY = "live_search_history_add_lock_key";

    public static final String KV_LIVE_ANCHORTAGADD_UID = "kv_live_anchortagadd_uid_";

    public static final String KV_LIVE_USER_ADD = "kv_live_user_add";

    public static final String KV_LIVE_ROBOT_USER = "liveRobotUser";

    public static final String KV_LIVE_ADD_ROBOT = "kv_live_add_robot";

    public static final String KV_LIVE_ADD_BARRAGE = "kv_live_add_barrage";

    public static final String KV_LIVE_ADD_COMMENT = "kv_live_add_comment";

    public static final String KV_LIVE_ROBOT_CONFIG_KEY = "kv_live_robot_config_key_";

    /**
     * 直播实时观看人数
     */
    public static final String KV_LIVE_REAL_TIME_COUNT_LOOKER = "kv_live_real_time_count_looker_";


    public static final String postToShuMeiRequestId = "postToShuMeiRequestId_";

    /**
     * 掉线直播
     */
    public static final String KV_OFFLINE_LIVE_DATA = "kv_offline_live_data";

    public static final String KV_LIVE_CHECK_WHITE_LIST = "kv_live_check_white_list";

    public static final String KV_LIVE_USER_WARM_COUNT_LIVE_ID = "kv_live_user_warm_count_live_id";

    public static final String KV_LIVE_CONFIG_WARM_COUNT = "kv_live_config_warm_count";

    public static final String KV_ROOT_TREND = "kv_root_trend_";

    /**
     * by su 2020-01-30 14:45:29
     */
    // 缓存用户信息
    public static final String KV_CACHE_USER_INFO = "kv_cache_user_info";

    public static final String KV_BROWSING_AUCHOR_LOCK_UID = "kv_browsing_auchor_lock_uid_";

    public static final String KV_CACHE = "kv_cache_";

    /**
     * 缓存用户的礼物记录
     */
    public static final String KV_LIVE_GIFT_RECORD = "kv_live_gift_record_";


    /**
     * 缓存主播用户信息
     */
    public static final String KV_CACHE_USER_ANCHOR = "kv_cache_user_anchor_";

    /**
     * 进入房间错误次数
     */
    public static final String KV_JOIN_ROOM_ERROR_UID = "kv_join_live_error_";

    /**
     * 可创建加密直播白名单
     */
    public static final String KV_ENCRYPTED_WHITELIST = "kv_encrypted_whitelist";

    /**
     * 修改房间号锁
     */
    public static final String KV_UPDATE_ROOM_NO_LOCK = "kv_update_room_no_lock";

    public static final String KV_WEIXIN_OPENID_FOR_LIVE_SHARE = "kv_weixin_openid_for_live_share";

    public static final String KV_IP_BLACKLIST_KEY = "kv_ip_blacklist_key_";

    public static final String KV_IP_WHITELIST_KEY = "kv_ip_whitelist_key_";

    public static final String KV_IP_KEY = "kv_ip_key_";

    public static final String kv_max_ip = "kv_max_ip";

    public static final String KV_STOP_CHECK_LIVE_IP = "kv_stop_check_live_ip";

    /**
     * 当前直播人气
     */
    public static final String KV_CURRENT_LIVE_COUNT_HOT_KEY = "kv_current_live_count_hot_";

    /**
     * 人气阶梯设置
     */
    public static final String KV_LIVE_COUNTHOT_STEP_OPTIONS = "kv_live_counthot_step_options";


    /**
     * 机器人任务队列
     */
    public static final String KV_LIVE_ROBOT_TASK_LIST = "kv_live_robot_task_list";

    /**
     * 虚拟弹幕任务队列
     */
    public static final String KV_LIVE_BARRAGE_TASK_LIST = "kv_live_barrage_task_list";

    /**
     * 虚拟评论任务队列
     */
    public static final String KV_LIVE_COMMENT_TASK_LIST = "kv_live_comment_task_list";

    /**
     * 直播PK申请记录
     * 例如：get kv_live_pk_apply_for_record_1 = 2
     * 指：用户2对用户1发起PK申请
     * 该记录设置有一定的有效期，比如在系统配置的基础上+3秒
     */
    public static final String KV_LIVE_PK_APPLY_FOR_RECORD = "kv_live_pk_apply_for_record_";

    /**
     * 申请直播PK锁
     */
    public static final String KV_LIVE_PK_APPLY_FOR_LOCK = "kv_live_pk_apply_for_lock_";

    /**
     * ack 申请直播PK锁
     */
    public static final String KV_LIVE_PK_APPLY_FOR_ACK_LOCK = "kv_live_pk_apply_for_ack_lock_";

    /**
     * PK申请次数
     */
    public static final String KV_LIVE_PK_APPLY_FOR_COUNT = "kv_live_pk_apply_for_count_";

    /**
     * 申请直播连麦锁
     */
    public static final String KV_LIVE_LM_APPLY_FOR_LOCK = "kv_live_lm_apply_for_lock_";

    /**
     * 存放需要退智慧豆的连麦申请
     */
    public static final String KV_LIVE_LM_APPLY_FOR_LIST_FOR_REFUND = "kv_live_lm_apply_for_list_for_refund";

    /**
     * 队列，给主播分成
     */
    public static final String KV_LIVE_LM_APPLY_FOR_LIST_FOR_INCOME = "kv_live_lm_apply_for_list_for_income";
    //智慧豆兑换上级返现 上级满足用户集合
    public static final String KV_QB_PARENT_LIST = "kv_qb_parent_list";

    public static final String KV_ROOT_USER_LIST = "kv_robot_user_list";

    /**
     * 连麦中的记录,如果结束时间到了用户都没有结束，后台强制结束
     */
    public static final String KV_LIVE_LM_APPLY_FOR_SET_FOR_END = "kv_live_lm_apply_for_set_for_end";

    //直播PK中收入QB列表
    public static final String KV_LIVE_PK_QB_LIST = "kv_live_pk_qb_list_";

    /**
     * 开播前的带课列表
     */
    public static final String KV_LIVE_PRE_LIST = "kv_live_pre_list_";
    /**
     * 开播前的带课列表序号，用于排序
     */
    public static final String KV_LIVE_PRE_INCREMENT = "kv_live_pre_increment_";

    /**
     * 保存 直播间 讲解中的课或者商品
     */
    public static final String KV_LIVE_GOING_SALE_GOODS = "kv_live_going_sale_goods_";

    /**
     * 添加直播回放任务 锁
     */
    public static final String KV_ADD_LIVE_REVIEW_TASK_LOCK = "kv_add_live_review_task_lock_";

    /**
     * 直播间发虚拟评论
     */
    public static final String KV_LIVE_ROBOT_COMMENT_TASK = "kv_live_robot_comment_task_";

    /**
     * 直播间发虚拟弹幕
     */
    public static final String KV_LIVE_ROBOT_BARRAGE_TASK = "kv_live_robot_barrage_task_";

    /**
     * 直播间发虚拟弹幕  控制发送的最大智慧豆数量
     */
    public static final String KV_LIVE_ROBOT_BARRAGE_TASK_MAXQB = "kv_live_robot_barrage_task_maxqb_";

}
