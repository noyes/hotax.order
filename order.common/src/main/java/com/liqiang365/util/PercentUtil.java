package com.liqiang365.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * 百分比工具类；用于计算和格式化统计相关的百分比计算；
 *
 * @author 苏赛飞
 */
public class PercentUtil {

    public static final DecimalFormat proportionFormat = new DecimalFormat("0.00");

    public static final BigDecimal multiplicand = new BigDecimal("100");


    public static String divide(BigDecimal a, BigDecimal b) {

        return getProportion(a.divide(b, 4, BigDecimal.ROUND_FLOOR));
    }

    public static String divide(int a, int b){
        BigDecimal aBd = new BigDecimal(a + "");
        BigDecimal bDd = new BigDecimal(b + "");
        return getProportion(aBd.divide(bDd, 4, BigDecimal.ROUND_FLOOR));
    }

    static String formatProportion(BigDecimal proportion){

        BigDecimal bigDecimal = proportion.multiply(multiplicand).setScale(2);
        return proportionFormat.format(bigDecimal)+"%";
    }



    public static String getProportion(BigDecimal value){
        String proportion = "";
        if(value == null) {
            proportion = "/";
        } else if(value.compareTo(BigDecimal.ZERO) > 0) {
            proportion = formatProportion(value);
        } else if(value.compareTo(BigDecimal.ZERO) == 0) {
            proportion = "-";
        } else {
            proportion = formatProportion(value);
        }
        return proportion;
    }
}
