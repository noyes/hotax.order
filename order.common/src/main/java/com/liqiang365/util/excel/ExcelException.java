package com.liqiang365.util.excel;

public class ExcelException extends Exception {
    public ExcelException(String message) {
        super(message);
    }
}
