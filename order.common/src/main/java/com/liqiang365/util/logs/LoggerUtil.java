
package com.liqiang365.util.logs;

import com.google.gson.Gson;
import com.liqiang365.util.date.DateUtils;
import com.liqiang365.util.http.HttpUtil;
import hotax.core.util.ValidUtil;

import javax.servlet.http.HttpServletRequest;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;


public class LoggerUtil {

    public final static String logStrBegin = "-------------------------请求开始-------------------------";
    public final static String logStrEnd = "-------------------------请求完成-------------------------";

    public static String logBegin() {
        return logStrBegin;
    }

    public static String logEnd() {
        return logStrEnd;
    }

    public static String logPath(HttpServletRequest request) {
        return "请求地址:" + request.getRequestURL();
    }

    public static String logClient(HttpServletRequest request) {
        return "请求客户端:" + "Time:" + DateUtils.getDateTime()
                + " , IP:" + HttpUtil.getIpAddr(request)
                + " , Agent:" + HttpUtil.getUserAgent(request);
    }

    public static String logArgs(HttpServletRequest request) {
        Map map = new HashMap<>(request.getParameterMap());
        if(null != map){
            if (ValidUtil.isNotEmpty(map.get("password"))) {
                map.put("password", "***");
            }
            if (ValidUtil.isNotEmpty(map.get("paypassword"))) {
                map.put("paypassword", "***");
            }
            if (ValidUtil.isNotEmpty(map.get("PASSWORD"))) {
                map.put("PASSWORD", "***");
            }
            if (ValidUtil.isNotEmpty(map.get("oldpassword"))) {
                map.put("oldpassword", "***");
            }
            if (ValidUtil.isNotEmpty(map.get("newpassword"))) {
                map.put("newpassword", "***");
            }
        }
        return "请求参数:" + new Gson().toJson(map);
    }

    public static String logException(Throwable e) {


        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);
        e.printStackTrace(writer);
        StringBuffer buffer = stringWriter.getBuffer();
        return "抛出异常:" + buffer.toString();
    }

    public static String logWarn(String msg, HttpServletRequest request) {
        return "警告信息:" + msg
                + " , URL:" + request.getRequestURL()
                + " , Args:" + logArgs(request)
                + " , IP:" + HttpUtil.getIpAddr(request)
                + " , Agent:" + HttpUtil.getUserAgent(request);
    }
}
