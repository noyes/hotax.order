package com.liqiang365.util.redlock;

/**
 * Created by Point on 2017/9/21.
 * 锁管理类接口
 */
public interface DistributedLocker {
    /**
     * 加锁，超时未能获取锁会触发UnableToAquireLockException，需要捕获
     * 使用方式为：
     * Object result = lock(key,new AquiredLockWorker<Object>() {
     *      @Override
     *      public Object invokeAfterLockAquire() {
     *          return doTask();
     *      }
     * });
     * @param key 锁住的资源名称
     * @param worker 要执行的操作
     * @return 要执行的操作的返回结果
     */
    public <T> T lock(String key, AquiredLockWorker<T> worker) throws Exception ;

    /**
     * 加锁，超时未能获取锁会触发UnableToAquireLockException，需要捕获
     * 使用方式为：
     * Object result = lock(key,new AquiredLockWorker<Object>() {
     *      @Override
     *      public Object invokeAfterLockAquire() {
     *          return doTask();
     *      }
     * });
     * @param key 锁住的资源名称
     * @param worker 要执行的操作
     * @param expireSecs 锁的超时时间（单位是秒），从获取锁开始算起，业务必须在expireSecs秒内完成并释放锁，否则锁会自动释放，默认10s
     * @return 要执行的操作的返回结果
     */
    public <T> T lock(String key, AquiredLockWorker<T> worker, int expireSecs) throws Exception ;

    /**
     * 加锁，超时未能获取锁会触发UnableToAquireLockException，需要捕获
     * 使用方式为：
     * Object result = lock(key,new AquiredLockWorker<Object>() {
     *      @Override
     *      public Object invokeAfterLockAquire() {
     *          return doTask();
     *      }
     * });
     * @param key 锁住的资源名称
     * @param worker 要执行的操作
     * @param timeoutMsecs timeout的时间范围内轮询锁(单位是毫秒)，默认为0，代表不等待锁资源，加锁失败立刻返回
     * @return 要执行的操作的返回结果
     */
    public <T> T lock(String key, AquiredLockWorker<T> worker, long timeoutMsecs) throws Exception ;


    /**
     * 加锁，超时未能获取锁会触发UnableToAquireLockException，需要捕获
     * 使用方式为：
     * Object result = lock(key,new AquiredLockWorker<Object>() {
     *      @Override
     *      public Object invokeAfterLockAquire() {
     *          return doTask();
     *      }
     * });
     * @param key 锁住的资源名称
     * @param worker 要执行的操作
     * @param expireSecs 锁的超时时间（单位是秒），从获取锁开始算起，业务必须在expireSecs秒内完成并释放锁，否则锁会自动释放，默认10s
     * @param timeoutMsecs timeout的时间范围内轮询锁(单位是毫秒)，默认为0，代表不等待锁资源，加锁失败立刻返回
     * @return 要执行的操作的返回结果
     */
    public <T> T lock(String key, AquiredLockWorker<T> worker, int expireSecs, long timeoutMsecs) throws Exception;


    /**
     * 加锁，超时未能获取锁会触发UnableToAquireLockException，需要捕获
     * 使用方式为：
     * long token = 0L;
     * try{
     *      token = lock(key,expireSecs,timeoutMsecs);
     *      executeMethod();
     *  }catch (UnableToAquireLockException){
     *      //超时未获取到锁资源
     *  }finally{
     *      unlock(key,token);
     *  }
     * @param key 锁住的资源名称
     * @return 锁票据，用于解锁
     */
    public long lock(String key) throws com.liqiang365.util.redlock.UnableToAquireLockException;

    /**
     * 加锁，超时未能获取锁会触发UnableToAquireLockException，需要捕获
     * 使用方式为：
     * long token = 0L;
     * try{
     *      token = lock(key,expireSecs,timeoutMsecs);
     *      executeMethod();
     *  }catch (UnableToAquireLockException){
     *      //超时未获取到锁资源
     *  }finally{
     *      unlock(key,token);
     *  }
     * @param key 锁住的资源名称
     * @param expireSecs 锁的超时时间（单位是秒），从获取锁开始算起，业务必须在expireSecs秒内完成并释放锁，否则锁会自动释放，默认10s
     * @return 锁票据，用于解锁
     */
    public  long lock(String key, int expireSecs) throws com.liqiang365.util.redlock.UnableToAquireLockException;

    /**
     * 加锁，超时未能获取锁会触发UnableToAquireLockException，需要捕获
     * 使用方式为：
     * long token = 0L;
     * try{
     *      token = lock(key,expireSecs,timeoutMsecs);
     *      executeMethod();
     *  }catch (UnableToAquireLockException){
     *      //超时未获取到锁资源
     *  }finally{
     *      unlock(key,token);
     *  }
     * @param key 锁住的资源名称
     * @param timeoutMsecs timeout的时间范围内轮询锁(单位是毫秒)，默认为0，代表不等待锁资源，加锁失败立刻返回
     * @return 锁票据，用于解锁
     */
    public  long lock(String key, long timeoutMsecs) throws com.liqiang365.util.redlock.UnableToAquireLockException;

    /**
     * 加锁，超时未能获取锁会触发UnableToAquireLockException，需要捕获
     * 使用方式为：
     * long token = 0L;
     * try{
     *      token = lock(key,expireSecs,timeoutMsecs);
     *      executeMethod();
     *  }catch (UnableToAquireLockException){
     *      //超时未获取到锁资源
     *  }finally{
     *      unlock(key,token);
     *  }
     * @param key 锁住的资源名称
     * @param expireSecs 锁的超时时间（单位是秒），从获取锁开始算起，业务必须在expireSecs秒内完成并释放锁，否则锁会自动释放，默认10s
     * @param timeoutMsecs timeout的时间范围内轮询锁(单位是毫秒)，默认为0，代表不等待锁资源，加锁失败立刻返回
     * @return 锁票据，用于解锁
     */
    public long lock(String key, int expireSecs, long timeoutMsecs) throws com.liqiang365.util.redlock.UnableToAquireLockException;

    /**
     * 加锁，返回操作成功失败，不返回token，解锁安全性依赖计时一致性假设
     * 使用方式为：
     * if(trylock()){
     *      try{
     *          executeMethod();
     *      }finally{
     *          unlock();
     *      }
     * }
     * @param key 锁住的资源名称
     * @return 加锁是否成功
     */
    public boolean tryLock(String key);

    /**
     * 加锁，返回操作成功失败，不返回token，解锁安全性依赖计时一致性假设
     * 使用方式为：
     * if(trylock()){
     *      try{
     *          executeMethod();
     *      }finally{
     *          unlock();
     *      }
     * }
     * @param key 锁住的资源名称
     * @param expireSecs 锁的超时时间（单位是秒），从获取锁开始算起，业务必须在expireSecs秒内完成并释放锁，否则锁会自动释放，默认10s
     * @return 加锁是否成功
     */
    public boolean tryLock(String key, int expireSecs);

    /**
     * 加锁，返回操作成功失败，不返回token，解锁安全性依赖计时一致性假设
     * 使用方式为：
     * if(trylock()){
     *      try{
     *          executeMethod();
     *      }finally{
     *          unlock();
     *      }
     * }
     * @param key 锁住的资源名称
     * @param timeoutMsecs timeout的时间范围内轮询锁(单位是毫秒)，默认为0，代表不等待锁资源，加锁失败立刻返回
     * @return 加锁是否成功
     */
    public boolean tryLock(String key, long timeoutMsecs);

    /**
     * 加锁，返回操作成功失败，不返回token，解锁安全性依赖计时一致性假设
     * 使用方式为：
     * if(trylock()){
     *      try{
     *          executeMethod();
     *      }finally{
     *          unlock();
     *      }
     * }
     * @param key 锁住的资源名称
     * @param expireSecs 锁的超时时间（单位是秒），从获取锁开始算起，业务必须在expireSecs秒内完成并释放锁，否则锁会自动释放，默认10s
     * @param timeoutMsecs timeout的时间范围内轮询锁(单位是毫秒)，默认为0，代表不等待锁资源，加锁失败立刻返回
     * @return 加锁是否成功
     */
    public boolean tryLock(String key, int expireSecs, long timeoutMsecs);

    /**
     * 释放锁（只会释放调用者持有的锁）
     * @param key 锁住的资源名称
     * @param token 加锁时得到的票据，推荐解锁时回传票据，在回传票据的情况下解锁安全性不依赖计时一致性假设，不回传票据的情况下解锁安全性依赖于计时一致性假设
     */
    public void unlock(String key, long token);

    /**
     * 释放锁（只会释放调用者持有的锁）
     * @param key 锁住的资源名称
     */
    public void unlock(String key);
}
