package liqiang365.util.redlock.aop;

import com.liqiang365.util.redis.RedisClient;
import com.liqiang365.util.redlock.RedisLocker;
import com.liqiang365.util.redlock.RedlockException;
import com.liqiang365.util.valid.ValidUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Random;

/**
 * Created by Point on 2017/9/25.
 */

@Aspect
@Component
public class RedlockInterceptor {

    public RedlockInterceptor(){

        System.out.println("AOP prepared");

    }


    /**
     * CAS环绕增强Interceptor
     *
     * @param pjp  ProceedingJoinPoint对象
     * @return
     */
    @Around("execution(* *(..)) && @annotation(RedisCAS)")
    public Object RedisCASInterceptor(ProceedingJoinPoint pjp ) throws Throwable {

        Signature sig = pjp.getSignature();
        MethodSignature msig = null;
        if (!(sig instanceof MethodSignature)) {
            throw new IllegalArgumentException("该注解只能用于方法");
        }

        msig = (MethodSignature) sig;
        Method currentMethod = pjp.getTarget().getClass().getMethod(msig.getName(), msig.getParameterTypes());

        Annotation[][] parameterAnnotations = currentMethod.getParameterAnnotations();

        Object[] args = pjp.getArgs();

        String redlockKey = null;
        //String redlockValue = null;
        //Object redisValueArg = null;
        int redisValueArgIndex = -1;

        for (int i = 0; i < parameterAnnotations.length; i++) {
            for (int j = 0; j < parameterAnnotations[i].length; j++) {
                if (parameterAnnotations[i][j] instanceof RedlockKey) {
                    redlockKey = String.valueOf(args[i]);
                }

                if (parameterAnnotations[i][j] instanceof RedlockValue) {
                    //redisValueArg = args[i];
                    redisValueArgIndex = i;
                    //redlockValue = String.valueOf(redisValueArg);
                }

            }

            if (redlockKey != null && redisValueArgIndex >= 0) {
                break;
            }
        }

        if (redlockKey == null || redisValueArgIndex < 0) {
            throw new IllegalArgumentException("参数中必须包含@RedlockKey及@RedlockValue");
        }

        RedisCAS cas = currentMethod.getAnnotation(RedisCAS.class);
        long timeoutMsec = cas.timeOutMsec();
        long retryLimitTimeMsec = System.currentTimeMillis()+timeoutMsec;

        do {
            String oldValue = RedisClient.get(redlockKey);

            args[redisValueArgIndex] = String.valueOf(oldValue);
            Object result;
            result = pjp.proceed(args);

            if(result == null)
            {
                throw new RedlockException("CAS方法没有返回要设置的Redis新值。");
            }

            boolean trySetResultFlag = RedisClient.setIfExiseByScript(redlockKey,oldValue,String.valueOf(result));

            if(trySetResultFlag)
            {
               return result;
            }
            //获取资源失败则短暂随机休眠，避免可能的活锁
            Random rand = new Random();
            Thread.sleep(5, rand.nextInt(30));
        }
        while(System.currentTimeMillis()<retryLimitTimeMsec);

        throw new RedlockException("到达超时时间操作仍然没有成功。");

    }


    /**
     * RedLock前置增强Interceptor
     *
     * @param pjp  ProceedingJoinPoint对象
     * @return
     */
    @Around(("execution(* *(..)) && @annotation(RedisLock)"))
    public Object RedisLockInterceptor (ProceedingJoinPoint pjp) throws Throwable {
        Signature sig = pjp.getSignature();
        MethodSignature msig = null;
        if (!(sig instanceof MethodSignature)) {
            throw new IllegalArgumentException("该注解只能用于方法");
        }

        msig = (MethodSignature) sig;
        Method currentMethod = pjp.getTarget().getClass().getMethod(msig.getName(), msig.getParameterTypes());

        RedisLock lockConfig = currentMethod.getAnnotation(RedisLock.class);
        long timeoutMsec = lockConfig.timeOutMsec();
        int expireTimeSec = lockConfig.expireTimeSec();
        String key = lockConfig.key();

        if(ValidUtil.isEmpty(key))
        {
            Annotation[][] parameterAnnotations = currentMethod.getParameterAnnotations();
            Object[] args = pjp.getArgs();

            for (int i = 0; i < parameterAnnotations.length; i++) {
                for (int j = 0; j < parameterAnnotations[i].length; j++) {
                    if (parameterAnnotations[i][j] instanceof RedlockKey) {
                        key = String.valueOf(args[i]);
                    }
                    if (key != null ) {
                        break;
                    }
                }
                if (key != null ) {
                    break;
                }
            }
        }

        if(ValidUtil.isEmpty(key))
        {
            throw new IllegalArgumentException("必须在@RedisLock注解中设置key的值或者在参数中包含@RedlockKey");
        }

        long retryLimitTimeMsec = System.currentTimeMillis()+timeoutMsec;

        RedisLocker myLocker = new RedisLocker();

        long token = myLocker.lock(key,expireTimeSec,timeoutMsec);
        if(token>0) {
            try {
                return pjp.proceed(pjp.getArgs());
            } finally {
                myLocker.unlock(key, token);
            }
        }

        throw new RedlockException("到达超时时间操作仍然没有成功。");
    }

}
