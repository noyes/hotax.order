package com.liqiang365.util.redlock;

/**
 * Created by Point on 2017/9/21.
 * 获取锁后需要处理的逻辑
 */
public interface AquiredLockWorker<T> {
    T invokeAfterLockAquire() throws Exception;
}
