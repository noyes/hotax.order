package com.liqiang365.util.redlock;

import com.liqiang365.util.redis.RedissonManager;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;


/**
 * Created by Point on 2017/9/25.
 */
@Component
public class RedisLocker implements DistributedLocker {

    private static final String LOCKER_PREFIX = "redlock:";
    private static final long MILLI_SEC = 1000L;

    //获取锁的最大等待时间，超过这个时间仍然没能获取锁则返回超时异常，单位是毫秒，默认2000ms即2s
    private static final long DEFAULT_TIMEOUT_MESC = 2000L;
    //锁的有效期，从成功获取锁开始算起，必须在expireTimeSec内完成操作并释放锁，否则锁会自动释放，单位是秒。默认10s
    private static final int DEFAULT_EXPIRETIME_SEC = 10;

    /**
     * 加锁，超时未能获取锁会触发UnableToAquireLockException，需要捕获
     * 使用方式为：
     * Object result = lock(key,new AquiredLockWorker<Object>() {
     *
     * @param key    锁住的资源名称
     * @param worker 要执行的操作
     * @return 要执行的操作的返回结果
     * @Override public Object invokeAfterLockAquire() {
     * return doTask();
     * }
     * });
     */
    public <T> T lock(String key, AquiredLockWorker<T> worker) throws Exception {
        return lock(key, worker, DEFAULT_EXPIRETIME_SEC, DEFAULT_TIMEOUT_MESC);
    }

    /**
     * 加锁，超时未能获取锁会触发UnableToAquireLockException，需要捕获
     * 使用方式为：
     * Object result = lock(key,new AquiredLockWorker<Object>() {
     *
     * @param key        锁住的资源名称
     * @param worker     要执行的操作
     * @param expireSecs 锁的超时时间（单位是秒），从获取锁开始算起，业务必须在expireSecs秒内完成并释放锁，否则锁会自动释放，默认10s
     * @return 要执行的操作的返回结果
     * @Override public Object invokeAfterLockAquire() {
     * return doTask();
     * }
     * });
     */
    public <T> T lock(String key, AquiredLockWorker<T> worker, int expireSecs) throws Exception {
        return lock(key, worker, DEFAULT_EXPIRETIME_SEC, DEFAULT_TIMEOUT_MESC);
    }

    /**
     * 加锁，超时未能获取锁会触发UnableToAquireLockException，需要捕获
     * 使用方式为：
     * Object result = lock(key,new AquiredLockWorker<Object>() {
     *
     * @param key          锁住的资源名称
     * @param worker       要执行的操作
     * @param timeoutMsecs timeout的时间范围内轮询锁(单位是毫秒)，默认为0，代表不等待锁资源，加锁失败立刻返回
     * @return 要执行的操作的返回结果
     * @Override public Object invokeAfterLockAquire() {
     * return doTask();
     * }
     * });
     */
    public <T> T lock(String key, AquiredLockWorker<T> worker, long timeoutMsecs) throws Exception {
        return lock(key, worker, DEFAULT_EXPIRETIME_SEC, DEFAULT_TIMEOUT_MESC);
    }


    /**
     * 加锁，超时未能获取锁会触发UnableToAquireLockException，需要捕获
     * 使用方式为：
     * Object result = lock(key,new AquiredLockWorker<Object>() {
     *
     * @param key          锁住的资源名称
     * @param worker       要执行的操作
     * @param expireSecs   锁的超时时间（单位是秒），从获取锁开始算起，业务必须在expireSecs秒内完成并释放锁，否则锁会自动释放，默认10s
     * @param timeoutMsecs timeout的时间范围内轮询锁(单位是毫秒)，默认为0，代表不等待锁资源，加锁失败立刻返回
     * @return 要执行的操作的返回结果
     * @Override public Object invokeAfterLockAquire() {
     * return doTask();
     * }
     * });
     */
    public <T> T lock(String key, AquiredLockWorker<T> worker, int expireSecs, long timeoutMsecs) throws Exception {
        key = LOCKER_PREFIX + key;
        long token = lock(key, expireSecs, timeoutMsecs);
        if (token > 0) {
            try {
                return worker.invokeAfterLockAquire();
            } finally {
                unlock(key, token);
            }
        }
        throw new com.liqiang365.util.redlock.UnableToAquireLockException();


    }


    /**
     * 加锁，超时未能获取锁会触发UnableToAquireLockException，需要捕获
     * 使用方式为：
     * long token = 0L;
     * try{
     * token = lock(key,expireSecs,timeoutMsecs);
     * executeMethod();
     * }catch (UnableToAquireLockException){
     * //超时未获取到锁资源
     * }finally{
     * unlock(key,token);
     * }
     *
     * @param key 锁住的资源名称
     * @return 锁票据，用于解锁
     */
    @Override
    public long lock(String key) throws com.liqiang365.util.redlock.UnableToAquireLockException {
        return lock(key, DEFAULT_EXPIRETIME_SEC, DEFAULT_TIMEOUT_MESC);
    }

    /**
     * 加锁，超时未能获取锁会触发UnableToAquireLockException，需要捕获
     * 使用方式为：
     * long token = 0L;
     * try{
     * token = lock(key,expireSecs,timeoutMsecs);
     * executeMethod();
     * }catch (UnableToAquireLockException){
     * //超时未获取到锁资源
     * }finally{
     * unlock(key,token);
     * }
     *
     * @param key        锁住的资源名称
     * @param expireSecs 锁的超时时间（单位是秒），从获取锁开始算起，业务必须在expireSecs秒内完成并释放锁，否则锁会自动释放，默认10s
     * @return 锁票据，用于解锁
     */
    @Override
    public long lock(String key, int expireSecs) throws com.liqiang365.util.redlock.UnableToAquireLockException {
        return lock(key, expireSecs, DEFAULT_TIMEOUT_MESC);
    }

    /**
     * 加锁，超时未能获取锁会触发UnableToAquireLockException，需要捕获
     * 使用方式为：
     * long token = 0L;
     * try{
     * token = lock(key,expireSecs,timeoutMsecs);
     * executeMethod();
     * }catch (UnableToAquireLockException){
     * //超时未获取到锁资源
     * }finally{
     * unlock(key,token);
     * }
     *
     * @param key          锁住的资源名称
     * @param timeoutMsecs timeout的时间范围内轮询锁(单位是毫秒)，默认为0，代表不等待锁资源，加锁失败立刻返回
     * @return 锁票据，用于解锁
     */
    @Override
    public long lock(String key, long timeoutMsecs) throws com.liqiang365.util.redlock.UnableToAquireLockException {
        return lock(key, DEFAULT_EXPIRETIME_SEC, timeoutMsecs);
    }

    /**
     * 加锁，超时未能获取锁会触发UnableToAquireLockException，需要捕获
     * 使用方式为：
     * long token = 0L;
     * try{
     * token = lock(key,expireSecs,timeoutMsecs);
     * executeMethod();
     * }catch (UnableToAquireLockException){
     * //超时未获取到锁资源
     * }finally{
     * unlock(key,token);
     * }
     *
     * @param key          锁住的资源名称
     * @param expireSecs   锁的超时时间（单位是秒），从获取锁开始算起，业务必须在expireSecs秒内完成并释放锁，否则锁会自动释放，默认10s
     * @param timeoutMsecs timeout的时间范围内轮询锁(单位是毫秒)，默认为0，代表不等待锁资源，加锁失败立刻返回
     * @return 锁票据，用于解锁
     */
    @Override
    public long lock(String key, int expireSecs, long timeoutMsecs) throws com.liqiang365.util.redlock.UnableToAquireLockException {

        //处理一下参数
       /* key = LOCKER_PREFIX+key;
        expireSecs = expireSecs<=0?10:expireSecs;
        timeoutMsecs = timeoutMsecs<0?0:timeoutMsecs;
        long timeoutLimitMsecs = System.currentTimeMillis()+timeoutMsecs;
        System.out.println("key="+key);*/
        try {
           /* do{
                //用于解锁校验
                long token = ((long)expireSecs)*MILLI_SEC + System.nanoTime();
                //luaScript是原子操作且线程安全
                if (RedisClient.setNXEXByScript(key, String.valueOf(token),expireSecs)) {
                    return token;
                }

                //获取资源失败则短暂随机休眠，避免可能的活锁
                Random rand = new Random();
                Thread.sleep(5, rand.nextInt(30));
            }
            while(System.currentTimeMillis() < timeoutLimitMsecs);*/
            int i = 60 * 30;
            RedissonManager.lock(key, i, TimeUnit.SECONDS);
        } catch (Exception e) {
            throw new RuntimeException("locking error", e);
        }
        //throw new UnableToAquireLockException("locking timeout");
        return 0;
    }

    /**
     * 加锁，返回操作成功失败，不返回token，解锁安全性依赖计时一致性假设
     * 使用方式为：
     * if(trylock()){
     * try{
     * executeMethod();
     * }finally{
     * unlock();
     * }
     * }
     *
     * @param key 锁住的资源名称
     * @return 加锁是否成功
     */
    @Override
    public boolean tryLock(String key) {
        return tryLock(key, DEFAULT_EXPIRETIME_SEC, DEFAULT_TIMEOUT_MESC);
    }

    /**
     * 加锁，返回操作成功失败，不返回token，解锁安全性依赖计时一致性假设
     * 使用方式为：
     * if(trylock()){
     * try{
     * executeMethod();
     * }finally{
     * unlock();
     * }
     * }
     *
     * @param key        锁住的资源名称
     * @param expireSecs 锁的超时时间（单位是秒），从获取锁开始算起，业务必须在expireSecs秒内完成并释放锁，否则锁会自动释放，默认10s
     * @return 加锁是否成功
     */
    @Override
    public boolean tryLock(String key, int expireSecs) {
        return tryLock(key, expireSecs, DEFAULT_TIMEOUT_MESC);
    }

    /**
     * 加锁，返回操作成功失败，不返回token，解锁安全性依赖计时一致性假设
     * 使用方式为：
     * if(trylock()){
     * try{
     * executeMethod();
     * }finally{
     * unlock();
     * }
     * }
     *
     * @param key          锁住的资源名称
     * @param timeoutMsecs timeout的时间范围内轮询锁(单位是毫秒)，默认为0，代表不等待锁资源，加锁失败立刻返回
     * @return 加锁是否成功
     */
    @Override
    public boolean tryLock(String key, long timeoutMsecs) {
        return tryLock(key, DEFAULT_EXPIRETIME_SEC, timeoutMsecs);
    }


    /**
     * 加锁，返回操作成功失败，不返回token，解锁安全性依赖计时一致性假设
     * 使用方式为：
     * if(trylock()){
     * try{
     * executeMethod();
     * }finally{
     * unlock();
     * }
     * }
     *
     * @param key          锁住的资源名称
     * @param expireSecs   锁的超时时间（单位是秒），从获取锁开始算起，业务必须在expireSecs秒内完成并释放锁，否则锁会自动释放，默认10s
     * @param timeoutMsecs timeout的时间范围内轮询锁(单位是毫秒)，默认为0，代表不等待锁资源，加锁失败立刻返回
     * @return 加锁是否成功
     */
    @Override
    public boolean tryLock(String key, int expireSecs, long timeoutMsecs) {
        boolean flag = false;
        try {
            long token = lock(key, expireSecs, timeoutMsecs);
            flag = token > 0;
        } catch (Throwable throwable) {
            flag = false;
        }
        return flag;
    }

    /**
     * 释放锁（只会释放调用者持有的锁）
     *
     * @param key   锁住的资源名称
     * @param token 加锁时得到的票据，推荐解锁时回传票据，在回传票据的情况下解锁安全性不依赖计时一致性假设，不回传票据的情况下解锁安全性依赖于计时一致性假设
     */
    @Override
    public void unlock(String key, long token) {
        try {
            RedissonManager.unlock(key);
        } catch (Exception e) {
            //异常
            e.printStackTrace();
        }
    }

    /**
     * 释放锁（只会释放调用者持有的锁）
     *
     * @param key 锁住的资源名称
     */
    @Override
    public void unlock(String key) {
        try {
            RedissonManager.unlock(key);
        } catch (Exception e) {
            //异常
            e.printStackTrace();
        }
    }
}