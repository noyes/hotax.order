package liqiang365.util.redlock.aop;

import java.lang.annotation.*;

/**
 * Created by Point on 2017/9/25.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisLock {
    String key() default "";//要锁住的资源名称，可以为“”，为“”时会尝试在参数中寻找@RedlockKey作为key
    long timeOutMsec() default 2000;//获取锁的最大等待时间，超过这个时间仍然没能获取锁则返回超时异常，单位是毫秒，默认2000ms即2s
    int expireTimeSec() default 10;//锁的有效期，从成功获取锁开始算起，必须在expireTimeSec内完成操作并释放锁，否则锁会自动释放，单位是秒。默认10s
}
