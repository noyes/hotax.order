package liqiang365.util.redlock.aop;

import java.lang.annotation.*;

/**
 * Created by Point on 2017/9/25.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisCAS {
    long timeOutMsec() default 2000;//执行操作的最大等待时间，超过这个时间仍然没能执行成功则返回超时异常，单位是毫秒，默认2000ms即2s
}
