package com.liqiang365.util.redlock;

/**
 * Created by Point on 2017/9/25.
 */
public class RedlockException extends RuntimeException {
    public RedlockException() {
    }

    public RedlockException(String message) {
        super(message);
    }

    public RedlockException(String message, Throwable cause) {
        super(message, cause);
    }
}
