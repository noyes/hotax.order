package com.liqiang365.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * @author 吴华龙
 * @classname DivideRedPackageUtils
 * @description 此家伙很懒，什么都没留下
 * @date 2019/12/30 13:55
 */
public class DivideRedPackageUtils {


    public static List<Integer> divideRedPackage(Integer totalAmount, Integer totalPeopleNum) {
        List<Integer> amountList = new ArrayList<>();
        Integer restAmount = totalAmount;
        Integer restPeopleNum = totalPeopleNum;
        Random random = new Random();
        for (int i = 0; i < totalPeopleNum - 1; i++) {
            //随机范围：[1，剩余人均金额的两倍)，左闭右开
            int res = restAmount / restPeopleNum * 2 - 1;
            int amount = 0;
            if (res <= 0) {
                amount = 0;
            } else {
                amount = random.nextInt(res) + 1;
            }
            restPeopleNum--;
            restAmount -= amount;
            amountList.add(amount);
        }
        Collections.shuffle(amountList);
        if (amountList.isEmpty()) {
            amountList.add(0);
        }
        return amountList;
    }
}
