package com.liqiang365.util.http;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import liqiang365.util.logs.LoggerUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@SuppressWarnings("ALL")
@Component
@Slf4j
public class RestTemplateUtil {


    @Autowired
    private RestTemplate restTemplate;


    public String GetData(String url, Map<String, Object> uriVariables) {
        ResponseEntity res = restTemplate.getForEntity(url, String.class, uriVariables);
        if (res.getStatusCode().equals(HttpStatus.OK)) {
            System.out.println("远程调用成功  url= " + url + " body= " + res.getBody());
            return String.valueOf(res.getBody());
        } else {
            System.out.println("远程调用失败   code= " + res.getStatusCode() + " body= " + res.getBody());
            return null;
        }
    }

    /**
     * 功能描述: <br> GET方式调用，支持restful风格动态
     * 〈〉
     *
     * @Param: url
     * http://localhost:8183/restTest/result?name={name}&age={age}&ss={ss}
     * @Param: uriVariables
     * <>
     * Map<String, Object> paramMap = new HashMap<>();
     * paramMap.put("name", "苏赛飞");
     * paramMap.put("age", 20);
     * List<String> ids = new ArrayList<>();
     * ids.add("1");
     * ids.add("2");
     * ids.add("3");
     * ids.add("哈哈哈");
     * String join = StringUtils.join(ids.toArray(), ",");
     * paramMap.put("ss", join);
     * </>
     * @Param: clazz 返回值类型
     * @Return: T
     * @Author: susaifei
     * @Date: 2019/4/24 11:33
     */
    public <T> T getForEntity(String url, Map<String, Object> uriVariables, Class<T> clazz) {
        return this.getForEntity(url, uriVariables, clazz, null);
    }

    public <T> T getForEntity(String url, Map<String, Object> uriVariables, Class<T> clazz, HttpEntity entity) {
        ResponseEntity<T> res = restTemplate.exchange(url, HttpMethod.GET, entity, clazz, uriVariables);
        return returnT(res, url);
    }

    public <T> T getForEntity(String url, HttpEntity entity, ParameterizedTypeReference<T> ptr, Map<String, Object> uriVariables) {
        ResponseEntity<T> res = restTemplate.exchange(url, HttpMethod.GET, entity, ptr, uriVariables);
        return returnT(res, url);
    }


    /**
     * 发送POST-map请求
     *
     * @param url
     * @param params
     * @return
     */

    /**
     * 功能描述: <br>Post方式请求，普通的表单提交方式
     * 〈〉
     *
     * @Param url 请求地址
     * @Param params 请求参数，MultiValueMap类型
     * @Param clazz 结果类型
     * @Param uriVariables 地址变量，用于比如
     * @Return: T 返回类型
     * @Author: susaifei
     * @Date: 2019/4/24 11:29
     */
    public <T> T PostData(String url, Object params, Class<T> clazz, Object... uriVariables) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        HttpEntity<Object> requestEntity = new HttpEntity<>(params, headers);
        return sendAndGetResult(url, HttpMethod.POST, requestEntity, clazz, uriVariables);
    }


    /**
     * 功能描述: 发送Post方式请求，参数是JSON字符串，接参方需要使用{@RequestBody}注解
     * 〈〉
     *
     * @throws JsonProcessingException 如果params格式不正常抛出异常
     * @Param url 请求地址
     * @Param params 请求参数，实体或者Map都可以
     * @Param clazz 结果类型
     * @Param uriVariables 地址变量，用于比如
     * @Return: T 返回类型
     * @Author: susaifei
     * @Date: 2019/4/24 11:05
     */
    public <T> T postJsonForEntity(String url, Object params, Class<T> clazz, Object... uriVariables) throws JsonProcessingException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        headers.add("Accept", MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.add("referer", "swagger");

        ObjectMapper mapper = new ObjectMapper();
        String param = mapper.writeValueAsString(params);

        HttpEntity<String> httpEntity = new HttpEntity<>(param, headers);
        return sendAndGetResult(url, HttpMethod.POST, httpEntity, clazz, uriVariables);
    }


    public <T> T postJsonForEntity(String url, Object params, Class<T> clazz, HttpHeaders headers, Object... uriVariables) throws JsonProcessingException {
        if (com.liqiang365.util.valid.ValidUtil.isEmpty(headers)) {
            headers = new HttpHeaders();
        }
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        headers.add("Accept", MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.add("referer", "swagger");

        ObjectMapper mapper = new ObjectMapper();
        String param = mapper.writeValueAsString(params);

        HttpEntity<String> httpEntity = new HttpEntity<>(param, headers);
        return sendAndGetResult(url, HttpMethod.POST, httpEntity, clazz, uriVariables);
    }


    /**
     * 功能描述: <br>
     * 〈〉
     *
     * @Param: [res]
     * @Param method GET 或者 POST方式
     * @Return: T
     * @Author: susaifei
     * @Date: 2019/4/24 11:19
     */
    private <T> T sendAndGetResult(String url, HttpMethod method, HttpEntity<?> httpEntity, Class<T> clazz, Object... uriVariables) {

        ResponseEntity<T> res = restTemplate.exchange(url, method, httpEntity, clazz, uriVariables);
        return returnT(res, url);
    }


    private <T> T returnT(ResponseEntity<T> res, String url) {
        if (res.getStatusCode().equals(HttpStatus.OK)) {
            // log.info("远程调用成功  url= " + url + " body= " + res.getBody());
            return res.getBody();
        } else {
            System.out.println("远程调用失败   code= " + res.getStatusCode() + " body= " + res.getBody());
            return null;
        }
    }
}
