package com.liqiang365.util.lang;

import java.lang.reflect.Field;
import java.util.Map;

public class BeanMapUtil {

    public static <T> T convertMap(Class<T> type, Map<String, Object> map) {
        T ob = null;
        try {
            if (map != null && map.size() > 0) {
                ob = type.newInstance();
                Field fields[] = type.getDeclaredFields();
                Field.setAccessible(fields, true);
                for (int i = 0; i < fields.length; i++) {
                    if (map.containsKey(fields[i].getName())) {
                        fields[i].set(ob, map.get(fields[i].getName()));
                    }
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
        return ob;
    }
}
