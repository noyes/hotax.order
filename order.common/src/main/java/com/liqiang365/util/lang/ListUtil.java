package com.liqiang365.util.lang;

import java.util.List;


public class ListUtil {

    public static <E> E findFirst(List<E> list) {
        if (list == null || list.size() < 1) {
            return null;
        }
        return list.get(0);
    }
}
