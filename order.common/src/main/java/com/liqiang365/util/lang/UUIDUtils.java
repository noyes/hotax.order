package com.liqiang365.util.lang;

import javax.servlet.http.HttpServletRequest;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

/**
 * @author
 * @version 1.0
 * <p><b>文件描述：根据sequence获取主键</b>
 * <p>
 * </p>
 **/
public class UUIDUtils {

    private static Calendar CALENDAR = Calendar.getInstance();
    private static int SERIALNUM = 0;

    public static void main(String[] args) {
        System.out.println(getPrimaryKeyUuid());
    }

    /**
     * 简单粗暴的主键生成策略
     *
     * @return
     */
    public static String getPrimaryKeyUuid() {
        return get32UUID().substring(0, 30);
    }

    /**
     * @param @return 设定文件
     * @return String    返回类型
     * @Title: getPrimaryKeyUuid
     * @Description: 生产数据库表主键
     */
    public static synchronized String getPrimaryKeyUuid2() {
        Calendar rightNow;
        do {
            rightNow = Calendar.getInstance();
            if (!rightNow.equals(CALENDAR)) {
                SERIALNUM = 0;
                CALENDAR = rightNow;
                break;
            }
            if (SERIALNUM < 255) {
                SERIALNUM++;
                break;
            }
            try {
                Thread.sleep(1L);
            } catch (InterruptedException interruptedexception) {
            }
        } while (true);
        long year = rightNow.get(1);
        long month = rightNow.get(2) + 1;
        long day = rightNow.get(5);
        long hour = rightNow.get(11);
        long minute = rightNow.get(12);
        long second = rightNow.get(13);
        long millisecond = rightNow.get(14);
        year <<= 36;
        month <<= 32;
        day <<= 27;
        hour <<= 22;
        minute <<= 16;
        second <<= 10;
        long t48 = year | month | day | hour | minute | second | millisecond;
        String ser = Integer.toHexString(SERIALNUM);
        ser = strPad(ser, 2);
        String sid = String.valueOf(Long.toHexString(t48))
                + String.valueOf(ser);
        sid = strPad(sid, 14);
        return sid;
    }

    private static String strPad(String str, int len) {
        int l = str.length();
        String s = str;
        for (int i = 0; i < len - l; i++) {
            s = "0".concat(String.valueOf(String.valueOf(s)));
        }

        return s;
    }

    public static String get32UUID() {
        return UUID.randomUUID().toString().trim().replaceAll("-", "");
    }


    //1位服务器节点码（根据ip或其他判断） + 1位渠道码（anroid、ios、公众号、pc） + 1位业务类型（vip升级、专栏、众筹商城、闪电购、竞价、充值） + 年月日时分秒12位（年去掉２０） + 4位随机数
    //总共19位！
    public static String getOrderUUID(HttpServletRequest request, String type) throws Exception {
        String randNum = new DecimalFormat("0").format(Math.random() * 9000 + 1000);
        String dateNum = new SimpleDateFormat("yyMMddHHmmss").format(new Date());
        String agent;
        if (com.liqiang365.util.valid.ValidUtil.isNotEmpty(request)) {
            agent = request.getHeader("user-agent");
        } else {
            agent = "Windows";
        }
        String clientType = getClientType(agent);
        String ip = getServerNum();
        return ip + clientType + type + dateNum + randNum;
    }


    private static String getServerNum() {
        return "4";
    }


    private static String getClientType(String agent) {
        //pc=1 ios=2 android=3 wx=4

        if (agent.contains("Windows")) {
            return "1";
        } else if (agent.contains("MicroMessenger") || agent.contains("microMessenger")) {
            return "4";
        } else if (agent.contains("Mac") || agent.contains("iPhone") || agent.contains("ios")) {
            return "2";
        } else if (agent.contains("android") || agent.contains("Android") || agent.contains("okhttp")) {
            return "3";
        }
        return "5";
    }


}
