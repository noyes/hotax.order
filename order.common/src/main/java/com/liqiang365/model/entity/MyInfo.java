package com.liqiang365.model.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 授权用户信息
 */
@Data
public class MyInfo {


    private String userId; // 主键
    private String username; // 登录名
    private String nickName; // 昵称
    private String headimgurl; // 头像

}