package com.liqiang365.model.em;
/**
 * 提示消息类型枚举
 *
 */
public enum MessageTypeEnum {

    Message_00("00", "成功"),
    Message_01("01", "用户已经有连麦申请"),
    Message_02("02", "当前正在和主播连麦中"),
    Message_03("03", "主播已关闭连麦申请"),
    Message_04("04", "主播PK时暂不能连麦"),
    Message_05("05", "您在该直播间申请的次数已超过%d次"),
    Message_06("06", "当前申请连麦人数已超过限制，请稍后再试"),
    Message_07("07", "您当前有其它房间连麦申请，结束后才能重新申请连麦"),
    Message_08("08", "本次连麦主播修改智慧豆数量为%d智慧豆"),
    Message_09("09", "您当前智慧豆不足，去购买"),
    Message_10("10", "您当前正在和其它主播连麦，结束后才能重新申请连麦");

    private String type;
    private String info;

    MessageTypeEnum(String type, String info) {
        this.type = type;
        this.info = info;
    }

    public String getType() {
        return type;
    }
    public String getInfo() {
        return info;
    }
}
