package com.liqiang365.model.em;

import com.liqiang365.util.valid.ValidUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Description: 订单类型枚举
 * Author:     gaocl
 * Date:       2016/12/7
 * Version:     V1.0.0
 * Update:     更新说明
 */
public enum OrderTypeEnum {

    UPLEVEL("会员升级", "1"),
    RECHARGE("充值余额", "2"),
    LIYANG("李阳专栏", "3"),
    SAVEQUESTION("提出问题", "4"),
    AGREEQUESTION("支持问题", "5"),
    MICROUSER("开通小微店主", "6"),
    SHOPVIP("开通商城会员", "7"),
    GROUPBUYLEADER("开通天天团团长", "8"),
    NEWSUBJECT("新版专栏开通", "9"),
    RECHARGEQB("直播充值强币", "10"),
    ENTERPRISEUPLEVEL("企业版会员升级", "11"),
    RENEWUPLEVEL("会员续费", "12"),
    GROUPBUYUSERRENEWUPLEVEL("团购商会员续费", "13"),
    MEETING("会议购票", "14"),
    SEVENDAYVIP("7天vip", "15"),
    FAMOUSTEACHERCOURSE("名师大舞台课程", "16"),
    COMPANYSEVENDAYVIP("公司邀请码体验7天", "17"),
    REDPACKAGEVIP("新年活动邀请用户送vip", "18"),
    SMS_NOTICE_LEVE_UP("短信提醒用户续费", "19"),
    NEWUSER("新用户大礼包--赠送7天VIP", "21"),
    EIGHT_HUNDRED_AND_EIGHTEEN_DAY("818优惠下单", "22"),
    INVITE_USER("邀请用户", "23"),
    GROUP("团购订单", "24"),
    PLATINUM_VIP("白金会员", "25"),
    SUPER_PLATINUM_VIP("超级白金会员", "26");

    private String name;
    private String value;

    OrderTypeEnum(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public static String getNameByValue(String value) {
        if (ValidUtil.isEmpty(value)) {
            return "";
        }
        OrderTypeEnum[] values = OrderTypeEnum.values();
        for (OrderTypeEnum en : values) {
            if (value.equals(en.getValue())) {
                return en.getName();
            }
        }
        return "";
    }

    public static List<Map<String, Object>> getNameValues() {
        List<Map<String, Object>> result = new ArrayList<>();
        OrderTypeEnum[] values = OrderTypeEnum.values();
        for (OrderTypeEnum en : values) {
            Map<String, Object> entity = new HashMap<>();
            entity.put("name", en.getName());
            entity.put("value", en.getValue());
            result.add(entity);
        }
        return result;
    }


}
