package com.liqiang365.model.em;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 功能描述: 跳转类型枚举类<br>
 * 〈〉
 * @Param:
 * @Return:
 * @Author: susaifei
 * @Date: 2019/5/22 17:57
 */
public enum JumpTypeEnum {
    /**
     *
     */
    DEFAULT("不跳转", "0"),
//2    LIQIANG("李强专区课程或视频", "1"),
//2    SUBJECT("名师讲堂课程或视频", "2"),
//2    YOUTH("中小教育课程或视频", "3"),
//2    BABY("幼儿教育课程或视频", "4"),
//2    DAREN("达人详情页", "5"),
    WEB("H5页面_分享图片", "6"),
//    GLOBAL("全球演讲团视频详情", "7"),
    LIQIANG_INDEX("李强专区", "8"),
    YOUTH_INDEX("育儿早教", "9"),
    BABY_INDEX("中小教育", "10"),
//    SUBJECT_INDEX("名师讲堂首页", "11"),
//    SUBJECT_CATEGORY("名师讲堂分类", "12"),
//    SUBJECT_TEACHER_LIST("名师讲堂老师列表", "13"),
//    SUBJECT_VIP_FREE_LIST("名师讲堂VIP免费列表", "14"),
//    SUBJECT_CHARGE_LIST("名师讲堂收费列表", "15"),
//    SUBJECT_TEACHER_APPLY("名师讲堂名师入驻", "16"),
//    DAREN_INDEX("达人首页", "17"),
    IMAGE("直接跳转显示图片", "18"),
    SHOP("跳转到商城", "19"),
//    DAREN_VIDEO("达人视频页", "20"),
//    GLOBAL_SPEAKING_GROUP("全球演讲团", "21"),
    WALK_TO_GLOBAL("语言学习主页", "22"),
//    TOPIC("资讯", "23"),
    SOLDIER("军人活动页面", "24"),
//3    SEVENDAYVIP("7天vip体验卡", "25"),
//3    ACTIVE_11_SHARE_JUMP("11分享活动跳转", "26"),
//3    ACTIVE_11_APPOINT_LUNBO("11预热活动跳转", "27"),
//3    ACTIVE_11_ADV("11正式活动跳转", "28"),
//    VOCATIONALEDUCATION("职业教育", "29"),
    TEACHERBIGSTAGE("名师大舞台", "30"),
//3    TASTELIFE("品味人生", "31"),
//2    TEACHERBIGSTAGE_VIDEO_COURSE("名师大舞台课程或视频", "32"),
    TEACHERBIGSTAGE_CATEGORY("名师大舞台课程列表页", "33"),
    TEACHERBIGSTAGE_TEACHER_LIST("名师大舞台名师列表", "34"),
    TEACHERBIGSTAGE_ENTER("名师大舞台名师入驻", "35"),
    TEACHERBIGSTAGE_INVITE("名师大舞台名师邀请", "36"),
//    TV_TOPPAGE_TEACHERBIGSTAGE("TV首页名师讲堂", "37"),
//3    HAPPINESSBUSINESSSCHOOL("幸福学院", "38"),
//3    BRAINBUSINESSSCHOOL("全脑商学院", "39"),
    GLOBAL_IELTS("雅思列表页", "40"),
    GLOBAL_TOEFL("托福列表页", "41"),
    GLOBAL_PRACTICALENGLISH("实用英语列表页", "42"),
    GLOBAL_OTHERS("小语种列表页", "43"),
//3    E_BUSINESS("电子商务", "44"),
    ELOCUTIONIST("我是演说家", "45"),
//2    OLDER("104通用版本课程或视频", "46"),
    EDUCATIONONEADDONE("艺术学院（原教育1+1）", "47"),
    WEB_PAGE("跳转H5分享h5链接", "48"),
    BUSINESS_MODULE("90后创新.创业", "49"),
    ACADEMICIAN("院士说科普", "50"),
    GOOD_LIFE("美好生活", "51"),
    PERSONAL_GROWTH("个人成长", "52"),
    WOMENS_FASHION("女性时尚", "53"),
    FAMILY_EDUCATION("家庭教育", "54"),
    LANGUAGE_STUDY("语言学习", "55"),
    ALL_CATEGORIES("首页全部分类", "56"),
    LQ_MARRIAGE_AND_FAMILY("李强专区-婚姻家庭首页", "57"),
    LQ_CHILDREN_EDUCATION("李强专区-子女教育首页", "58"),
    LQ_SPEECH_ELOQUENCE("李强专区-演讲口才首页", "59"),
    LQ_SELF_GROWTH("李强专区-自我成长首页", "60"),
    LQ_HUMAN_RELATIONS("李强专区-人际关系首页", "61"),
    LQ_BUSINESS_MANAGEMENT("李强专区-企业管理首页", "62"),
    LQ_TEAM_BUILDING("李强专区-团队打造首页", "63"),
    LQ_MARKETING("李强专区-市场营销首页", "64"),
    KNOWLEDGE_COURSE("知识点课程首页", "65"),
    SYNCHRONOUS_COURSE("同步课程首页", "66"),
    SYNCHRONOUS_PRACTICE("练一练", "67"),
    INTEREST_IN_EDUCATION("兴趣教育首页", "68"),
    GKZN("高考指南首页", "69"),
    CHINESE_COLLEGE("中文学院", "70"),
    QI_GUAN_TIAN_XIA("企管天下", "71"),
    TRADITION("传统文化", "72"),
    COURSE_PLAY("通用课程播放", "73"),
    LEARNING_METHOD("学习方法", "74"),
    RADIO("电台", "76"),
    LIVE("跳转直播", "77");

    private String name;
    private String value;


    JumpTypeEnum(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public static String getNameByValue(String value) {
        JumpTypeEnum[] jumpTypeEnum = JumpTypeEnum.values();
        for (JumpTypeEnum anJumpTypeEnum : jumpTypeEnum) {
            if (anJumpTypeEnum.getValue().equals(value)) {
                return anJumpTypeEnum.getName();
            }
        }
        return JumpTypeEnum.DEFAULT.getName();
    }

    public static List<Map<String, String>> getKeyValues() {
        List<Map<String, String>> result = new ArrayList<>();
        JumpTypeEnum[] enums = JumpTypeEnum.values();
        for (JumpTypeEnum e : enums) {
            Map<String, String> ee = new HashMap<>();
            ee.put("name", e.getName());
            ee.put("value", e.getValue());
            result.add(ee);
        }
        return result;
    }
}
