package com.liqiang365.model.em;

/**
 * @author 严聪
 * @description
 * @return
 * @date 2019/12/28 14:06
 */
public enum ShuMeiStateEnum {


    PASS("正常内容，建议直接放行", "PASS"),
    REVIEW("可疑内容建议人工审核", "REVIEW"),
    REJECT("违规内容建议直接拦截", "REJECT");

    private String name;
    private String value;


    ShuMeiStateEnum(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }
}
