package com.liqiang365.version;

import com.liqiang365.util.valid.ValidUtil;
import org.springframework.web.servlet.mvc.condition.RequestCondition;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 严聪
 * @description
 * @date 2019/3/29 16:48
 */
public class ApiVersionCondition implements RequestCondition<ApiVersionCondition> {

    // 路径中版本的前缀， 这里用 /v[1-9]/的形式
    private final static Pattern VERSION_PREFIX_PATTERN = Pattern.compile("v(\\d+)/");


    public int getApiVersion() {
        return apiVersion;
    }

    /**
     * api的版本
     */
    private int apiVersion;

    public ApiVersionCondition(int apiVersion) {
        this.apiVersion = apiVersion;
    }

    /**
     * @return
     * @description 根据request查找匹配到的筛选条件
     * @date 2019/3/29 16:51
     * @author 严聪
     */
    @Override
    public ApiVersionCondition combine(ApiVersionCondition apiVersionCondition) {
        // 采用最后定义优先原则，则方法上的定义覆盖类上面的定义
        return new ApiVersionCondition(apiVersionCondition.getApiVersion());

    }

    /**
     * @param httpServletRequest
     * @return com.liqiang365.test.ApiVersionCondition
     * @description
     * @date 2019/3/29 16:51
     * @author 严聪
     */
    @Override
    public ApiVersionCondition getMatchingCondition(HttpServletRequest httpServletRequest) {
        String requestUrl = httpServletRequest.getRequestURL().toString();
        String referer = httpServletRequest.getHeader("referer");
        String version = httpServletRequest.getHeader("apiVersion");
        if (ValidUtil.isNotEmpty(referer)) {
            if (referer.indexOf("swagger") > -1) {
                return this;
            } else {
                if (ValidUtil.isNotEmpty(version)) {
                    version = version.replace("V", "");
                    version = version.replace("v", "");
                    int apiVersion = Integer.parseInt(version);
                    if (apiVersion == this.apiVersion) {
                        return this;
                    }
                } else {
                    String method = httpServletRequest.getMethod();
                    if ("OPTIONS".equalsIgnoreCase(method)) {
                        return this;
                    }
                    Matcher m = VERSION_PREFIX_PATTERN.matcher(requestUrl);
                    if (m.find()) {
                        Integer apiVersion = Integer.valueOf(m.group(1));
                        if (apiVersion == this.apiVersion) {
                            return this;
                        }
                        return null;
                    }
                }
            }
        } else {
            if (ValidUtil.isNotEmpty(version)) {
                version = version.replace("V", "");
                version = version.replace("v", "");
                int apiVersion = Integer.parseInt(version);
                if (apiVersion == this.apiVersion) {
                    return this;
                }
            }
        }
        return null;
    }

    @Override
    public int compareTo(ApiVersionCondition apiVersionCondition, HttpServletRequest request) {
        return apiVersionCondition.getApiVersion() - this.apiVersion;
    }
}
