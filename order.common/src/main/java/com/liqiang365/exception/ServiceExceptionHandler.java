package com.liqiang365.exception;

import hotax.core.ReturnBean;
import hotax.web.exception.HotaxExceptionhandler;
import org.springframework.stereotype.Component;

@Component
public class ServiceExceptionHandler implements HotaxExceptionhandler
{


	@Override
	public ReturnBean ProdException(ReturnBean rb, Exception ex) {
		return globalExcetpion(rb,ex);
	}

	@Override
	public ReturnBean TestException(ReturnBean rb, Exception ex) {
		return globalExcetpion(rb,ex);
	}


	@Override
	public ReturnBean CustException(ReturnBean rb, Exception ex) {
		return rb;
	}

	private ReturnBean globalExcetpion(ReturnBean rb, Exception ex){
		if(ex instanceof ServiceException){
			ServiceException serviceException =(ServiceException)ex;
			if(serviceException.getErrorCode()== null){
				rb.setCode("40010");
			}else{
				rb.setCode(serviceException.getErrorCode());
			}
			rb.setInfo(serviceException.getErrorMessage());
			return rb;
		}
		return null;
	}
}
