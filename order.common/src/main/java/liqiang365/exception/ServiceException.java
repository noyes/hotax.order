package liqiang365.exception;

import com.liqiang365.model.em.CodeEnum;
import hotax.core.exception.BaseException;

/**
 * @author whl
 * @Description 定义公共ServiceException
 * @date 2017-10-10
 */
public class ServiceException extends BaseException {
    private static final long serialVersionUID = 1L;

    private String errorMessage;
    private String errorCode;


    /**
     * 用给定的异常信息构造新实例。
     *
     * @param errorMessage 异常信息。
     */
    public ServiceException(String errorMessage) {
        super((String) null);
        this.errorMessage = errorMessage;
    }

    public ServiceException(CodeEnum codeEnum) {
        super((String) null);
        this.errorMessage = codeEnum.getName();
        this.errorCode = codeEnum.getCode();
    }

    public ServiceException(CodeEnum codeEnum,String errorMessage) {
        super((String) null);
        this.errorMessage = errorMessage;
        this.errorCode = codeEnum.getCode();
    }

    public ServiceException(String errorCode,String errorMessage) {
        super((String) null);
        this.errorMessage = errorMessage;
        this.errorCode = errorCode;
    }



    /**
     * 返回异常信息。
     *
     * @return 异常信息。
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * 返回错误代码的字符串表示。
     *
     * @return 错误代码的字符串表示。
     */
    public String getErrorCode() {
        return errorCode;
    }



    @Override
    public String getMessage() {
        return getErrorMessage()
                + "\n[ErrorCode]: " + getErrorCode();
    }

}
