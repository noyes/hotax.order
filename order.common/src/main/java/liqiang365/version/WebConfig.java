package liqiang365.version;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

//@SpringBootConfiguration
//@ConditionalOnClass({ ApiVersion.class })
//public class WebConfig{
//    @Bean
//    public RequestMappingHandlerMapping customRequestMappingHandlerMapping() {
//        CustomRequestMappingHandlerMapping mappingHandlerMapping = new CustomRequestMappingHandlerMapping();
//        mappingHandlerMapping.setOrder(Ordered.HIGHEST_PRECEDENCE);//设置排序
//        return mappingHandlerMapping;
//    }
//
//}
