/**
* @filename:LiveLockRef 2020年01月02日
* @project live.api  V1.0
* Copyright(c) 2020 严聪 Co. Ltd.
 * All right reserved.
*/
package liqiang365.model.entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;

/**
* <p>自动生成工具：mybatis-generator</p>
*
* <p>说明： 用户实体类</P>
* @version: V1.0
* @author: 严聪
*
*/
@Table(name="live_lock_ref")
@Data
@ApiModel
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class LiveLockRef  implements Serializable {

private static final long serialVersionUID = 1580721074078L;

@Id
@ApiModelProperty(name = "id" , value = "")
private Integer id;
@ApiModelProperty(name = "liveId" , value = "直播id")
private String liveId;
@ApiModelProperty(name = "uid" , value = "用户id")
private String uid;


}
