/**
* @filename:LiveTag 2019年11月22日
    * @project liqiang365  V1.0
    * Copyright(c) 2020 严聪 Co. Ltd.
    * All right reserved.
    */
    package liqiang365.model.entity;
    import javax.persistence.Id;
    import javax.persistence.Table;
    import javax.persistence.Transient;
    import com.fasterxml.jackson.annotation.JsonFormat;
    import io.swagger.annotations.ApiModel;
    import io.swagger.annotations.ApiModelProperty;
    import lombok.*;
    import org.springframework.format.annotation.DateTimeFormat;
    import java.io.Serializable;

/**
* <p>自动生成工具：mybatis-dsc-generator</p>
*
* <p>说明： 用户实体类</P>
* @version: V1.0
* @author: 严聪
*
*/
@Table(name="live_tag")
@Data
@ApiModel
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class LiveTag  implements Serializable {

private static final long serialVersionUID = 1574414552477L;

    @Id
    @ApiModelProperty(name = "id" , value = "主键")
    private String id;
    @ApiModelProperty(name = "name" , value = "标签名称")
    private String name;
    @ApiModelProperty(name = "backgroundColor" , value = "背景颜色")
    private String backgroundColor;
    @ApiModelProperty(name = "prefixColor" , value = "前置颜色")
    private String prefixColor;
    @ApiModelProperty(name = "sort" , value = "排序")
    private Integer sort;
    @ApiModelProperty(name = "state" , value = "启用状态  0 未启用 1 启用")
    private String state;
    @ApiModelProperty(name = "tid" , value = "站点ID")
    private String tid;
    @ApiModelProperty(name = "createTime" , value = "创建时间")
    private String createTime;
    @ApiModelProperty(name = "updateTime" , value = "更新时间")
    private String updateTime;
    @ApiModelProperty(name = "deleteState" , value = "删除状态 0=未删除 1=已删除")
    private String deleteState;
    @ApiModelProperty(name = "createUid" , value = "创建者id")
    private String createUid;
    @ApiModelProperty(name = "lastUpdateUid" , value = "最后更新者id")
    private String lastUpdateUid;

}
