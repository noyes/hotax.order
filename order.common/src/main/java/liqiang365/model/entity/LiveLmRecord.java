/**
 * @filename:LiveImRecord 2020年01月02日
 * @project live.api  V1.0
 * Copyright(c) 2020 严聪 Co. Ltd.
 * All right reserved.
 */
package liqiang365.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>自动生成工具：mybatis-generator</p>
 *
 * <p>说明： 用户实体类</P>
 *
 * @version: V1.0
 * @author: 严聪
 */
@Table(name = "live_lm_record")
@Data
@ApiModel
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class LiveLmRecord implements Serializable {

    private static final long serialVersionUID = 1586314867382L;

    @Id
    @ApiModelProperty(name = "id", value = "主键")
    private String id;
    @ApiModelProperty(name = "createTime", value = "创建时间")
    private String createTime;
    @ApiModelProperty(name = "deleteState", value = "删除：1=删除；0=未删除；")
    private String deleteState;
    @ApiModelProperty(name = "uid", value = "连麦申请用户")
    private String uid;
    @ApiModelProperty(name = "liveId", value = "直播")
    private String liveId;
    @ApiModelProperty(name = "roomId", value = "房间")
    private Integer roomId;
    @ApiModelProperty(name = "costQb", value = "花费智慧豆")
    private Integer costQb;
    @ApiModelProperty(name = "duration", value = "申请连麦时长（秒）")
    private Integer duration;
    @ApiModelProperty(name = "giftQb", value = "赠送智慧豆")
    private Integer giftQb;
    @ApiModelProperty(name = "state", value = "状态：1=连麦中；0=正常结束；")
    private Integer state;
    @ApiModelProperty(name = "liveName", value = "直播名")
    private String liveName;
    @ApiModelProperty(name = "startTime", value = "开始时间")
    private String startTime;
    @ApiModelProperty(name = "endTime", value = "结束时间")
    private String endTime;
    @ApiModelProperty(name = "applyForId", value = "直播申请id 外键live_lm_apply_for主键")
    private String applyForId;
    @ApiModelProperty(name = "endUid", value = "结束人")
    private String endUid;
    @ApiModelProperty(name = "remark", value = "备注")
    private String remark;
    @ApiModelProperty(name = "type", value = "连麦类型：1=音频；2=视频；")
    private Integer type;
    @ApiModelProperty(name = "liveUid", value = "主播")
    private String liveUid;
    @ApiModelProperty(name = "proportion", value = "主播分成比例(0.5)")
    private BigDecimal proportion;
    @ApiModelProperty(name = "income_qb", value = "主播智慧豆收益")
    private Integer incomeQb;
    @Transient
    @ApiModelProperty(name = "userName", value = "用户昵称")
    private String userName;
    @Transient
    @ApiModelProperty(name = "userAccount", value = "用户账号")
    private String userAccount;
    @ApiModelProperty(value = "申请扣豆流水记录")
    private String liveQbRecordId;


}
