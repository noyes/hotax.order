/**
 * @filename:LiveUser 2020年01月02日
 * @project live.api  V1.0
 * Copyright(c) 2020 严聪 Co. Ltd.
 * All right reserved.
 */
package liqiang365.model.entity;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * <p>自动生成工具：mybatis-generator</p>
 *
 * <p>说明： 用户实体类</P>
 *
 * @version: V1.0
 * @author: 严聪
 */
@Table(name = "live_user")
@Data
@ApiModel
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class LiveUser implements Serializable {

    private static final long serialVersionUID = 1587704829985L;

    @Id
    @ApiModelProperty(name = "id", value = "")
    private String id;
    @ApiModelProperty(name = "name", value = "机器人昵称")
    private String name;
    @JSONField(serialize = false)
    @ApiModelProperty(name = "account", value = "机器人账号")
    private String account;
    @ApiModelProperty(name = "portrait", value = "机器人头像")
    private String portrait;

}
