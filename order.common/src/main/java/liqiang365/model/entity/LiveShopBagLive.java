package liqiang365.model.entity;

import hotax.core.valid.UpdateCheck;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;

@ApiModel(value ="live_shop_bag_live", description ="live_shop_bag_live" )
public class LiveShopBagLive implements Serializable {
    /**
     *
     */
    @ApiModelProperty(value ="", required = true)
    @NotBlank(message="字段id不能为空",groups ={UpdateCheck.class})
    private String id;

    /**
     *
     */
    @ApiModelProperty(value ="", required = true)
    @NotBlank(message="字段uid不能为空")
    private String uid;

    /**
     * 直播id
     */
    @ApiModelProperty(value ="直播id", required = false)
    private String lid;

    /**
     * 商品id
     */
    @ApiModelProperty(value ="商品id", required = false)
    private String gid;

    /**
     * 商品名称
     */
    @ApiModelProperty(value ="商品名称", required = false)
    private String gname;

    /**
     * 商品图片地址
     */
    @ApiModelProperty(value ="商品图片地址", required = false)
    private String gurl;

    /**
     * 商品价格
     */
    @ApiModelProperty(value ="商品价格", required = false)
    private BigDecimal gprice;

    /**
     *
     */
    @ApiModelProperty(value ="", required = false)
    private Integer cdate;

    /**
     * add by 苏 During 直播带课
     */
    @ApiModelProperty(value ="讲解状态：1=讲解中；0=讲解；", required = false)
    private String going;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid == null ? null : uid.trim();
    }

    public String getLid() {
        return lid;
    }

    public void setLid(String lid) {
        this.lid = lid == null ? null : lid.trim();
    }

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid == null ? null : gid.trim();
    }

    public String getGname() {
        return gname;
    }

    public void setGname(String gname) {
        this.gname = gname == null ? null : gname.trim();
    }

    public String getGurl() {
        return gurl;
    }

    public void setGurl(String gurl) {
        this.gurl = gurl == null ? null : gurl.trim();
    }

    public BigDecimal getGprice() {
        return gprice;
    }

    public void setGprice(BigDecimal gprice) {
        this.gprice = gprice;
    }

    public Integer getCdate() {
        return cdate;
    }

    public void setCdate(Integer cdate) {
        this.cdate = cdate;
    }

    public String getGoing() {
        return going;
    }

    public void setGoing(String going) {
        this.going = going;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", uid=").append(uid);
        sb.append(", lid=").append(lid);
        sb.append(", gid=").append(gid);
        sb.append(", gname=").append(gname);
        sb.append(", gurl=").append(gurl);
        sb.append(", gprice=").append(gprice);
        sb.append(", cdate=").append(cdate);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}