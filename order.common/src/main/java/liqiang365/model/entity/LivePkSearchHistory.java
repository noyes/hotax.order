package liqiang365.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 直播PK搜索主播记录
 *
 * @author 苏
 * @date 2020-03-20 18:56:11
 */
@Table(name = "live_pk_search_history")
@Data
@ApiModel
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class LivePkSearchHistory implements Serializable {
    private static final long serialVersionUID = 3381031503289675356L;

    @Id
    @ApiModelProperty(name = "id", value = "搜索历史主键")
    private String id;
    @ApiModelProperty(name = "createTime", value = "创建时间")
    private String createTime;
    @ApiModelProperty(name = "uid", value = "搜索用户id")
    private String uid;
    @ApiModelProperty(name = "content", value = "搜索内容")
    private String content;
}
