package liqiang365.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Table;
import javax.persistence.Transient;

@Table(name = "live_public_message")
@Data
@ApiModel
@EqualsAndHashCode(callSuper = true)
public class LivePublicMessage  extends BaseEntity{

    @ApiModelProperty(value="创建者id")
    private String createUid;

    @ApiModelProperty(value="最后更新者id")
    private String lastUpdateUid;

    @ApiModelProperty(value="发送人")
    private String sendUser;

    @ApiModelProperty(value="发送内容")
    private String content;

    @ApiModelProperty(value="发送时间")
    private String sendTime;

    @ApiModelProperty(value="1立即发送 2定时发送 3 定时已发送")
    private String sendState;

    @ApiModelProperty(value="发送次数")
    private Integer sendFrequency;

    @ApiModelProperty(value="发送间隔时间秒")
    private Integer sendInterval;

    @ApiModelProperty(value="悬停时间秒")
    private Integer hoverTime;

    @ApiModelProperty(value="1立即发送（已发送） 2定时发送（待发送）")
    @Transient
    private String sendSet;

    @ApiModelProperty(value="1仅一次 2自定义")
    @Transient
    private String sendFrequencySet;

}