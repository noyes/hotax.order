/**
* @filename:LiveAccusationType 2019年11月22日
    * @project liqiang365  V1.0
    * Copyright(c) 2020 严聪 Co. Ltd.
    * All right reserved.
    */
    package liqiang365.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
* <p>自动生成工具：mybatis-dsc-generator</p>
*
* <p>说明： 用户实体类</P>
* @version: V1.0
* @author: 严聪
*
*/
@Table(name="live_accusation_type")
@Data
@ApiModel
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class LiveAccusationType  implements Serializable {

    private static final long serialVersionUID = 1574414724425L;

    @Id
    @ApiModelProperty(name = "id", value = "类型id")
    private String id;
    @ApiModelProperty(name = "name", value = "名称")
    private String name;
    @ApiModelProperty(name = "seq", value = "排序号")
    private Integer seq;
    @ApiModelProperty(name = "publishState", value = "发布状态0=未发布1=已发布")
    private Integer publishState;
    @ApiModelProperty(name = "createTime", value = "创建时间")
    private String createTime;
    @ApiModelProperty(name = "updateTime", value = "更新时间")
    private String updateTime;
    @ApiModelProperty(name = "tid", value = "站点id")
    private String tid;
    @ApiModelProperty(name = "deleteState", value = "删除状态0=未删除1=删除")
    private Integer deleteState;

}