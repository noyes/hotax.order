/**
* @filename:LiveRechargeRule 2019年11月22日
    * @project liqiang365  V1.0
    * Copyright(c) 2020 严聪 Co. Ltd.
    * All right reserved.
    */
    package liqiang365.model.entity;
    import javax.persistence.Id;
    import javax.persistence.Table;
    import javax.persistence.Transient;
    import com.fasterxml.jackson.annotation.JsonFormat;
    import io.swagger.annotations.ApiModel;
    import io.swagger.annotations.ApiModelProperty;
    import lombok.*;
    import org.springframework.format.annotation.DateTimeFormat;
    import java.io.Serializable;
        import java.math.BigDecimal;

/**
* <p>自动生成工具：mybatis-dsc-generator</p>
*
* <p>说明： 用户实体类</P>
* @version: V1.0
* @author: 严聪
*
*/
@Table(name="live_recharge_rule")
@Data
@ApiModel
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class LiveRechargeRule  implements Serializable {

private static final long serialVersionUID = 1574414618280L;

    @Id
    @ApiModelProperty(name = "id" , value = "主键")
    private String id;
    @ApiModelProperty(name = "ruleName" , value = "规则名称")
    private String ruleName;
    @ApiModelProperty(name = "price" , value = "价格(元)")
    private BigDecimal price;
    @ApiModelProperty(name = "qb" , value = "智慧豆")
    private Long qb;
    @ApiModelProperty(name = "giveQb" , value = "赠送智慧豆")
    private Integer giveQb;
    @ApiModelProperty(name = "publishState" , value = "发布状态0=暂不发布1=立即发布")
    private Integer publishState;
    @ApiModelProperty(name = "seq" , value = "排序号")
    private Integer seq;
    @ApiModelProperty(name = "tid" , value = "站点id")
    private String tid;
    @ApiModelProperty(name = "createTime" , value = "创建时间")
    private String createTime;
    @ApiModelProperty(name = "updateTime" , value = "更新时间")
    private String updateTime;
    @ApiModelProperty(name = "deleteState" , value = "0=未删除1=已删除")
    private Integer deleteState;
}
