/**
* @filename:LiveReview 2019年11月22日
    * @project liqiang365  V1.0
    * Copyright(c) 2020 严聪 Co. Ltd.
    * All right reserved.
    */
    package liqiang365.model.entity;
    import javax.persistence.Id;
    import javax.persistence.Table;
    import javax.persistence.Transient;
    import com.fasterxml.jackson.annotation.JsonFormat;
    import io.swagger.annotations.ApiModel;
    import io.swagger.annotations.ApiModelProperty;
    import lombok.*;
    import org.springframework.format.annotation.DateTimeFormat;
    import java.io.Serializable;
    import java.util.List;

/**
    * <p>自动生成工具：mybatis-dsc-generator</p>
    *
    * <p>说明： 用户实体类</P>
    * @version: V1.0
    * @author: 严聪
    *
    */
    @Table(name="live_review")
    @Data
    @ApiModel
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    @ToString
    public class LiveReview  implements Serializable {

    private static final long serialVersionUID = 1574414602057L;

        @Id
        @ApiModelProperty(name = "id" , value = "主键")
        private String id;
        @ApiModelProperty(name = "liveId" , value = "直播id")
        private String liveId;
        @ApiModelProperty(name = "roomId" , value = "房间id/")
        private Integer roomId;
        @ApiModelProperty(name = "liveName" , value = "直播名称")
        private String liveName;
        @ApiModelProperty(name = "streamName" , value = "流名称")
        private String streamName;
        @ApiModelProperty(name = "taskId" , value = "腾讯 任务ID")
        private Integer taskId;
        @ApiModelProperty(name = "openState" , value = "是否开启回放")
        private String openState;
        @ApiModelProperty(name = "showFlag" , value = "是否显示 1显示 0 隐藏")
        private String showFlag;
        @ApiModelProperty(name = "state" , value = "状态  1已存储 2转码中 3回放中")
        private String state;
        @ApiModelProperty(name = "url" , value = "播放地址")
        private String url;
        @ApiModelProperty(name = "tid" , value = "站点ID")
        private String tid;
        @ApiModelProperty(name = "createTime" , value = "创建时间")
        private String createTime;
        @ApiModelProperty(name = "updateTime" , value = "更新时间")
        private String updateTime;
        @ApiModelProperty(name = "deleteState" , value = "删除状态 0=未删除 1=已删除")
        private String deleteState;
        @ApiModelProperty(name = "createUid" , value = "创建者id")
        private String createUid;
        @ApiModelProperty(name = "lastUpdateUid" , value = "最后更新者id")
        private String lastUpdateUid;

        @Transient
        @ApiModelProperty(name = "urlList" , value = "预览的播放列表")
        private List<String> urlList;
}
