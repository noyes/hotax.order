package liqiang365.model.entity;

import hotax.core.valid.UpdateCheck;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.validation.constraints.NotBlank;

@ApiModel(value ="live_shop_fans", description ="live_shop_fans" )
public class LiveShopFans implements Serializable {
    /**
     * 主播用户id
     */
    @ApiModelProperty(value ="主播用户id", required = true)
    @NotBlank(message="字段uid不能为空",groups ={UpdateCheck.class})
    private String uid;

    /**
     * 0:不是米粉 1:是米粉 2:后台设置米粉 3
     */
    @ApiModelProperty(value ="0:不是米粉1:是米粉2:后台设置米粉3", required = false)
    private Integer status;

    /**
     * 0:来源商城 1:来源后台 
     */
    @ApiModelProperty(value ="0:来源商城1:来源后台", required = false)
    private Integer source;

    /**
     * 创建时间
     */
    @ApiModelProperty(value ="创建时间", required = false)
    private Integer cdata;

    private static final long serialVersionUID = 1L;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid == null ? null : uid.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSource() {
        return source;
    }

    public void setSource(Integer source) {
        this.source = source;
    }

    public Integer getCdata() {
        return cdata;
    }

    public void setCdata(Integer cdata) {
        this.cdata = cdata;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", uid=").append(uid);
        sb.append(", status=").append(status);
        sb.append(", source=").append(source);
        sb.append(", cdata=").append(cdata);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}