package liqiang365.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "live_user_tag")
@Data
@ApiModel
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class LiveUserTag implements Serializable {

    @Id
    @ApiModelProperty(name = "id", value = "主键")
    private String id;
    @ApiModelProperty(name = "deleteState", value = "删除状态 0=未删除 1=已删除")
    private String deleteState;
    @ApiModelProperty(name = "createTime", value = "创建时间")
    private String createTime;
    @ApiModelProperty(name = "updateTime", value = "更新时间")
    private String updateTime;
    @ApiModelProperty(name = "tid", value = "站点ID")
    private String tid;
    @ApiModelProperty(name = "uid", value = "打标签用户")
    private String uid;
    @ApiModelProperty(name = "targetUid", value = "被打标签用户")
    private String targetUid;
    @ApiModelProperty(name = "tagId", value = "标签ID")
    private String tagId;
}
