/**
 * @filename:LiveCourse 2020年05月09日
 * @project live.api  V1.0
 * Copyright(c) 2020 zhuxl Co. Ltd.
 * All right reserved.
 */
package liqiang365.model.entity;

import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>自动生成工具：mybatis-generator</p>
 *
 * <p>说明： 直播课程实体类</P>
 * @version: V1.0
 * @author: zhuxl
 *
 */
@Table(name = "live_course")
@Data
@ApiModel
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class LiveCourse implements Serializable {

    private static final long serialVersionUID = 1588988623119L;

    @Id
    @ApiModelProperty(name = "id", value = "课程表主键")
    private String id;
    @ApiModelProperty(name = "createTime", value = "创建时间")
    private String createTime;
    @ApiModelProperty(name = "deleteState", value = "删除状态 1=已删除 0=未删除")
    private String deleteState;
    @ApiModelProperty(name = "name", value = "课程名称")
    private String name;
    @ApiModelProperty(name = "price", value = "课程价格")
    private BigDecimal price;
    @ApiModelProperty(name = "coverImgUrl", value = "课程封面图")
    private String coverImgUrl;
    @ApiModelProperty(name = "takeGoodsReward", value = "直播带货奖励")
    private BigDecimal takeGoodsReward;
    @ApiModelProperty(name = "teacher", value = "主讲人")
    private String teacher;
    @ApiModelProperty(name = "courseType", value = "1=视频 2=音频")
    private Integer courseType;


}
