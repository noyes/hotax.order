/**
 * @filename:LiveIncome 2020年01月02日
 * @project live.api  V1.0
 * Copyright(c) 2020 严聪 Co. Ltd.
 * All right reserved.
 */
package liqiang365.model.entity;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>自动生成工具：mybatis-generator</p>
 *
 * <p>说明： 用户实体类</P>
 * @version: V1.0
 * @author: 严聪
 *
 */
@Table(name = "live_income")
@Data
@ApiModel
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class LiveIncome implements Serializable {

    private static final long serialVersionUID = 1589190322040L;

    @Id
    @ApiModelProperty(name = "id", value = "主键")
    private String id;
    @ApiModelProperty(name = "liveId", value = "直播")
    private String liveId;
    @ApiModelProperty(name = "liveUid", value = "主播")
    private String liveUid;
    @ApiModelProperty(name = "uid", value = "购买人")
    private String uid;
    @ApiModelProperty(name = "account", value = "购买人账号")
    private String account;
    @ApiModelProperty(name = "goodsName", value = "商品名")
    private String goodsName;
    @ApiModelProperty(name = "goodsPrice", value = "商品价格")
    private BigDecimal goodsPrice;
    @ApiModelProperty(name = "incomeType", value = "收益类型：1=365会员；2=壹企购会员；3=课程；4=商品；")
    private Integer incomeType;
    @ApiModelProperty(name = "income", value = "收益金额")
    private BigDecimal income;
    @ApiModelProperty(name = "status", value = "收益状态：0=待入账；1=已入账；2=已退款；")
    private Integer status;
    @ApiModelProperty(name = "paytype", value = "支付方式")
    private Integer paytype;
    @ApiModelProperty(name = "createTime", value = "支付时间")
    private String createTime;
    @ApiModelProperty(name = "orderId", value = "订单编号")
    private String orderId;
    @ApiModelProperty(name = "liveName", value = "直播名")
    private String liveName;
    @ApiModelProperty(name = "roomId" , value = "房间号")
    private String roomId;
    @ApiModelProperty(name = "anchorName" , value = "主播昵称")
    private String anchorName;


}
