/**
 * @filename:LiveStat 2019年11月22日
 * @project liqiang365  V1.0
 * Copyright(c) 2020 严聪 Co. Ltd.
 * All right reserved.
 */
package liqiang365.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>自动生成工具：mybatis-dsc-generator</p>
 *
 * <p>说明： 用户实体类</P>
 *
 * @version: V1.0
 * @author: 严聪
 */
@Table(name = "live_stat")
@Data
@ApiModel
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@EqualsAndHashCode(callSuper = true)
public class LiveStat extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1574428621816L;

    @Id
    @ApiModelProperty(name = "id", value = "")
    private String id;
    @ApiModelProperty(name = "createTime", value = "创建时间")
    private String createTime;
    @ApiModelProperty(name = "updateTime", value = "更新时间")
    private String updateTime;
    @ApiModelProperty(name = "deleteState", value = "删除状态")
    private String deleteState;
    @ApiModelProperty(name = "countWatch", value = "实际观看人数")
    private Integer countWatch;
    @ApiModelProperty(name = "countRobot", value = "机器人数")
    private Integer countRobot;
    @ApiModelProperty(name = "countHot", value = "人气")
    private Integer countHot;
    @ApiModelProperty(name = "countAppointmentCurrent", value = "实际预约人数")
    private Integer countAppointmentCurrent;
    @ApiModelProperty(name = "countAppointmentRobot", value = "虚拟预约人数")
    private Integer countAppointmentRobot;
    @ApiModelProperty(name = "qb", value = "直播礼物")
    private Integer qb;
    @ApiModelProperty(name = "countAttention", value = "新增粉丝数量")
    private Integer countAttention;
    @ApiModelProperty(name = "countReg", value = "注册数")
    private Integer countReg;
    @ApiModelProperty(name = "countVip", value = "升级VIP数")
    private Integer countVip;
    @ApiModelProperty(name = "countGroup", value = "升级团购数")
    private Integer countGroup;
    @ApiModelProperty(name = "liveNum", value = "直播号")
    private Integer liveNum;
    @ApiModelProperty(name = "roomId", value = "房间号/主播ID")
    private Integer roomId;
    @ApiModelProperty(name = "reviewQb", value = "回放礼物")
    private Integer reviewQb;
    @ApiModelProperty(name = "totalReviews", value = "回放总浏览数")
    private Integer totalReviews;
    @ApiModelProperty(name = "countUid", value = "打赏人数")
    @Transient
    private Integer countUid;
    @ApiModelProperty(name = "saleCourseCommission", value = "带课佣金")
    private BigDecimal saleCourseCommission;

}
