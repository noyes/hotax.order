package liqiang365.model.entity;

import lombok.Data;

import java.util.List;

@Data
public class ListWithTotal<T> {


    private List<T> list;

    /**
     * 总行数
     */
    private int total;
    private int pageNum;
    private int pageSize;
    private int size;
}

