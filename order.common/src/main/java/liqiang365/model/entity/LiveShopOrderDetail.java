package liqiang365.model.entity;

import hotax.core.valid.UpdateCheck;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.validation.constraints.NotBlank;

@ApiModel(value ="live_shop_order_detail", description ="live_shop_order_detail" )
public class LiveShopOrderDetail implements Serializable {
    /**
     * 订单明细id
     */
    @ApiModelProperty(value ="订单明细id", required = true)
    @NotBlank(message="字段id不能为空",groups ={UpdateCheck.class})
    private String id;

    /**
     * 订单id
     */
    @ApiModelProperty(value ="订单id", required = true)
    @NotBlank(message="字段oid不能为空")
    private String oid;

    /**
     * 商品id
     */
    @ApiModelProperty(value ="商品id", required = true)
    @NotBlank(message="字段goods不能为空")
    private String goods;

    /**
     * 商品数量
     */
    @ApiModelProperty(value ="商品数量", required = false)
    private Integer num;

    /**
     * 商品名称
     */
    @ApiModelProperty(value ="商品名称", required = false)
    private String gname;

    /**
     * 收益
     */
    @ApiModelProperty(value ="收益", required = false)
    private BigDecimal income;

    /**
     * 单价
     */
    @ApiModelProperty(value ="单价", required = false)
    private BigDecimal price;

    /**
     * 商品图片url
     */
    @ApiModelProperty(value ="商品图片url", required = false)
    private String gurl;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid == null ? null : oid.trim();
    }

    public String getGoods() {
        return goods;
    }

    public void setGoods(String goods) {
        this.goods = goods == null ? null : goods.trim();
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getGname() {
        return gname;
    }

    public void setGname(String gname) {
        this.gname = gname == null ? null : gname.trim();
    }

    public BigDecimal getIncome() {
        return income;
    }

    public void setIncome(BigDecimal income) {
        this.income = income;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getGurl() {
        return gurl;
    }

    public void setGurl(String gurl) {
        this.gurl = gurl == null ? null : gurl.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", oid=").append(oid);
        sb.append(", goods=").append(goods);
        sb.append(", num=").append(num);
        sb.append(", gname=").append(gname);
        sb.append(", income=").append(income);
        sb.append(", price=").append(price);
        sb.append(", gurl=").append(gurl);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}