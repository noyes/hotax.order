package liqiang365.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * 直播用户数据
 * @date 2020-03-18 14:37:33
 * @author 苏
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "直播用户数据")
@Builder
@Table(name = "live_user_info")
public class LiveUserInfo {
    /**
     * 用户ID
     */
    @Id
    @Column(name = "uid")
    @ApiParam("用户ID")
    @ApiModelProperty(value="用户ID")
    private String uid;

    /**
     * 昵称
     */
    @Column(name = "nickname")
    @ApiParam("昵称")
    @ApiModelProperty(value="昵称")
    private String nickname;

    /**
     * 头像
     */
    @Column(name = "user_headimg")
    @ApiParam("头像")
    @ApiModelProperty(value="头像")
    private String userHeadimg;

    /**
     * 账号
     */
    @Column(name = "user_phone")
    @ApiParam("账号")
    @ApiModelProperty(value="账号")
    private String userPhone;

}
