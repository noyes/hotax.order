package liqiang365.model.entity;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author 孙冠杰
 * @classname LiveGiftRechargeRecord
 * @description 礼物充值记录
 * @date 2019/11/22 10:28
 */
@Table(name = "live_gift_recharge_record")
@Data
public class LiveGiftRechargeRecord {
    /**
     * 主键序号
     */
    @Id
    @Column(name = "id")
    @ApiParam("主键序号")
    @ApiModelProperty(value="主键序号")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 订单编号
     */
    @Column(name = "order_no")
    @ApiParam("订单编号")
    @ApiModelProperty(value="订单编号")
    private String orderNo;

    /**
     * 用户id
     */
    @Column(name = "uid")
    @ApiParam("用户id")
    @ApiModelProperty(value="用户id")
    private String uid;

    /**
     * 用户手机号
     */
    @Column(name = "mobile_no")
    @ApiParam("用户手机号")
    @ApiModelProperty(value="用户手机号")
    private String mobileNo;

    /**
     * 用户编号
     */
    @Column(name = "user_no")
    @ApiParam("用户编号")
    @ApiModelProperty(value="用户编号")
    private Integer userNo;

    /**
     * 会员昵称
     */
    @Column(name = "user_nickname")
    @ApiParam("会员昵称")
    @ApiModelProperty(value="会员昵称")
    private String userNickname;

    /**
     * 会员昵称
     */
    @Column(name = "rechargeRule_id")
    @ApiParam("充值规则id")
    @ApiModelProperty(value="充值规则id")
    private String rechargeRuleId;

    /**
     * 订单金额
     */
    @Column(name = "order_money")
    @ApiParam("订单金额")
    @ApiModelProperty(value="订单金额")
    private BigDecimal orderMoney;

    /**
     * 兑换虚拟币
     */
    @Column(name = "exchange_virtual")
    @ApiParam("兑换虚拟币")
    @ApiModelProperty(value="兑换虚拟币")
    private Integer exchangeVirtual;

    /**
     * 赠送虚拟币
     */
    @Column(name = "give_virtual")
    @ApiParam("赠送虚拟币")
    @ApiModelProperty(value="赠送虚拟币")
    private Integer giveVirtual;

    /**
     * 支付方式1=微信2=支付宝
     */
    @Column(name = "pay_type")
    @ApiParam("支付方式1=支付宝2=微信")
    @ApiModelProperty(value="支付方式1=支付宝2=微信")
    private Integer payType;

    /**
     * 渠道来源
     */
    @Column(name = "pay_source")
    @ApiParam("渠道来源1=Android；2=iOS；3=wechat H5")
    @ApiModelProperty(value="渠道来源1=Android；2=iOS；3=wechat H5")
    private Integer paySource;

    /**
     * 订单状态 1=已完成2=已退款3=代付款4=已过期
     */
    @Column(name = "order_state")
    @ApiParam("订单状态 1=已完成2=已退款3=代付款4=已过期")
    @ApiModelProperty(value="订单状态 1=已完成2=已退款3=代付款4=已过期")
    private String orderState;

    /**
     * 站点id
     */
    @Column(name = "tid")
    @ApiParam("站点id")
    @ApiModelProperty(value="站点id")
    private String tid;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    @ApiParam("创建时间")
    @ApiModelProperty(value="创建时间")
    private String createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    @ApiParam("更新时间")
    @ApiModelProperty(value="更新时间")
    private String updateTime;

    /**
     * 删除状态
     */
    @Column(name = "delete_state")
    @ApiParam("删除状态")
    @ApiModelProperty(value="删除状态")
    private Integer deleteState;

    @ApiParam("商户单号")
    @ApiModelProperty("商户单号")
    @Transient
    private String merchantNo;

    @Transient
    @ApiModelProperty("商户信息列表")
    private List<ProductInfo> productInfoList;

    @Data
    public static class ProductInfo {

        @ApiParam("商品名称")
        @ApiModelProperty("商品名称")
        @Transient
        private String productName;

        @ApiParam("单价")
        @ApiModelProperty("单价")
        @Transient
        private BigDecimal price;

        @ApiParam("数量")
        @ApiModelProperty("数量")
        @Transient
        private Integer num;

        @ApiParam("优惠")
        @ApiModelProperty("优惠")
        @Transient
        private BigDecimal benefit;

        @ApiParam("小计")
        @ApiModelProperty("小计")
        @Transient
        private BigDecimal total;
    }


}