/**
* @filename:LiveSearchHistory 2019年11月22日
    * @project liqiang365  V1.0
    * Copyright(c) 2020 严聪 Co. Ltd.
    * All right reserved.
    */
    package liqiang365.model.entity;
    import javax.persistence.Id;
    import javax.persistence.Table;
    import javax.persistence.Transient;
    import com.fasterxml.jackson.annotation.JsonFormat;
    import io.swagger.annotations.ApiModel;
    import io.swagger.annotations.ApiModelProperty;
    import lombok.*;
    import org.springframework.format.annotation.DateTimeFormat;
    import java.io.Serializable;

/**
* <p>自动生成工具：mybatis-dsc-generator</p>
*
* <p>说明： 用户实体类</P>
* @version: V1.0
* @author: 严聪
*
*/
@Table(name="live_search_history")
@Data
@ApiModel
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class LiveSearchHistory  implements Serializable {

private static final long serialVersionUID = 1574414572720L;

    @Id
    @ApiModelProperty(name = "id" , value = "搜索历史主键")
    private String id;
    @ApiModelProperty(name = "createTime" , value = "创建时间")
    private String createTime;
    @ApiModelProperty(name = "updateTime" , value = "修改时间")
    private String updateTime;
    @ApiModelProperty(name = "deleteState" , value = "删除状态：0 未删除   1 已删除")
    private Integer deleteState;
    @ApiModelProperty(name = "uid" , value = "搜索用户id")
    private String uid;
    @ApiModelProperty(name = "tid" , value = "站点id")
    private String tid;
    @ApiModelProperty(name = "content" , value = "搜索内容")
    private String content;

}
