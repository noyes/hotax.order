/**
* @filename:LiveForbiddenUser 2019年11月22日
    * @project liqiang365  V1.0
    * Copyright(c) 2020 严聪 Co. Ltd.
    * All right reserved.
    */
    package liqiang365.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

/**
* <p>自动生成工具：mybatis-dsc-generator</p>
*
* <p>说明： 用户实体类</P>
* @version: V1.0
* @author: 严聪
*
*/
@Table(name="live_forbidden_user")
@Data
@ApiModel
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@EqualsAndHashCode(callSuper = true)
public class LiveForbiddenUser extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1574414670495L;

    @Id
    @ApiModelProperty(name = "id", value = "")
    private String id;
    @ApiModelProperty(name = "tid", value = "站点ID")
    private String tid;
    @ApiModelProperty(name = "createTime", value = "创建时间")
    private String createTime;
    @ApiModelProperty(name = "updateTime", value = "更新时间")
    private String updateTime;
    @ApiModelProperty(name = "deleteState", value = "删除状态 0=未删除 1=已删除")
    private String deleteState;
    @ApiModelProperty(name = "createUid", value = "创建者id")
    private String createUid;
    @ApiModelProperty(name = "lastUpdateUid", value = "最后更新者id")
    private String lastUpdateUid;
    @ApiModelProperty(name = "forbiddenUid", value = "被踢/被禁言用户uid")
    private String forbiddenUid;
    @ApiModelProperty(name = "forbiddenNum", value = "被踢/被禁言用户编号")
    private String forbiddenNum;
    @ApiModelProperty(name = "forbiddenNickname", value = "被踢/被禁言用户昵称")
    private String forbiddenNickname;
    @ApiModelProperty(name = "forbiddenTime", value = "被禁言截止时间")
    private String forbiddenTime;
    @ApiModelProperty(name = "operatorNickname", value = "操作人昵称")
    private String operatorNickname;
    @ApiModelProperty(name = "operatorNum", value = "操作人编号")
    private String operatorNum;
    @ApiModelProperty(name = "operatorUid", value = "操作人Uid")
    private String operatorUid;
    @ApiModelProperty(name = "anchorNickname", value = "主播昵称")
    private String anchorNickname;
    @ApiModelProperty(name = "operatorUid", value = "主播Uid")
    private String anchorUid;
    @ApiModelProperty(name = "liveNum", value = "直播id")
    private Integer liveNum;
    @ApiModelProperty(name = "forbiddenType", value = "0禁言1踢人")
    private String forbiddenType;
    @ApiModelProperty(name = "roomId", value = "房间id 主播id")
    private Integer roomId;

    @Transient
    private String startTime;
    @Transient
    private String endTime;
}