/**
 * @filename:LiveImUserData 2020年01月02日
 * @project live.api  V1.0
 * Copyright(c) 2020 严聪 Co. Ltd.
 * All right reserved.
 */
package liqiang365.model.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;


/**
 * <p>自动生成工具：mybatis-generator</p>
 *
 * <p>说明： 用户实体类</P>
 *
 * @version: V1.0
 * @author: 严聪
 */
@Table(name = "live_lm_user_data")
@Data
@ApiModel(value = "直播用户连麦信息")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class LiveLmUserData{

    @Id
    @Column(name = "uid")
    @ApiModelProperty(name = "uid", value = "直播用户")
    private String uid;

    @Column(name = "create_time")
    @ApiModelProperty(name = "createTime", value = "创建时间")
    private String createTime;

    @Column(name = "setup")
    @ApiModelProperty(name = "setup", value = "设置")
    private String setup;


}
