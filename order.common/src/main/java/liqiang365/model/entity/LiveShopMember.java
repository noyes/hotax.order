package liqiang365.model.entity;

import hotax.core.valid.InsertCheck;
import hotax.core.valid.UpdateCheck;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.validation.constraints.NotBlank;

@ApiModel(value ="live_shop_member", description ="live_shop_member" )
public class LiveShopMember implements Serializable {
    /**
     * 
     */
    @ApiModelProperty(value ="", required = true)
    @NotBlank(message="字段id不能为空",groups ={UpdateCheck.class})
    private String id;

    /**
     * 用户id
     */
    @ApiModelProperty(value ="用户id", required = true)
    @NotBlank(message="字段uid不能为空")
    private String uid;

    /**
     * 状态 0:非会员 1:会员
     */
    @ApiModelProperty(value ="状态0:非会员1:会员", required = true)
    private Integer status;

    /**
     * 0:米粉(橱窗) 1:白金会员 2:超白金会员
     */
    @ApiModelProperty(value ="0:米粉(橱窗)1:白金会员2:超白金会员", required = true)
    private Integer type;

    /**
     * 0:来源商城 1:来源后台 2.来源于学习平台app端设置
     */
    @ApiModelProperty(value ="0:来源商城1:来源后台2.来源于学习平台app端设置", required = true)
    private Integer source;

    /**
     * 设置日期
     */
    @ApiModelProperty(value ="设置日期", required = true)
    private Integer cdate;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid == null ? null : uid.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getSource() {
        return source;
    }

    public void setSource(Integer source) {
        this.source = source;
    }

    public Integer getCdate() {
        return cdate;
    }

    public void setCdate(Integer cdate) {
        this.cdate = cdate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", uid=").append(uid);
        sb.append(", status=").append(status);
        sb.append(", type=").append(type);
        sb.append(", source=").append(source);
        sb.append(", cdate=").append(cdate);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}