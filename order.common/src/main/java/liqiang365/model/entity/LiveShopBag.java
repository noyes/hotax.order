package liqiang365.model.entity;

import hotax.core.valid.UpdateCheck;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.validation.constraints.NotBlank;

@ApiModel(value ="live_shop_bag", description ="live_shop_bag" )
public class LiveShopBag implements Serializable {
    /**
     * 
     */
    @ApiModelProperty(value ="", required = true)
    @NotBlank(message="字段id不能为空",groups ={UpdateCheck.class})
    private String id;

    /**
     * 
     */
    @ApiModelProperty(value ="", required = false)
    private String uid;

    /**
     * 购物袋id
     */
    @ApiModelProperty(value ="购物袋id", required = false)
    private String bid;

    /**
     * 创建时间
     */
    @ApiModelProperty(value ="创建时间", required = false)
    private Integer cdate;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid == null ? null : uid.trim();
    }

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid == null ? null : bid.trim();
    }

    public Integer getCdate() {
        return cdate;
    }

    public void setCdate(Integer cdate) {
        this.cdate = cdate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", uid=").append(uid);
        sb.append(", bid=").append(bid);
        sb.append(", cdate=").append(cdate);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}