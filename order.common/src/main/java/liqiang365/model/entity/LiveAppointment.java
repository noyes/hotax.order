/**
* @filename:LiveAppointment 2019年11月22日
    * @project liqiang365  V1.0
    * Copyright(c) 2020 严聪 Co. Ltd.
    * All right reserved.
    */
    package liqiang365.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
* <p>自动生成工具：mybatis-dsc-generator</p>
*
* <p>说明： 用户实体类</P>
* @version: V1.0
* @author: 严聪
*
*/
@Table(name="live_appointment")
@Data
@ApiModel
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class LiveAppointment  implements Serializable {

    private static final long serialVersionUID = 1574414709449L;

    @Id
    @ApiModelProperty(name = "id", value = "")
    private String id;
    @ApiModelProperty(name = "tid", value = "站点ID")
    private String tid;
    @ApiModelProperty(name = "createTime", value = "创建时间")
    private String createTime;
    @ApiModelProperty(name = "deleteState", value = "删除状态 0=未删除 1=已删除")
    private String deleteState;
    @ApiModelProperty(name = "liveId", value = "直播id")
    private String liveId;
    @ApiModelProperty(name = "uid", value = "用户id")
    private String uid;

    @ApiModelProperty(name = "userHead", value = "用户头像")
    private String userHead;

    @ApiModelProperty(name = "nickname", value = "昵称")
    private String nickname;

    @ApiModelProperty(name = "account", value = "用户账号")
    private String account;



}