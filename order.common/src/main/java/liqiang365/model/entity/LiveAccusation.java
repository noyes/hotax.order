/**
* @filename:LiveAccusation 2019年11月22日
    * @project liqiang365  V1.0
    * Copyright(c) 2020 严聪 Co. Ltd.
    * All right reserved.
    */
    package liqiang365.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

/**
* <p>自动生成工具：mybatis-dsc-generator</p>
*
* <p>说明： 用户实体类</P>
* @version: V1.0
* @author: 严聪
*
*/
@Table(name="live_accusation")
@Data
@ApiModel
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class LiveAccusation  implements Serializable {

    private static final long serialVersionUID = 1574414734811L;

    @Id
    @ApiModelProperty(name = "id", value = "主键序号")
    private Long id;
    @ApiModelProperty(name = "deleteState", value = "0=未删除1=已删除")
    private Integer deleteState;
    @ApiModelProperty(name = "createTime", value = "创建时间")
    private String createTime;
    @ApiModelProperty(name = "updateTime", value = "更新时间")
    private String updateTime;
    @ApiModelProperty(name = "uid", value = "举报人id")
    private String uid;
    @ApiModelProperty(name = "liveId", value = "直播id")
    private Integer liveNum;
    @ApiModelProperty(name = "uname", value = "举报人名称")
    private String uname;
    @ApiModelProperty(name = "uno", value = "举报人编号")
    private String uno;
    @ApiModelProperty(name = "bUid", value = "被举报人id")
    private String bUid;
    @ApiModelProperty(name = "bUname", value = "被举报人名称")
    private String bUname;
    @ApiModelProperty(name = "bUno", value = "被举报人编号id")
    private Integer bUno;
    @ApiModelProperty(name = "typeId", value = "举报类型id")
    private String typeId;
    @ApiModelProperty(name = "content", value = "举报内容")
    private String content;
    @ApiModelProperty(name = "handleOpinions", value = "处理意见")
    private String handleOpinions;
    @ApiModelProperty(name = "handleUser", value = "操作人")
    private String handleUser;
    @ApiModelProperty(name = "state", value = "处理状态0=未处理1=已处理")
    private Integer state;
    @ApiModelProperty(name = "tid", value = "站点id")
    private String tid;

    @Transient
    @ApiModelProperty(name = "typeName", value = "举报类型名称")
    private String typeName;

}