package liqiang365.model.entity;

import lombok.Data;

@Data
public class LiveShopGoodsExt {
    private String vipName;
    private String orderType;
    private String vipType;
    private String price;
}
