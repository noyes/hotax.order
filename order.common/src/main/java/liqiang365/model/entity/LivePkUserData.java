package liqiang365.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * 直播用户PK信息
 * @date 2020-03-18 14:37:33
 * @author 苏
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "直播用户PK信息")
@Builder
@Table(name = "live_pk_user_data")
public class LivePkUserData {
    /**
     * 直播用户
     */
    @Id
    @Column(name = "uid")
    @ApiParam("直播用户")
    @ApiModelProperty(value="直播用户")
    private String uid;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    @ApiParam("创建时间")
    @ApiModelProperty(value="创建时间")
    private String createTime;

    /**
     * PK设置
     */
    @Column(name = "pk_setup")
    @ApiParam("PK设置")
    @ApiModelProperty(value="PK设置")
    private String pkSetup;

    /**
     * pk胜利场次数
     */
    @Column(name = "win_count")
    @ApiParam("pk胜利场次数")
    @ApiModelProperty(value="pk胜利场次数")
    private Integer winCount;

    /**
     * PK失败场次数
     */
    @Column(name = "fail_count")
    @ApiParam("PK失败场次数")
    @ApiModelProperty(value="PK失败场次数")
    private Integer failCount;

    /**
     * PK平的场次数
     */
    @Column(name = "flat_count")
    @ApiParam("PK平的场次数")
    @ApiModelProperty(value="PK平的场次数")
    private Integer flatCount;

    /**
     * 参与PK的次数
     */
    @Column(name = "pk_count")
    @ApiParam("参与PK的次数")
    @ApiModelProperty(value="参与PK的次数")
    private Integer pkCount;
}
