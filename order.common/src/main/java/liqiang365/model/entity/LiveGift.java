/**
 * @filename:LiveGift 2019年11月22日
 * @project liqiang365  V1.0
 * Copyright(c) 2020 严聪 Co. Ltd.
 * All right reserved.
 */
package liqiang365.model.entity;

import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>自动生成工具：mybatis-dsc-generator</p>
 *
 * <p>说明： 用户实体类</P>
 * @version: V1.0
 * @author: 严聪
 *
 */
@Table(name = "live_gift")
@Data
@ApiModel
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class LiveGift implements Serializable {

    private static final long serialVersionUID = 1574414657814L;

    @Id
    @ApiModelProperty(name = "id", value = "主键")
    private String id;
    @ApiModelProperty(name = "name", value = "礼物名称")
    private String name;
    @ApiModelProperty(name = "type", value = "礼物类型：0普通礼物 1豪华礼物  2套餐礼物")
    private Integer type;
    @ApiModelProperty(name = "cornerMark", value = "礼物角标 0普通 1热门 2守护 3幸运")
    private Integer cornerMark;
    @ApiModelProperty(name = "doubleHit", value = "连击 0不可连击 1可连击")
    private Integer doubleHit;
    @ApiModelProperty(name = "amount", value = "赠送礼物所需点数 0代表免费")
    private Integer amount;
    @ApiModelProperty(name = "img", value = "礼物封面图片")
    private String img;
    @ApiModelProperty(name = "animationType", value = "动画类型 GIF  SVGA JSON")
    private String animationType;
    @ApiModelProperty(name = "horizontalScreenUrl", value = "横屏地址")
    private String horizontalScreenUrl;
    @ApiModelProperty(name = "vertical screenUrl", value = "竖屏url")
    private String verticalScreenUrl;
    @ApiModelProperty(name = "animationTime", value = "动画时间")
    private BigDecimal animationTime;
    @ApiModelProperty(name = "sort", value = "排序")
    private Integer sort;
    @ApiModelProperty(name = "openState", value = "是否开放  0 不开放 1开放")
    private Integer openState;
    @ApiModelProperty(name = "openTime", value = "开启时间")
    private String openTime;
    @ApiModelProperty(name = "tid", value = "站点ID")
    private String tid;
    @ApiModelProperty(name = "createTime", value = "创建时间")
    private String createTime;
    @ApiModelProperty(name = "updateTime", value = "更新时间")
    private String updateTime;
    @ApiModelProperty(name = "deleteState", value = "删除状态 0=未删除 1=已删除")
    private String deleteState;
    @ApiModelProperty(name = "createUid", value = "创建者id")
    private String createUid;
    @ApiModelProperty(name = "lastUpdateUid", value = "最后更新者id")
    private String lastUpdateUid;
    @ApiModelProperty(name = "horizontalScreenState", value = "是否横屏 0否 1是")
    private Integer horizontalScreenState;
}
