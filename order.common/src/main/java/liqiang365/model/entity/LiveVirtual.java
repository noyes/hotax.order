package liqiang365.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author 吴华龙
 * @classname LiveVirtual
 * @description 直播弹幕/虚拟弹幕实体
 * @date 2019/11/21 14:12
 */
@ApiModel
@Table(name = "live_virtual")
@Data
public class LiveVirtual implements java.io.Serializable{
    private static final long serialVersionUID = -4940170972218573233L;

    @Id
    private String id;
    /**
     * 评论库/弹幕库名称
     */
    @ApiModelProperty("评论库/弹幕库名称")
    private String title;
    /**
     * 类型：1虚拟评论 2虚拟弹幕
     */
    @ApiModelProperty("类型：1-虚拟评论 2-虚拟弹幕")
    private Integer type;
    /**
     * 评论内容/弹幕内容
     */
    @ApiModelProperty("评论内容/弹幕内容")
    private String content;
    /**
     * pid=-1 评论库/弹幕库  pid != -1 评论内容/弹幕内容
     */
    @ApiModelProperty("pid=-1 评论库/弹幕库  pid != -1 评论内容/弹幕内容")
    private String pid;
    @ApiModelProperty("是否默认库 0非默认 1默认")
    private Integer virtualDefault;
    @ApiModelProperty("内容数量")
    @Transient
    private Integer virtualCount;

    private String createTime;

    private String updateTime;

    private String deleteState;

    private String createUid;

    private String lastUpdateUid;

    private String tid;
    @Transient
    private Integer pageSize;
    @Transient
    private Integer pageNum;


    public Integer getPageSize() {
        return pageSize == null ? 10 : pageSize;
    }

    public Integer getPageNum() {
        return pageNum == null ? 0 : pageNum;
    }
}
