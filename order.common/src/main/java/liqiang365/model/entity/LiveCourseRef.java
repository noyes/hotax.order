/**
 * @filename:LiveCourseRef 2020年05月09日
 * @project live.api  V1.0
 * Copyright(c) 2020 zhuxl Co. Ltd.
 * All right reserved.
 */
package liqiang365.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * <p>自动生成工具：mybatis-generator</p>
 *
 * <p>说明： 直播课程实体类</P>
 * @version: V1.0
 * @author: zhuxl
 *
 */
@Table(name = "live_course_ref")
@Data
@ApiModel
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class LiveCourseRef implements Serializable {

    private static final long serialVersionUID = 1588989175952L;

    @Id
    @ApiModelProperty(name = "id", value = "主键")
    private String id;
    @ApiModelProperty(name = "createTime", value = "创建时间")
    private String createTime;
    @ApiModelProperty(name = "updateTime", value = "更新时间")
    private String updateTime;
    @ApiModelProperty(name = "deleteState", value = "删除状态 1=已删除 0=未删除")
    private String deleteState;
    @ApiModelProperty(name = "courseId", value = "关联课程")
    private String courseId;
    @ApiModelProperty(name = "liveId", value = "关联直播")
    private String liveId;
    @ApiModelProperty(name = "uid", value = "关联主播用户")
    private String uid;


}
