/**
* @filename:LiveUserAuth 2019年11月22日
    * @project liqiang365  V1.0
    * Copyright(c) 2020 严聪 Co. Ltd.
    * All right reserved.
    */
    package liqiang365.model.entity;
    import javax.persistence.Id;
    import javax.persistence.Table;
    import javax.persistence.Transient;
    import com.fasterxml.jackson.annotation.JsonFormat;
    import io.swagger.annotations.ApiModel;
    import io.swagger.annotations.ApiModelProperty;
    import lombok.*;
    import org.springframework.format.annotation.DateTimeFormat;
    import java.io.Serializable;

/**
* <p>自动生成工具：mybatis-dsc-generator</p>
*
* <p>说明： 用户实体类</P>
* @version: V1.0
* @author: 严聪
*
*/
@Table(name="live_user_auth")
@Data
@ApiModel
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class LiveUserAuth  implements Serializable {

private static final long serialVersionUID = 1574414509295L;

    @Id
    @ApiModelProperty(name = "id" , value = "主键")
    private String id;
    @ApiModelProperty(name = "uid" , value = "用户id")
    private String uid;
    @ApiModelProperty(name = "roomid" , value = "房间id  查询的时候 用户ID=房间号")
    private Integer roomid;
    @ApiModelProperty(name = "realname" , value = "真实姓名")
    private String realname;
    @ApiModelProperty(name = "nickname" , value = "昵称")
    private String nickname;
    @ApiModelProperty(name = "phone" , value = "手机号")
    private String phone;
    @ApiModelProperty(name = "idcardNo" , value = "身份证号")
    private String idcardNo;
    @ApiModelProperty(name = "idcardImgObverse" , value = "身份证正面照片")
    private String idcardImgObverse;
    @ApiModelProperty(name = "idcardImgReverse" , value = "身份证反面照片")
    private String idcardImgReverse;
    @ApiModelProperty(name = "idcardImgHold" , value = "身份证手持照片")
    private String idcardImgHold;
    @ApiModelProperty(name = "state" , value = "状态 0 审核中 1审核通过 2 审核不通过")
    private String state;
    @ApiModelProperty(name = "reason" , value = "不通过原因")
    private String reason;
    @ApiModelProperty(name = "authTime" , value = "审核时间")
    private String authTime;
    @ApiModelProperty(name = "tid" , value = "站点ID")
    private String tid;
    @ApiModelProperty(name = "createTime" , value = "创建时间")
    private String createTime;
    @ApiModelProperty(name = "updateTime" , value = "更新时间")
    private String updateTime;
    @ApiModelProperty(name = "deleteState" , value = "删除状态 0=未删除 1=已删除")
    private String deleteState;
    @ApiModelProperty(name = "createUid" , value = "创建者id")
    private String createUid;
    @ApiModelProperty(name = "lastUpdateUid" , value = "最后更新者id")
    private String lastUpdateUid;

    @ApiModelProperty(value = "页面每页数量")
    @Transient
    private Integer pageSize;

    @ApiModelProperty(value = "第几页")
    @Transient
    private Integer pageNum;

    @ApiModelProperty(value = "时间范围起始时间")
    @Transient
    private String createTimeStartTime;

    @ApiModelProperty(value = "时间范围结束时间")
    @Transient
    private String createTimeEndTime;

}
