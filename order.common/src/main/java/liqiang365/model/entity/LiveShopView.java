package liqiang365.model.entity;

import hotax.core.valid.UpdateCheck;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.validation.constraints.NotBlank;

@ApiModel(value ="live_shop_view", description ="live_shop_view" )
public class LiveShopView implements Serializable {
    /**
     * 
     */
    @ApiModelProperty(value ="", required = true)
    @NotBlank(message="字段id不能为空",groups ={UpdateCheck.class})
    private String id;

    /**
     * 直播间用户id
     */
    @ApiModelProperty(value ="直播间用户id", required = true)
    @NotBlank(message="字段uid不能为空")
    private String uid;

    /**
     * 1:白金会员+超级白金会员
     */
    @ApiModelProperty(value ="1:白金会员+超级白金会员", required = true)
    @NotBlank(message="字段fno不能为空")
    private String fno;

    /**
     * 白金会员+超级白金会员
     */
    @ApiModelProperty(value ="白金会员+超级白金会员", required = true)
    @NotBlank(message="字段fname不能为空")
    private String fname;

    /**
     * 0:不显示 1:显示
     */
    @ApiModelProperty(value ="0:不显示1:显示", required = false)
    private Integer status;

    /**
     * 
     */
    @ApiModelProperty(value ="", required = false)
    private Integer cdate;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid == null ? null : uid.trim();
    }

    public String getFno() {
        return fno;
    }

    public void setFno(String fno) {
        this.fno = fno == null ? null : fno.trim();
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname == null ? null : fname.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCdate() {
        return cdate;
    }

    public void setCdate(Integer cdate) {
        this.cdate = cdate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", uid=").append(uid);
        sb.append(", fno=").append(fno);
        sb.append(", fname=").append(fname);
        sb.append(", status=").append(status);
        sb.append(", cdate=").append(cdate);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}