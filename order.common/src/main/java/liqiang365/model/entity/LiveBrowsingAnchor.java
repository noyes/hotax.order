/**
 * @filename:LiveTag 2019年11月22日
 * @project liqiang365  V1.0
 * Copyright(c) 2020 严聪 Co. Ltd.
 * All right reserved.
 */
package liqiang365.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * <p>自动生成工具：mybatis-dsc-generator</p>
 *
 * <p>浏览主播记录</P>
 * @version: V1.0
 * @author: 严聪
 *
 */
@Table(name = "live_browsing_anchor")
@Data
@ApiModel
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class LiveBrowsingAnchor implements Serializable {

    private static final long serialVersionUID = 1574414552477L;

    @Id
    @ApiModelProperty(name = "id", value = "主键")
    private String id;
    @ApiModelProperty(name = "deleteState", value = "删除状态 0=未删除 1=已删除")
    private Boolean deleteState;
    @ApiModelProperty(name = "createTime", value = "创建时间")
    private String createTime;
    @ApiModelProperty(name = "updateTime", value = "更新时间")
    private String updateTime;
    @ApiModelProperty(name = "tid", value = "站点ID")
    private String tid;
    @ApiModelProperty(name = "uid", value = "观看用户")
    private String uid;
    @ApiModelProperty(name = "anchorUid", value = "主播用户")
    private String anchorUid;

}
