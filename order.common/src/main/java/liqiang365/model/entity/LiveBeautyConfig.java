/**
 * @filename:Live 2019年11月22日
 * @project liqiang365  V1.0
 * Copyright(c) 2020 严聪 Co. Ltd.
 * All right reserved.
 */
package liqiang365.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * <p>自动生成工具：mybatis-dsc-generator</p>
 *
 * <p>说明： 用户实体类</P>
 * @version: V1.0
 * @author: 严聪
 *
 */
@Table(name = "live_beauty_config")
@Data
@ApiModel
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@EqualsAndHashCode(callSuper = true)
public class LiveBeautyConfig extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1574414745892L;

    @Id
    @ApiModelProperty(name = "id", value = "")
    private String id;
    @ApiModelProperty(name = "createTime", value = "创建时间")
    private String createTime;
    @ApiModelProperty(name = "updateTime", value = "更新时间")
    private String updateTime;
    @ApiModelProperty(name = "deleteState", value = "删除状态 0=未删除 1=已删除")
    private String deleteState;
    @ApiModelProperty(name = "createUid", value = "创建者id")
    private String createUid;
    @ApiModelProperty(name = "lastUpdateUid", value = "最后更新者id")
    private String lastUpdateUid;
    @ApiModelProperty(name = "uid", value = "主播id")
    private String uid;
    @ApiModelProperty(name = "tid", value = "tid")
    private String tid;
    @ApiModelProperty(name = "value", value = "美颜配置数据")
    private String value;
}