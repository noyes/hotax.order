package liqiang365.model.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * description
 *
 * @author f
 * @date 2019/3/19 15:48
 **/
@Data
public class BaseEntity implements Serializable {


    /**
     * PK
     */
    @Id
    @Column(name = "id")
    private String id;


    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private String createTime ;

    /**
     * 创建时间
     */
    @Column(name = "update_time")
    private String updateTime ;

    /**
     * 1 表示删除，0 表示未删除
     */
    @Column(name = "delete_state")
    private String deleteState ;

    @Column(name = "tid")
    private String tid ;

}
