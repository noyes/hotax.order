/**
 * @filename:LiveLmApplyFor 2020年01月02日
 * @project live.api  V1.0
 * Copyright(c) 2020 严聪 Co. Ltd.
 * All right reserved.
 */
package liqiang365.model.entity;

import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>自动生成工具：mybatis-generator</p>
 *
 * <p>说明： 用户实体类</P>
 *
 * @version: V1.0
 * @author: 严聪
 */
@Table(name = "live_lm_apply_for")
@Data
@ApiModel
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class LiveLmApplyFor implements Serializable {

    private static final long serialVersionUID = 1586314811892L;

    @Id
    @ApiModelProperty(name = "id", value = "主键")
    private String id;
    @ApiModelProperty(name = "createTime", value = "申请创建时间")
    private String createTime;
    @ApiModelProperty(name = "deleteState", value = "删除：1=删除；0=未删除；")
    private String deleteState;
    @ApiModelProperty(name = "uid", value = "连麦申请用户")
    private String uid;
    @ApiModelProperty(name = "liveId", value = "直播")
    private String liveId;
    @ApiModelProperty(name = "roomId", value = "房间")
    private Integer roomId;
    @ApiModelProperty(name = "liveName", value = "直播名称")
    private String liveName;
    @ApiModelProperty(name = "costQb", value = "花费智慧豆")
    private Integer costQb;
    @ApiModelProperty(name = "duration", value = "申请连麦时长（秒）")
    private Integer duration;
    @ApiModelProperty(name = "giftQb", value = "赠送智慧豆")
    private Integer giftQb;
    @ApiModelProperty(name = "type", value = "连麦类型：1=音频；2=视频；")
    private Integer type;
    @ApiModelProperty(name = "state", value = "状态：-3=主播关闭直播；-2=超时取消；-1=观看端取消；0=申请中；1=连麦成功；2=等待连麦；3=连麦结束")
    private Integer state;
    @ApiModelProperty(name = "liveUid", value = "主播")
    private String liveUid;
    @ApiModelProperty(name = "proportion", value = "主播分成比例(0.5)")
    private BigDecimal proportion;
    @ApiModelProperty(name = "sysSetup", value = "连麦系统配置JSON")
    private String sysSetup;
    @ApiModelProperty(name = "remark", value = "备注")
    private String remark;
    @ApiModelProperty(value = "申请超时时间")
    private String endTimeout;
    @ApiModelProperty(name = "endUid", value = "结束人")
    private String endUid;
    @ApiModelProperty(name = "endTime", value = "结束时间")
    private String endTime;
    @ApiModelProperty(value = "智慧豆流水状态：0=扣豆；1=退还；2=给主播分成；")
    private Integer qbState;
    @ApiModelProperty(value = "退还智慧豆流水或者给主播分成流水ID")
    private String lastQbRecordId;
    @ApiModelProperty(value = "申请扣豆流水记录")
    private String liveQbRecordId;
    @ApiModelProperty(value = "连麦记录ID")
    private String lmRecordId;
    @ApiModelProperty(name = "startTime", value = "开始时间")
    private String startTime;

}
