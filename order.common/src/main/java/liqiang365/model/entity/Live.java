/**
 * @filename:Live 2019年11月22日
 * @project liqiang365  V1.0
 * Copyright(c) 2020 严聪 Co. Ltd.
 * All right reserved.
 */
package liqiang365.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>自动生成工具：mybatis-dsc-generator</p>
 *
 * <p>说明： 用户实体类</P>
 * @version: V1.0
 * @author: 严聪
 *
 */
@Table(name = "live")
@Data
@ApiModel
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@EqualsAndHashCode(callSuper = true)
public class Live extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1574414745892L;

    @Id
    @ApiModelProperty(name = "id", value = "")
    private String id;
    @ApiModelProperty(name = "createTime", value = "创建时间")
    private String createTime;
    @ApiModelProperty(name = "updateTime", value = "更新时间")
    private String updateTime;
    @ApiModelProperty(name = "deleteState", value = "删除状态 0=未删除 1=已删除")
    private String deleteState;
    @ApiModelProperty(name = "createUid", value = "创建者id")
    private String createUid;
    @ApiModelProperty(name = "lastUpdateUid", value = "最后更新者id")
    private String lastUpdateUid;
    @ApiModelProperty(name = "uid", value = "主播id")
    private String uid;
    @ApiModelProperty(name = "liveNum", value = "直播ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer liveNum;
    @ApiModelProperty(name = "userNickname", value = "主播昵称")
    private String userNickname;
    @ApiModelProperty(name = "userPhone", value = "主播手机号")
    private String userPhone;
    @ApiModelProperty(name = "liveName", value = "直播名称")
    private String liveName;
    @ApiModelProperty(name = "livePic", value = "直播封面")
    private String livePic;
    @ApiModelProperty(name = "liveClassId", value = "直播分类Id")
    private String liveClassId;
    @ApiModelProperty(name = "liveClassName", value = "直播分类名称")
    private String liveClassName;
    @ApiModelProperty(name = "roomType", value = "房间类型 0=普通房间 1=加密房间 2=付费房间 3=计时房间")
    private String roomType;
    @ApiModelProperty(name ="lockType" ,value= "0=所有人可进入 1=vip用户 2=团购商用户 3=指定人员")
    private String lockType;
    @ApiModelProperty(name = "roomId", value = "房间号")
    private Integer roomId;
    @ApiModelProperty(name = "roomPassword", value = "房间密码")
    private String roomPassword;
    @ApiModelProperty(name = "roomPrice", value = "房间价格")
    private BigDecimal roomPrice;
    @ApiModelProperty(name = "videoType", value = "视频类型 0=竖版 1=横版")
    private String videoType;
    @ApiModelProperty(name = "equipment", value = "设备信息")
    private String equipment;
    @ApiModelProperty(name = "startTime", value = "直播开始时间")
    private String startTime;
    @ApiModelProperty(name = "endTime", value = "直播结束时间")
    private String endTime;
    @ApiModelProperty(name = "liveState", value = "0=未开始 1=直播中 2=离开中 3=回放中 4=已结束")
    private String liveState;
    @ApiModelProperty(name = "liveType", value = "直播类型 0=rtmp 1=mp4")
    private String liveType;
    @ApiModelProperty(name = "mp4Url", value = "mp4Url")
    private String mp4Url;
    @ApiModelProperty(name = "pushUrl", value = "推流地址")
    private String pushUrl;
    @ApiModelProperty(name = "pullUrl", value = "app拉流地址")
    private String pullUrl;
    @ApiModelProperty(name = "wxpullurl", value = "H5拉流地址")
    private String wxpullurl;
    @ApiModelProperty(name = "publishState", value = "0=立即上架 1=定时上架 2=暂不上架")
    private String publishState;
    @ApiModelProperty(name = "recommendState", value = "0=不推荐 1=推荐")
    private String recommendState;
    @ApiModelProperty(name = "reviewRecommendState", value = "回看推荐   0=不推荐 1=推荐")
    private String reviewRecommendState;
    @ApiModelProperty(name = "indexRecommendState", value = "首页推荐   0=不推荐 1=推荐")
    private String indexRecommendState;
    @ApiModelProperty(name = "hot", value = "是否热门 0非热门 1热门")
    private String hot;
    @ApiModelProperty(name = "bannedState", value = "0=禁播 1=正常 2=警告")
    private String bannedState;
    @ApiModelProperty(name = "bannedInfo", value = "禁播事由")
    private String bannedInfo;
    @ApiModelProperty(name = "bannedTime", value = "禁播时间")
    private String bannedTime;
    @ApiModelProperty(name = "seq", value = "排序")
    private Integer seq;
    @ApiModelProperty(name = "tid", value = "站点id")
    private String tid;
    @ApiModelProperty(name = "robot", value = "机器人设置")
    private String robot;
    @Transient
    private String bannedTimeStart;
    @Transient
    private String bannedTimeEnd;

    @ApiModelProperty(name = "appointmentState", value = "是否预约 0 非预约创建 1 预约创建")
    private String appointmentState;

    @ApiModelProperty(name = "publishTime", value = "发布时间")
    private String publishTime;

    @ApiModelProperty(value = "0=未连麦PK；1=连麦；2=PK")
    private String pkState;

    @ApiModelProperty(value = "当前PK或者连麦ID")
    private String pkId;

    @ApiModelProperty(value = "推流地址")
    private String streamName;

    @Transient
    @ApiModelProperty(name = "time", value = "直播时长")
    private String time;
    @Transient
    @ApiModelProperty(name = "time", value = "直播礼物数量")
    private Integer countGift;
    @Transient
    private String createTimeStart;
    @Transient
    private String createTimeEnd;
    @Transient
    private LiveStat liveStat;
    @Transient
    @ApiModelProperty(name = "countAccusation", value = "举报数量")
    private Integer countAccusation;
    @Transient
    @ApiModelProperty(name = "accusationState", value = "举报状态")
    private Integer accusationState;
    @Transient
    @ApiModelProperty(name = "accusationState", value = "已开始时间--秒")
    private Long startSecond;

    @Transient
    @ApiModelProperty(name = "userAccount", value = "加密房间指定人员")
    private List<String> userAccount;

    /** PK 新增|by 苏 start*/
    @Transient
    @ApiModelProperty(value = "PK次数")
    private Integer pkCount;
    @Transient
    @ApiModelProperty(value = "PK胜率")
    private String pkWinRate;
    /** PK 新增|by 苏 end*/


    @Transient
    @ApiModelProperty(value = "连麦次数")
    private Integer lmCount;
    @Transient
    @ApiModelProperty(value = "连麦收益")
    private Integer lmIncome;
    @Transient
    @ApiModelProperty(value = "带货收益")
    private BigDecimal anchorIncome;
}
