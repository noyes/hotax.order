package liqiang365.model.entity;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author 孙冠杰
 * @classname LiveGiftConsumeRecord
 * @description 礼物消费记录
 * @date 2019/11/22 10:29
 */
@Table(name = "live_gift_consume_record")
@Data
public class LiveGiftConsumeRecord {
    /**
     * 主键序号
     */
    @Id
    @Column(name = "id")
    @ApiParam("主键序号")
    @ApiModelProperty(value = "主键序号")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 收支类型 1=支出2 =收入
     */
    @Column(name = "type")
    @ApiParam("收支类型 1=支出2 =收入")
    @ApiModelProperty(value = "收支类型 1=支出2 =收入")
    private Integer type;

    /**
     * 用户原有智慧豆
     */
    @Column(name = "old_qb")
    @ApiParam("用户原有智慧豆")
    @ApiModelProperty(value = "用户原有智慧豆")
    private Integer oldQb;

    /**
     * 现有智慧豆
     */
    @Column(name = "now_qb")
    @ApiParam("现有智慧豆")
    @ApiModelProperty(value = "现有智慧豆")
    private Integer nowQb;

    /**
     * 收支行为 1=赠送礼物 2=弹幕
     */
    @Column(name = "action_type")
    @ApiParam("收支行为 1=赠送礼物 2=弹幕 3=连麦")
    @ApiModelProperty(value = "收支行为 1=赠送礼物 2=弹幕 3=连麦")
    private Integer actionType;

    /**
     * 用户id
     */
    @Column(name = "uid")
    @ApiParam("用户id")
    @ApiModelProperty(value = "用户id")
    private String uid;

    /**
     * 用户昵称
     */
    @Column(name = "user_nickname")
    @ApiParam("用户昵称")
    @ApiModelProperty(value = "用户昵称")
    private String userNickname;

    /**
     * 用户序号
     */
    @Column(name = "user_no")
    @ApiParam("用户账号")
    @ApiModelProperty(value = "用户序号")
    private String userNo;

    /**
     * 主播昵称
     */
    @Column(name = "anchor_nickname")
    @ApiParam("主播昵称")
    @ApiModelProperty(value = "主播昵称")
    private String anchorNickname;

    /**
     * 礼物价值智慧豆数量
     */
    @Column(name = "gift_qb")
    @ApiParam("礼物价值智慧豆数量")
    @ApiModelProperty(value = "礼物价值智慧豆数量")
    private String giftQb;

    /**
     * 赠送ip
     */
    @Column(name = "ip")
    @ApiParam("赠送ip")
    @ApiModelProperty(value = "赠送ip")
    private String ip;

    /**
     * 主播序号
     */
    @Column(name = "anchor_no")
    @ApiParam("主播序号")
    @ApiModelProperty(value = "主播序号")
    private Integer anchorNo;

    /**
     * 直播id序号
     */
    @Column(name = "live_no")
    @ApiParam("直播id序号")
    @ApiModelProperty(value = "直播id序号")
    private Integer liveNo;

    /**
     * 直播名称
     */
    @Column(name = "live_name")
    @ApiParam("直播名称")
    @ApiModelProperty(value = "直播名称")
    private String liveName;

    /**
     * 礼物Id
     */
    @Column(name = "gift_id")
    @ApiParam("礼物Id")
    @ApiModelProperty(value = "礼物Id")
    private String giftId;

    /**
     * 礼物名称
     */
    @Column(name = "gift_name")
    @ApiParam("礼物名称")
    @ApiModelProperty(value = "礼物名称")
    private String giftName;

    /**
     * 数量
     */
    @Column(name = "num")
    @ApiParam("数量")
    @ApiModelProperty(value = "数量")
    private Integer num;

    /**
     * 总价
     */
    @Column(name = "total_money")
    @ApiParam("总价")
    @ApiModelProperty(value = "总价")
    private BigDecimal totalMoney;

    /**
     * 站点id
     */
    @Column(name = "tid")
    @ApiParam("站点id")
    @ApiModelProperty(value = "站点id")
    private String tid;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    @ApiParam("创建时间")
    @ApiModelProperty(value = "创建时间")
    private String createTime;

    /**
     * 0=未删除1=已删除
     */
    @Column(name = "delete_state")
    @ApiParam("0=未删除1=已删除")
    @ApiModelProperty(value = "0=未删除1=已删除")
    private Integer deleteState;

    /**
     * add by 苏 PK开发
     */
    @ApiModelProperty(value = "pkId")
    @Transient
    private String pkId;
    @ApiModelProperty(value = "直播状态：0=正常直播；1=连麦；2=PK")
    @Transient
    private String livePkState;

    @ApiModelProperty(value="PK记录id")
    private String livePkRecordId;

}
