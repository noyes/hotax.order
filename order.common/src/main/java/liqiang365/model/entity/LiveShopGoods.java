package liqiang365.model.entity;

import hotax.core.valid.InsertCheck;
import hotax.core.valid.UpdateCheck;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.validation.constraints.NotBlank;

@ApiModel(value ="live_shop_goods", description ="live_shop_goods" )
public class LiveShopGoods implements Serializable {
    /**
     * 商品id
     */
    @ApiModelProperty(value ="商品id", required = true)
    @NotBlank(message="字段gno不能为空",groups ={UpdateCheck.class})
    private String gno;

    /**
     * 商品名称
     */
    @ApiModelProperty(value ="商品名称", required = true)
    @NotBlank(message="字段gname不能为空")
    private String gname;

    /**
     * 0:不显示 1:显示
     */
    @ApiModelProperty(value ="0:不显示1:显示", required = false)
    private Integer status;

    /**
     * 
     */
    @ApiModelProperty(value ="", required = false)
    private Integer cdate;

    /**
     * 
     */
    @ApiModelProperty(value ="", required = false)
    private String note;

    /**
     * 
     */
    @ApiModelProperty(value ="", required = false)
    private Integer otype;

    /**
     * 
     */
    @ApiModelProperty(value ="", required = false)
    private BigDecimal price;

    private static final long serialVersionUID = 1L;

    public String getGno() {
        return gno;
    }

    public void setGno(String gno) {
        this.gno = gno == null ? null : gno.trim();
    }

    public String getGname() {
        return gname;
    }

    public void setGname(String gname) {
        this.gname = gname == null ? null : gname.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCdate() {
        return cdate;
    }

    public void setCdate(Integer cdate) {
        this.cdate = cdate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    public Integer getOtype() {
        return otype;
    }

    public void setOtype(Integer otype) {
        this.otype = otype;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", gno=").append(gno);
        sb.append(", gname=").append(gname);
        sb.append(", status=").append(status);
        sb.append(", cdate=").append(cdate);
        sb.append(", note=").append(note);
        sb.append(", otype=").append(otype);
        sb.append(", price=").append(price);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}