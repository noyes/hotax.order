package liqiang365.model.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author 吴华龙
 * @classname LiveConfig
 * @description 直播配置
 * @date 2019/11/20 15:30
 */
@Table(name = "live_config")
@Data
public class LiveConfig {

    @Id
    private Integer id;

    @Column(name = "`key`")
    private String key;

    @Column(name = "`value`")
    private String value;

    private String createTime;

    private String updateTime;

    private String deleteState;

    private String createUid;

    private String lastUpdateUid;

    private String tid;


}
