package liqiang365.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 直播配合表
 */
@Table(name = "live_system_config")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "直播配置实体")
public class LiveSystemConfig {

    @Id
    @ApiModelProperty(value = "配置ID")
    private String id;

    @ApiModelProperty(value = "创建时间")
    private String createTime;

    @ApiModelProperty(value = "配置Key")
    private String configKey;

    @ApiModelProperty(value = "配置Value")
    private String configValue;

    @ApiModelProperty(value = "备注")
    private String remark;


}
