/**
* @filename:LiveRoomManage 2019年11月22日
    * @project liqiang365  V1.0
    * Copyright(c) 2020 严聪 Co. Ltd.
    * All right reserved.
    */
    package liqiang365.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
* <p>自动生成工具：mybatis-dsc-generator</p>
*
* <p>说明： 用户实体类</P>
* @version: V1.0
* @author: 严聪
*
*/
@Table(name="live_room_manage")
@Data
@ApiModel
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class LiveRoomManage  implements Serializable {

private static final long serialVersionUID = 1574414589188L;

    @Id
    @ApiModelProperty(name = "id" , value = "主键")
    private String id;
    @ApiModelProperty(name = "createTime" , value = "创建时间")
    private String createTime;
    @ApiModelProperty(name = "updateTime" , value = "更新类型")
    private String updateTime;
    @ApiModelProperty(name = "deleteState" , value = "删除状态")
    private String deleteState;
    @ApiModelProperty(name = "roomId" , value = "主播id")
    private Integer roomId;
    @ApiModelProperty(name = "roomUid" , value = "主播在用户表id")
    private String roomUid;
    @ApiModelProperty(name = "uid" , value = "房管用户id")
    private String uid;
    @ApiModelProperty(name = "type" , value = "0=场控  1=超管")
    private Integer type;

    @ApiModelProperty(name = "tid" , value = "租户id")
    private String tid;

}
