package liqiang365.model.entity;

import hotax.core.valid.UpdateCheck;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.validation.constraints.NotBlank;

@ApiModel(value ="live_shop_order", description ="live_shop_order" )
public class LiveShopOrder implements Serializable {
    /**
     * 订单id(商城系统生成)
     */
    @ApiModelProperty(value ="订单id(商城系统生成)", required = true)
    @NotBlank(message="字段id不能为空",groups ={UpdateCheck.class})
    private String id;

    /**
     * 主播用户id
     */
    @ApiModelProperty(value ="主播用户id", required = true)
    @NotBlank(message="字段uid不能为空")
    private String uid;

    /**
     * 直播id
     */
    @ApiModelProperty(value ="直播id", required = true)
    @NotBlank(message="字段live不能为空")
    private String live;

    /**
     * 房间号
     */
    @ApiModelProperty(value ="房间号", required = true)
    private Integer room;

    /**
     * 订单金额
     */
    @ApiModelProperty(value ="订单金额", required = false)
    private BigDecimal money;

    /**
     * 收益金额
     */
    @ApiModelProperty(value ="收益金额", required = false)
    private BigDecimal income;

    /**
     * 订单状态 0:已经创建 1.支付成功 2.支付失败 3.已经同步到余额  4.已经退款

所有:1+3+2+4
待入账:1
已入账:3
已退款:2,4
     */
    @ApiModelProperty(value ="订单状态0:已经创建1.支付成功2.支付失败3.已经同步到余额4.已经退款所有:1+3+2+4待入账:1已入账:3已退款:2,4", required = true)
    private Integer status;

    /**
     * 订单时间
     */
    @ApiModelProperty(value ="订单时间", required = true)
    private Integer cdate;

    /**
     * 支付时间
     */
    @ApiModelProperty(value ="支付时间", required = false)
    private Integer pdate;

    /**
     * 入账时间
     */
    @ApiModelProperty(value ="入账时间", required = false)
    private Integer mdate;

    /**
     * 退款时间
     */
    @ApiModelProperty(value ="退款时间", required = false)
    private Integer rdate;

    /**
     * 购买者用户id
     */
    @ApiModelProperty(value ="购买者用户id", required = false)
    private String bid;

    /**
     * 0:商品订单 1:会员订单
     */
    @ApiModelProperty(value ="0:商品订单1:会员订单", required = false)
    private Integer type;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid == null ? null : uid.trim();
    }

    public String getLive() {
        return live;
    }

    public void setLive(String live) {
        this.live = live == null ? null : live.trim();
    }

    public Integer getRoom() {
        return room;
    }

    public void setRoom(Integer room) {
        this.room = room;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public BigDecimal getIncome() {
        return income;
    }

    public void setIncome(BigDecimal income) {
        this.income = income;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCdate() {
        return cdate;
    }

    public void setCdate(Integer cdate) {
        this.cdate = cdate;
    }

    public Integer getPdate() {
        return pdate;
    }

    public void setPdate(Integer pdate) {
        this.pdate = pdate;
    }

    public Integer getMdate() {
        return mdate;
    }

    public void setMdate(Integer mdate) {
        this.mdate = mdate;
    }

    public Integer getRdate() {
        return rdate;
    }

    public void setRdate(Integer rdate) {
        this.rdate = rdate;
    }

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid == null ? null : bid.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", uid=").append(uid);
        sb.append(", live=").append(live);
        sb.append(", room=").append(room);
        sb.append(", money=").append(money);
        sb.append(", income=").append(income);
        sb.append(", status=").append(status);
        sb.append(", cdate=").append(cdate);
        sb.append(", pdate=").append(pdate);
        sb.append(", mdate=").append(mdate);
        sb.append(", rdate=").append(rdate);
        sb.append(", bid=").append(bid);
        sb.append(", type=").append(type);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}