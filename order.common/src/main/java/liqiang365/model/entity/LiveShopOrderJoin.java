package liqiang365.model.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class LiveShopOrderJoin {

    /**
     * 订单id(商城系统生成)
     */
    @ApiModelProperty(value ="订单id(商城系统生成)", required = true)
    private String id;

    /**
     * 用户id
     */
    @ApiModelProperty(value ="用户id", required = true)
    private String uid;

    /**
     * 直播id
     */
    @ApiModelProperty(value ="直播id", required = true)
    private String live;

    /**
     * 直播名称
     */
    @ApiModelProperty(value ="直播名称", required = true)
    private String liveName;

    /**
     * 房间号
     */
    @ApiModelProperty(value ="房间号", required = true)
    private Integer room;

    /**
     * 订单金额
     */
    @ApiModelProperty(value ="订单金额", required = true)
    private BigDecimal money;

    /**
     * 收益金额
     */
    @ApiModelProperty(value ="收益金额", required = true)
    private BigDecimal income;

    /**
     * 订单状态 0:已经创建 1.支付成功 2.支付失败 3.已经同步到余额  4.已经退款
     */
    @ApiModelProperty(value ="订单状态0:已经创建1.支付成功2.支付失败3.已经同步到余额4.已经退款", required = true)
    private Integer status;

    /**
     * 订单时间
     */
    @ApiModelProperty(value ="订单时间", required = true)
    private Integer cdate;

    /**
     * 入账时间
     */
    @ApiModelProperty(value ="入账时间", required = false)
    private Integer mdate;

    /**
     * 退款时间
     */
    @ApiModelProperty(value ="退款时间", required = false)
    private Integer rdate;


    /**
     * 商品id
     */
    @ApiModelProperty(value ="商品id", required = false)
    private String goods;

    /**
     * 商品数量
     */
    @ApiModelProperty(value ="商品数量", required = false)
    private Integer num;

    /**
     * 商品名称
     */
    @ApiModelProperty(value ="商品名称", required = true)
    private String gname;

    /**
     * 商品图片url
     */
    @ApiModelProperty(value ="商品图片url", required = true)
    private String gurl;


}
