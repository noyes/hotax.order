package liqiang365.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 孙冠杰
 * @classname LiveFinanceReq
 * @description 直播充值查询req
 * @date 2019/11/20 16:06
 */
@Data
@ApiModel(value = "直播充值查询req")
@AllArgsConstructor
@NoArgsConstructor
public class LiveFinanceReq {

    @ApiModelProperty(value = "查询类型 1=用户昵称 2=用户手机号 3=订单号")
    private String queryType;

    @ApiModelProperty(value = "查询内容")
    private String queryContent;

    @ApiModelProperty(value = "订单开始时间")
    private String orderStartDate;

    @ApiModelProperty(value = "订单结束时间")
    private String orderEndDate;

    @ApiModelProperty(value = "订单状态 -3系统异常 -2=已退款 -1=已超时 0=已取消 1=待支付 2=已支付")
    private String orderState;

    @ApiModelProperty(value = "订单状渠道来源 1=APP 2=微信小程序 3=微信H5店铺")
    private String orderSource;

    @ApiModelProperty(value = "支付方式 1=支付宝支付 2=微信支付 ")
    private String payType;

    @ApiModelProperty(value = "收款方式 1= 平台代收 2= 商户自有")
    private String incomeType;

    private Integer pageNo;
}
