package liqiang365.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * PK对战记录
 *
 * @author 苏
 * @date 2020-03-18 14:35:05
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "PK对战记录")
@Builder
@Table(name = "live_pk_record")
public class LivePkRecord implements Serializable {

    private static final long serialVersionUID = 1041930043884365992L;
    /**
     * 主键
     */
    @Column(name = "id")
    @ApiParam("主键")
    @ApiModelProperty(value = "主键")
    @Id
    private String id;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    @ApiParam("创建时间")
    @ApiModelProperty(value = "创建时间")
    private String createTime;

    /**
     * 删除：1=删除；0=未删除
     */
    @Column(name = "delete_state")
    @ApiParam("删除：1=删除；0=未删除")
    @ApiModelProperty(value = "删除：1=删除；0=未删除")
    private String deleteState;

    /**
     * PK发起方
     */
    @Column(name = "uid")
    @ApiParam("PK发起方")
    @ApiModelProperty(value = "PK发起方")
    private String uid;

    /**
     * PK发起方收益智慧豆
     */
    @Column(name = "qb_income")
    @ApiParam("PK发起方收益智慧豆")
    @ApiModelProperty(value = "PK发起方收益智慧豆")
    private Integer qbIncome;

    /**
     * PK发起方直播id
     */
    @Column(name = "live_id")
    @ApiParam("PK发起方直播id")
    @ApiModelProperty(value = "PK发起方直播id")
    private String liveId;

    /**
     * PK发起方直播名
     */
    @Column(name = "live_name")
    @ApiParam("PK发起方直播名")
    @ApiModelProperty(value = "PK发起方直播名")
    private String liveName;

    /**
     * PK接受方
     */
    @Column(name = "pk_uid")
    @ApiParam("PK接受方")
    @ApiModelProperty(value = "PK接受方")
    private String pkUid;

    /**
     * PK接受方收益智慧豆
     */
    @Column(name = "pk_qb_income")
    @ApiParam("PK接受方收益智慧豆")
    @ApiModelProperty(value = "PK接受方收益智慧豆")
    private Integer pkQbIncome;

    /**
     * PK接受方直播id
     */
    @Column(name = "pk_live_id")
    @ApiParam("PK接受方直播id")
    @ApiModelProperty(value = "PK接受方直播id")
    private String pkLiveId;

    /**
     * PK接受方直播名
     */
    @Column(name = "pk_live_name")
    @ApiParam("PK接受方直播名")
    @ApiModelProperty(value = "PK接受方直播名")
    private String pkLiveName;

    /**
     * PK开始时间
     */
    @Column(name = "start_time")
    @ApiParam("PK开始时间")
    @ApiModelProperty(value = "PK开始时间")
    private String startTime;

    /**
     * PK结束时间
     */
    @Column(name = "end_time")
    @ApiParam("PK结束时间")
    @ApiModelProperty(value = "PK结束时间")
    private String endTime;

    /**
     * PK时长(秒)
     */
    @Column(name = "duration")
    @ApiParam("PK时长(秒)")
    @ApiModelProperty(value = "PK时长(秒)")
    private Integer duration;

    /**
     * PK结果 -1=PK方胜；0=平；1=发起方胜
     */
    @Column(name = "result")
    @ApiParam("PK结果 -1=PK方胜；0=平；1=发起方胜")
    @ApiModelProperty(value = "PK结果 -1=PK方胜；0=平；1=发起方胜")
    private Integer result;

    /**
     * 惩罚状态：1=打开；0=关闭；
     */
    @Column(name = "damp_state")
    @ApiParam("惩罚状态：1=打开；0=关闭；")
    @ApiModelProperty(value = "惩罚状态：1=打开；0=关闭；")
    private Integer dampState;

    /**
     * 惩罚时间设置(秒)
     */
    @Column(name = "pentime")
    @ApiParam("惩罚时间设置(秒)")
    @ApiModelProperty(value = "惩罚时间设置(秒)")
    private Integer pentime;

    /**
     * 剩余惩罚时间(秒)
     */
    @Column(name = "remain_pentime")
    @ApiParam("剩余惩罚时间(秒)")
    @ApiModelProperty(value = "剩余惩罚时间(秒)")
    private Integer remainPentime;

    /**
     * PK当前所处状态：1=pk中；0=pk结束；
     */
    @Column(name = "pk_state")
    @ApiParam("PK当前所处状态：1=pk中；0=pk结束；")
    @ApiModelProperty(value = "PK当前所处状态：1=pk中；0=pk结束；")
    private String pkState;

    /**
     * PK发起方房间
     */
    @Column(name = "room_id")
    @ApiParam("PK发起方房间")
    @ApiModelProperty(value = "PK发起方房间")
    private Integer roomId;

    /**
     * PK接受方房间
     */
    @Column(name = "pk_room_id")
    @ApiParam("PK接受方房间")
    @ApiModelProperty(value = "PK接受方房间")
    private Integer pkRoomId;

    /**
     * PK类型：1=随机；2=定向；
     */
    @Column(name = "pk_type")
    @ApiParam("PK类型：1=随机；2=定向；")
    @ApiModelProperty(value = "PK类型：1=随机；2=定向；")
    private Integer pkType;

    /**
     * 结束PK人（可能是PK双方，也可能是被job、或者后台关闭）
     */
    @Column(name = "end_pk_uid")
    @ApiParam("结束PK人（可能是PK双方，也可能是被job、或者后台关闭）")
    @ApiModelProperty(value = "结束PK人（可能是PK双方，也可能是被job、或者后台关闭）")
    private String endPkUid;

}
