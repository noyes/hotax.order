package liqiang365.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author 吴华龙
 * @classname LiveVirtual
 * @description 主播机器人配置表
 * @date 2019/11/21 14:12
 */
@ApiModel
@Table(name = "live_anchor_robot")
@Data
public class LiveAnchorRobot implements java.io.Serializable{


    /**
     * 机器人配置
     */
    @ApiModelProperty("机器人配置")
    private String robot;
    @Id
    @ApiModelProperty("房间id")
    private Integer roomId;
    @ApiModelProperty("租户id")
    private String tid;
}
