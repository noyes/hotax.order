/**
 * @filename:LiveClass 2019年11月22日
 * @project liqiang365  V1.0
 * Copyright(c) 2020 严聪 Co. Ltd.
 * All right reserved.
 */
package liqiang365.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * <p>自动生成工具：mybatis-dsc-generator</p>
 *
 * <p>说明： 用户实体类</P>
 *
 * @version: V1.0
 * @author: 严聪
 */
@Table(name = "live_class")
@Data
@ApiModel
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@EqualsAndHashCode(callSuper = true)
public class LiveClass extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1574414695506L;


    @ApiModelProperty(name = "tid", value = "站点ID")
    private String tid;
    @ApiModelProperty(name = "createUid", value = "创建者id")
    private String createUid;
    @ApiModelProperty(name = "lastUpdateUid", value = "最后更新者id")
    private String lastUpdateUid;
    @ApiModelProperty(name = "icon", value = "分类图标")
    private String icon;
    @ApiModelProperty(name = "name", value = "分类名称")
    private String name;
    @ApiModelProperty(name = "info", value = "描述")
    private String info;
    @ApiModelProperty(name = "seq", value = "排序")
    private Integer seq;
    @ApiModelProperty(name = "enableState", value = "0禁用1启用")
    private String enableState;

}