package liqiang365.model.em;

/**
 * Description: 资金记录类型枚举
 * Author:     gaocl
 * Date:       2016/12/7
 * Version:     V1.0.0
 * Update:     更新说明
 */
public enum MoneyRecordTypeEnum {

    ALIPAY("支付宝", 1),
    WEIXIN("微信", 2),
    Unionpay("银联", 3),
    BALANCE("余额", 4),
    INCOME("奖励", 5),
    TRANSFER("转账", 6),
    MONEYDRAW("提现", 7),
    SIGNIN("签到", 8),
    SYSTEM("后台修改", 9),
    YEEPAY("易宝支付", 10),
    GROUPBUY("团购人工发放佣金", 11),
    MICROUSER("小微店主购物返佣", 12),
    SHOP("商城购物", 13),
    UP_SHOP_VIP("升级商城vip",14),
    BONUS("分红",15);

    private String name;
    private int value;

    MoneyRecordTypeEnum(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

  
    public int getValue() {
        return value;
    }

    public static String getNameByValue(int value){
        MoneyRecordTypeEnum[] moneyRecordTypeEnums = MoneyRecordTypeEnum.values();
        for(MoneyRecordTypeEnum em : moneyRecordTypeEnums){
                if(em.getValue() == value){
                    return em.getName();
                }
        }
        return "无此类型";
    }

}
