package liqiang365.model.em;

import java.util.ArrayList;
import java.util.List;

/**
 * @author admin
 */

public enum MqMessageTagEnum {
    /**
     *
     */
    TEST_MESSAGE("测试消息接收", "testMessage"),
    ANCHOR_SALE_COURSE("主播课程带货", "anchor_sale_course"),
    COURSE_UPDATE("课程信息变动", "course_update"),
    LIVEUSER_BARRAGE("机器人发送弹幕", "liveuser_barrage");

    MqMessageTagEnum(String name, String value) {
        this.name = name;
        this.value = value;
    }

    private String name;
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static List<String> getAllMessageTagValue() {
        List<String> list = new ArrayList<>();
        MqMessageTagEnum[] enums = MqMessageTagEnum.values();
        for (MqMessageTagEnum mqMessage : enums) {
            list.add(mqMessage.getValue());
        }
        return list;
    }
}
