package liqiang365.model.em;

/**
 * Description: client枚举
 * Author:     gaocl
 * Date:       2017/3/13
 * Version:     V1.0.0
 * Update:     更新说明
 */
public enum ClientTypeEnum {

    LIQIANG365("Liqiang365", "1"),
    TOGNSHUAI("统帅APP", "2"),
    ENTPUSER("企业用户定制版", "3");

    private String name;
    private String value;
    ClientTypeEnum(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }


}
