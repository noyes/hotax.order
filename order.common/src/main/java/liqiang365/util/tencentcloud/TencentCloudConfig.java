package liqiang365.util.tencentcloud;


import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.live.v20180801.LiveClient;
import com.tencentcloudapi.vod.v20180717.VodClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import javax.annotation.PostConstruct;

/**
 * Description:腾讯云SDK配置
 * Author:     su
 * Date:       2019-11-21 12:33:36
 * Version:    V1.0.0
 * Update:     更新说明
 */
@Configuration
@PropertySource(value = {"file:config/hotax-${spring.profiles.active}.properties", "config/hotax-${spring.profiles.active}.properties", "file:config/hotax.properties", "config/hotax.properties" }, ignoreResourceNotFound = true, encoding = "UTF-8")
public class TencentCloudConfig {

    public String secretId;

    public String secretKey;

    public String regionId;


    @Autowired
    Environment env;

    @PostConstruct
    public void initConfig(){
        this.secretId = env.getProperty("live.tencentcloud.secretId");
        this.secretKey = env.getProperty("live.tencentcloud.secretKey");
        this.regionId = env.getProperty("live.tencentcloud.regionId");
    }

    @Bean
    public LiveClient liveClient(TencentCloudConfig cloudConfig) {
        HttpProfile httpProfile = new HttpProfile();
        // post请求(默认为post请求)
        httpProfile.setReqMethod("POST");
        // 请求连接超时时间，单位为秒(默认60秒)
        httpProfile.setConnTimeout(60);
        ClientProfile clientProfile = new ClientProfile();
        clientProfile.setHttpProfile(httpProfile);
        clientProfile.setSignMethod(ClientProfile.SIGN_TC3_256);
        Credential credential = new Credential(cloudConfig.secretId, cloudConfig.secretKey);
        return new LiveClient(credential, null,clientProfile);
    }

    @Bean
    public VodClient vodClient(TencentCloudConfig cloudConfig) {
        HttpProfile httpProfile = new HttpProfile();
        // post请求(默认为post请求)
        httpProfile.setReqMethod("POST");
        // 请求连接超时时间，单位为秒(默认60秒)
        httpProfile.setConnTimeout(60);
        ClientProfile clientProfile = new ClientProfile();
        clientProfile.setHttpProfile(httpProfile);
        clientProfile.setSignMethod(ClientProfile.SIGN_TC3_256);
        Credential credential = new Credential(cloudConfig.secretId, cloudConfig.secretKey);
        return new VodClient(credential, null,clientProfile);
    }

}
