package liqiang365.util;

import java.text.DecimalFormat;

/**
 * description
 *
 * @author f
 * @date 2019/6/28 15:16
 **/
public class DoubleUtil {
    public static String formatDouble(Object o) {
        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        String result = decimalFormat.format(o);
        return result.replace(".00","");
    }

    public static void main(String[] args) {
        System.out.println(formatDouble(1000.0001));
        System.out.println(formatDouble(1000.0401));
        System.out.println(formatDouble(1000.0091));
        System.out.println(formatDouble(1000.00));
        System.out.println(formatDouble(1000.01));
    }
}
