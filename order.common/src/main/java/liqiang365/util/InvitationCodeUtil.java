package liqiang365.util;

import java.util.Random;

/* modified by 董东贤 at 2016.9.13 删除有歧义字符或数字 */
public class InvitationCodeUtil {
    public static String getRandomString(int length) { //length表示生成字符串的长度
        String base = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }
}