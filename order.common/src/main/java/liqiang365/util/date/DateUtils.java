package liqiang365.util.date;


import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 日期处理工具函数包,包括日期对象、日期字符串相关转换函数
 *
 * @version 1.0 <br>
 */
public class DateUtils {
    /**
     * 定义常见的时间格式
     */
    private static String[] dateFormat = {"yyyy-MM-dd HH:mm:ss", "yyyy/MM/dd HH:mm:ss", "yyyy年MM月dd日HH时mm分ss秒", "yyyy-MM-dd",
            "yyyy/MM/dd", "yy-MM-dd", "yy/MM/dd", "yyyy年MM月dd日", "HH:mm:ss", "yyyyMMddHHmmss", "yyyyMMdd", "yyyy.MM.dd", "yy.MM.dd",
            "MM月dd日HH时mm分", "yyyy年MM月dd日 HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM-dd 00:00:00", "yyyy-MM"};

    /**
     * <p>
     * <b>作者：</b>Administrator
     * </p>
     * <p>
     * <b>日期：</b>2011-9-8
     * </p>
     * <p>
     * <b>方法描述：</b> 将日期格式从 java.util.Calendar 转到 java.sql.Timestamp 格式
     * </p>
     *
     * @param date 格式表示的日期
     * @return <p>
     * <b>使用说明：</b>
     * </p>
     */
    public static Timestamp convUtilCalendarToSqlTimestamp(Calendar date) {
        if (date == null) {
            return null;
        } else {
            return new Timestamp(date.getTimeInMillis());
        }
    }

    /**
     * <p>
     * <b>作者：</b>Administrator
     * </p>
     * <p>
     * <b>日期：</b>2011-9-8
     * </p>
     * <p>
     * <b>方法描述：</b> 将日期格式从 java.util.Timestamp 转到 java.util.Calendar 格式
     * </p>
     *
     * @param date 格式表示的日期
     * @return <p>
     * <b>使用说明：</b>
     * </p>
     */
    public static Calendar convSqlTimestampToUtilCalendar(Timestamp date) {
        if (date == null) {
            return null;
        } else {
            GregorianCalendar gc = new GregorianCalendar();
            gc.setTimeInMillis(date.getTime());
            return gc;
        }
    }

    public static String secToTime(int time) {
        String timeStr = null;
        int hour = 0;
        int minute = 0;
        int second = 0;
        if (time <= 0) {
            return "00:00";
        }
        else {
            minute = time / 60;
            if (minute < 60) {
                second = time % 60;
                timeStr = unitFormat(minute) + ":" + unitFormat(second);
            } else {
                hour = minute / 60;
                if (hour > 99) {
                    return "99:59:59";
                }
                minute = minute % 60;
                second = time - hour * 3600 - minute * 60;
                timeStr = unitFormat(hour) + ":" + unitFormat(minute) + ":" + unitFormat(second);
            }
        }
        return timeStr;
    }


    public static String unitFormat(int i) {
        String retStr = null;
        if (i >= 0 && i < 10) {
            retStr = "0" + Integer.toString(i);
        }
        else {
            retStr = "" + i;
        }
        return retStr;
    }

    /**
     * <p>
     * <b>作者：</b>Administrator
     * </p>
     * <p>
     * <b>日期：</b>2011-9-8
     * </p>
     * <p>
     * <b>方法描述：</b> 解析一个字符串，形成一个Calendar对象，适应各种不同的日期表示法
     * </p>
     *
     * @param dateStr 期望解析的字符串，注意，不能传null进去，否则出错
     * @return <p>
     * <b>使用说明：</b> <br>
     * <br>
     * 可输入的日期字串格式如下： <br>
     * "yyyy-MM-dd HH:mm:ss", <br>
     * "yyyy/MM/dd HH:mm:ss", <br>
     * "yyyy年MM月dd日HH时mm分ss秒", <br>
     * "yyyy-MM-dd", <br>
     * "yyyy/MM/dd", <br>
     * "yy-MM-dd", <br>
     * "yy/MM/dd", <br>
     * "yyyy年MM月dd日", <br>
     * "HH:mm:ss", <br>
     * "yyyyMMddHHmmss", <br>
     * "yyyyMMdd", <br>
     * "yyyy.MM.dd", <br>
     * "yy.MM.dd"
     * </p>
     */
    public static Calendar parseDate(String dateStr) {
        if (dateStr == null || dateStr.trim().length() == 0) {
            return null;
        }

        Date result = parseDate(dateStr, 0);

        Calendar cal = Calendar.getInstance();
        cal.setTime(result);

        return cal;
    }

    /**
     * <p>
     * <b>作者：</b>Administrator
     * </p>
     * <p>
     * <b>日期：</b>2011-9-8
     * </p>
     * <p>
     * <b>方法描述：</b> 将一个日期转成日期时间格式，格式这样 2002-08-05 21:25:21
     * </p>
     *
     * @param date 期望格式化的日期对象
     * @return 返回格式化后的字符串
     * <p>
     * <b>使用说明：</b> <br>
     * Calendar date = new GregorianCalendar(); <br>
     * String ret = DateUtils.toDateTimeStr(date); <br>
     * 返回： <br>
     * ret = "2002-12-04 09:13:16";
     * </p>
     */
    public static String toDateTimeStr(Calendar date) {
        if (date == null) {
            return null;
        }
        return new SimpleDateFormat(dateFormat[0]).format(date.getTime());
    }

    /**
     * <p>
     * <b>作者：</b>Administrator
     * </p>
     * <p>
     * <b>日期：</b>2011-9-8
     * </p>
     * <p>
     * <b>方法描述：</b> 将一个日期转成日期时间格式
     * </p>
     *
     * @param format 日志格式序号
     * @param date   期望格式化的日期对象
     * @return <p>
     * <b>使用说明：</b>
     * </p>
     */
    public static String toDateTimeStr(int format, Calendar date) {
        if (date == null) {
            return null;
        }

        return new SimpleDateFormat(dateFormat[format]).format(date.getTime());
    }

    /**
     * <p>
     * <b>作者：</b>Administrator
     * </p>
     * <p>
     * <b>日期：</b>2011-9-8
     * </p>
     * <p>
     * <b>方法描述：</b> 将一个日期转成日期格式， 格式：yyyy-MM-dd
     * </p>
     *
     * @param date 期望格式化的日期对象
     * @return 返回格式化后的字符串
     * <p>
     * <b>使用说明：</b>
     * </p>
     */
    public static String toDateStr(Calendar date) {
        if (date == null) {
            return null;
        }
        return new SimpleDateFormat(dateFormat[3]).format(date.getTime());
    }

    /**
     * <p>
     * <b>作者：</b>Administrator
     * </p>
     * <p>
     * <b>日期：</b>2011-9-8
     * </p>
     * <p>
     * <b>方法描述：</b> 根据format数组的序号，将Calendar转换为对应格式的String
     * </p>
     *
     * @param date        要转换的日期对象
     * @param formatIndex format数组中的索引
     * @return 返回格式化后的字符串
     * <p>
     * <b>使用说明：</b>
     * </p>
     */
    public static String toDateStrByFormatIndex(Calendar date, int formatIndex) {
        if (date == null) {
            return null;
        }
        return new SimpleDateFormat(dateFormat[formatIndex]).format(date.getTime());
    }

    /**
     * <p>
     * <b>作者：</b>Administrator
     * </p>
     * <p>
     * <b>日期：</b>2011-9-8
     * </p>
     * <p>
     * <b>方法描述：</b> 第一个日期与第二个日期相差的天数
     * </p>
     *
     * @param d1 第一个日期
     * @param d2 第二个日期
     * @return 两个日期之间相差的天数
     * <p>
     * <b>使用说明：</b>
     * </p>
     */
    public static int calendarMinus(Calendar d1, Calendar d2) {
        if (d1 == null || d2 == null) {
            return 0;
        }

        d1.set(Calendar.HOUR_OF_DAY, 0);
        d1.set(Calendar.MINUTE, 0);
        d1.set(Calendar.SECOND, 0);

        d2.set(Calendar.HOUR_OF_DAY, 0);
        d2.set(Calendar.MINUTE, 0);
        d2.set(Calendar.SECOND, 0);

        long t1 = d1.getTimeInMillis();
        long t2 = d2.getTimeInMillis();
        long daylong = 3600 * 24 * 1000;
        t1 = t1 - t1 % (daylong);
        t2 = t2 - t2 % (daylong);

        long t = t1 - t2;
        int value = (int) (t / (daylong));
        return value;
    }

    /**
     * <p>
     * <b>作者：</b>Administrator
     * </p>
     * <p>
     * <b>日期：</b>2011-9-8
     * </p>
     * <p>
     * <b>方法描述：</b> 第一个日期与第二个日期相差的天数
     * </p>
     *
     * @param d1 第一个日期
     * @param d2 第二个日期
     * @return 两个日期之间相差的天数
     * <p>
     * <b>使用说明：</b>
     * </p>
     */
    public static long calendarminus(Calendar d1, Calendar d2) {
        if (d1 == null || d2 == null) {
            return 0;
        }
        return (d1.getTimeInMillis() - d2.getTimeInMillis()) / (3600 * 24000);
    }

    /**
     * <p>
     * <b>作者：</b>Administrator
     * </p>
     * <p>
     * <b>日期：</b>2011-9-8 10:00:00
     * </p>
     * <p>
     * <b>方法描述：</b> 第一个日期与第二个日期相差的秒数
     * </p>
     *
     * @param d1 第一个日期
     * @param d2 第二个日期
     * @return 两个日期之间相差的秒数
     * <p>
     * <b>使用说明：</b>
     * </p>
     */
    public static long calendarTime(Calendar d1, Calendar d2) {
        if (d1 == null || d2 == null) {
            return 0;
        }
        return (d1.getTimeInMillis() - d2.getTimeInMillis()) / 1000;
    }

    /**
     * <p>
     * <b>作者：</b>Administrator
     * </p>
     * <p>
     * <b>日期：</b>2011-9-9
     * </p>
     * <p>
     * <b>方法描述：</b> 内部方法，根据某个索引中的日期格式解析日期
     * </p>
     *
     * @param dateStr 日期的字符串形式
     * @param index   日期格式索引
     * @return 符合日期字符串的日期对象
     * <p>
     * <b>使用说明：</b>
     * </p>
     */
    public static Date parseDate(String dateStr, int index) {
        DateFormat df = null;
        try {
            df = new SimpleDateFormat(dateFormat[index]);
            return df.parse(dateStr);
        } catch (Exception aioe) {
            return null;
        }
    }

    /**
     * <p>
     * <b>作者：</b>Administrator
     * </p>
     * <p>
     * <b>日期：</b>2011-9-9
     * </p>
     * <p>
     * <b>方法描述：</b> 字符转日期,字符串格式："yyyy-MM-dd"，例如2006-01-01
     * </p>
     *
     * @param dateStr 日期的字符串形式
     * @return 符合日期字符串的日期对象
     * <p>
     * <b>使用说明：</b>
     * </p>
     */
    public static Date StringToDate(String dateStr) {
        if (dateStr == null || dateStr.trim().length() == 0) {
            return null;
        }
        return parseDate(dateStr, 3);
    }

    /**
     * <p>
     * <b>作者：</b>Administrator
     * </p>
     * <p>
     * <b>日期：</b>2011-9-9
     * </p>
     * <p>
     * <b>方法描述：</b> 将日期转换为格式化后的日期字符串
     * </p>
     *
     * @param date  日期
     * @param index 日期格式索引
     * @return <p>
     * <b>使用说明：</b>
     * </p>
     */
    public static String dateToString(Date date, int index) {
        if (date == null) {
            return null;
        }
        return new SimpleDateFormat(dateFormat[index]).format(date);
    }

    /**
     * <p>
     * <b>作者：</b>Administrator
     * </p>
     * <p>
     * <b>日期：</b>2011-9-9
     * </p>
     * <p>
     * <b>方法描述：</b> 返回固定格式的日期字符串。转换结果格式为："yyyy-MM-dd"
     * </p>
     *
     * @param date 待转换的日期对象
     * @return <p>
     * <b>使用说明：</b>
     * </p>
     */
    public static String dateToString(Date date) {
        if (date == null) {
            return null;
        }
        return new SimpleDateFormat(dateFormat[3]).format(date);
    }

    /**
     * @param @return 设定文件
     * @return String 返回类型
     * @throws
     * @Title: getDate
     * @Description: 获取当前日期，"yyyy-MM-dd"
     */
    public static String getDate() {

        return new SimpleDateFormat(dateFormat[3]).format(new Date());
    }

    /**
     * @param @return 设定文件
     * @return String 返回类型
     * @throws
     * @Title: getDateTime
     * @Description: 获取当前日期，"yyyy-MM-dd HH:mm:ss:SSS"
     */
    public static String getDateTime() {

        return new SimpleDateFormat(dateFormat[0]).format(new Date());
    }

    public static String getDateTimeHHMM(String datetime) throws ParseException {
        return new SimpleDateFormat(dateFormat[15]).format(new SimpleDateFormat(dateFormat[15]).parse(datetime));
    }

    /**
     * <p>
     * <b>作者：</b>Administrator
     * </p>
     * <p>
     * <b>日期：</b>2011-9-9
     * </p>
     * <p>
     * <b>方法描述：</b> 将日期格式从 java.util.Date 转到 java.sql.Timestamp 格式
     * </p>
     *
     * @param date
     * @return <p>
     * <b>使用说明：</b>
     * </p>
     */
    public static Timestamp convUtilDateToSqlTimestamp(Date date) {
        if (date == null) {
            return null;
        } else {
            return new Timestamp(date.getTime());
        }
    }

    /**
     * <p>
     * <b>作者：</b>Administrator
     * </p>
     * <p>
     * <b>日期：</b>2011-9-9
     * </p>
     * <p>
     * <b>方法描述：</b> 将Date对象转换成Calendar对象
     * </p>
     *
     * @param date
     * @return <p>
     * <b>使用说明：</b>
     * </p>
     */
    public static Calendar convUtilDateToUtilCalendar(Date date) {
        if (date == null) {
            return null;
        } else {
            GregorianCalendar gc = new GregorianCalendar();
            gc.setTimeInMillis(date.getTime());
            return gc;
        }
    }

    /**
     * <p>
     * <b>作者：</b>Administrator
     * </p>
     * <p>
     * <b>日期：</b>2011-9-9
     * </p>
     * <p>
     * <b>方法描述：</b> 根据某个索引中的日期格式解析日期
     * </p>
     *
     * @param dateStr 日期
     * @param index   日期格式的索引
     * @return <p>
     * <b>使用说明：</b>
     * </p>
     */
    public static Timestamp parseTimestamp(String dateStr, int index) {
        DateFormat df = null;
        try {
            df = new SimpleDateFormat(dateFormat[index]);
            return new Timestamp(df.parse(dateStr).getTime());
        } catch (ParseException pe) {
            return new Timestamp(parseDate(dateStr, index + 1).getTime());
        } catch (ArrayIndexOutOfBoundsException aioe) {
            return null;
        }
    }

    /**
     * <p>
     * <b>作者：</b>Administrator
     * </p>
     * <p>
     * <b>日期：</b>2011-9-9
     * </p>
     * <p>
     * <b>方法描述：</b> 返回固定格式的日期字符串。转换结果格式为："yyyy-MM-dd"
     * </p>
     *
     * @param dateStr 待转换的日期对象
     * @return <p>
     * <b>使用说明：</b>
     * </p>
     */
    public static Timestamp parseTimestamp(String dateStr) {
        DateFormat df = null;
        try {
            df = new SimpleDateFormat(dateFormat[3]);
            return new Timestamp(df.parse(dateStr).getTime());
        } catch (ParseException pe) {
            return null;
        } catch (ArrayIndexOutOfBoundsException aioe) {
            return null;
        }
    }

    /**
     * @param calendar
     * @param days
     * @return Calendar 返回类型
     * @Title: calculateCalendarByday
     * @Description: 日期加减天数
     */
    public static Calendar calculateCalendarByday(Calendar calendar, int days) {
        calendar.add(Calendar.DAY_OF_MONTH, days);
        return calendar;
    }

    /**
     * 获取倒计时
     *
     * @param countdownTime 倒计时的时间，格式：yyyy-MM-dd HH:mm:ss
     * @return String 为null时转换失败
     * 倒计时够天数显示天数，不够显示小时，不够小时显示分钟
     * 2天后面没有1天，而是24小时，2小时同理
     */
    public static String getCountdownTime(String countdownTime) {
        try {
            Calendar c = Calendar.getInstance();
            c.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(countdownTime));// 将字符串转换为可获取毫秒数的日期类型
            long nowDate = System.currentTimeMillis();
            long countdown = (c.getTimeInMillis() - nowDate) / (1000 * 60);// 获取倒计时（分钟）
            long days = countdown / (60 * 24);// 天
            long hours = countdown / 60 % 24;// 小时
            long mins = countdown % 60;// 分钟
            if (days > 0) {
                return (days + 1) + "天";
            }
            if (hours > 0) {
                return (hours + 1) + "小时";
            }
            if (mins > 0) {
                return mins + "分钟";
            }
            return "0分钟";
        } catch (ParseException e) {

            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取时间节点是在当前时间节点多长时间以前
     *
     * @param oldTime 之前的时间，格式：yyyy-MM-dd HH:mm:ss
     * @return String 为null时转换失败
     * 倒计时够天数显示天数，不够显示小时，不够小时显示分钟
     */
    public static String getDiffTimeLessNow(String oldTime) {
        try {
            Calendar c = Calendar.getInstance();
            c.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(oldTime));// 将字符串转换为可获取毫秒数的日期类型
            long nowDate = System.currentTimeMillis();
            long countdown = (nowDate - c.getTimeInMillis()) / (1000 * 60);// 获取倒计时（分钟）
            long days = countdown / (60 * 24);// 天
            long hours = countdown / 60 % 24;// 小时
            long mins = countdown % 60;// 分钟
            if (days > 0) {
                return days + "天";
            }
            if (hours > 0) {
                return hours + "小时";
            }
            if (mins > 0) {
                return mins + "分钟";
            }
            return "0分钟";
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 是否已经过时
     *
     * @param time 判断过时的时间对象，格式：yyyy-MM-dd HH:mm:ss
     * @return boolean 为null时判断失败
     * true表示改时间是当前时间节点之前的时间，false表示改时间是当前时间节点之后的时间
     */
    public static boolean isOldTime(String time) {
        try {
            Calendar c = Calendar.getInstance();
            c.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(time));// 将字符串转换为可获取毫秒数的日期类型
            long nowDate = System.currentTimeMillis();
            long dateDiff = (c.getTimeInMillis() - nowDate);// 获取时间差
            if (dateDiff < 0) {
                return true;
            } else {
                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }


    /**
     * @param startDate 2015-09-21
     * @param endDate   2015-11-13
     * @return List<String> String日期格式"yyyy-MM-dd"的集合
     * @Description: 获得两个日期之间的所有日期
     * @author ZXN
     * @date 2015-9-21 下午03:32:21
     */
    public static List<String> getDates(String startDate, String endDate) {
        List<String> result = new ArrayList<String>();
        if (startDate.equals(endDate)) {
            return result;
        }
        Calendar startDay = Calendar.getInstance();
        Calendar endDay = Calendar.getInstance();
        startDay.setTime(DateUtils.parseDate(startDate, 3));
        endDay.setTime(DateUtils.parseDate(endDate, 3));
        Calendar temp = (Calendar) startDay.clone();
        temp.add(Calendar.DAY_OF_YEAR, 1);
        result.add(startDate);
        while (temp.before(endDay)) {
            result.add(DateUtils.dateToString(temp.getTime()));
            temp.add(Calendar.DAY_OF_YEAR, 1);
        }
        result.add(endDate);
        return result;
    }

    /**
     * @param minDate 2015-01
     * @param maxDate 2016-07
     * @return List<String>
     * @throws ParseException
     * @Description: 获得两个月之间的所有月
     * @author ZXN
     * @date 2015-9-22 上午10:28:02
     */
    public static List<String> getMonthBetween(String minDate, String maxDate) throws ParseException {
        ArrayList<String> result = new ArrayList<String>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");// 格式化为年月

        Calendar min = Calendar.getInstance();
        Calendar max = Calendar.getInstance();

        min.setTime(sdf.parse(minDate));
        min.set(min.get(Calendar.YEAR), min.get(Calendar.MONTH), 1);

        max.setTime(sdf.parse(maxDate));
        max.set(max.get(Calendar.YEAR), max.get(Calendar.MONTH), 2);

        Calendar curr = min;
        while (curr.before(max)) {
            result.add(sdf.format(curr.getTime()));
            curr.add(Calendar.MONTH, 1);
        }

        return result;
    }

    /**
     * @param date 2015-02
     * @return
     * @Description: 获取某年某月最后一天
     * @author ZXN
     * @date 2015-9-22 下午02:10:25
     */
    public static String getLastDayOfMonth(String date) {
        int year = Integer.parseInt(date.split("-")[0]);
        int month = Integer.parseInt(date.split("-")[1]);
        Calendar cal = Calendar.getInstance();
        // 设置年份
        cal.set(Calendar.YEAR, year);
        // 设置月份
        cal.set(Calendar.MONTH, month - 1);
        // 获取某月最大天数
        int lastDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        // 设置日历中月份的最大天数
        cal.set(Calendar.DAY_OF_MONTH, lastDay);
        // 格式化日期
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String lastDayOfMonth = sdf.format(cal.getTime());

        return lastDayOfMonth;
    }

    /**
     * @param date 2015-02
     * @return
     * @Description: 获取某年某月第一天
     * @author ZXN
     * @date 2015-9-22 下午02:11:56
     */
    public static String getFirstDayOfMonth(String date) {
        int year = Integer.parseInt(date.split("-")[0]);
        int month = Integer.parseInt(date.split("-")[1]);
        Calendar cal = Calendar.getInstance();
        // 设置年份
        cal.set(Calendar.YEAR, year);
        // 设置月份
        cal.set(Calendar.MONTH, month - 1);
        // 获取某月最小天数
        int lastDay = cal.getActualMinimum(Calendar.DAY_OF_MONTH);
        // 设置日历中月份的最大天数
        cal.set(Calendar.DAY_OF_MONTH, lastDay);
        // 格式化日期
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String lastDayOfMonth = sdf.format(cal.getTime());
        return lastDayOfMonth;
    }

    /**
     * 得到年月日时分秒
     */
    public static String getNowtime() {
        Calendar calendar = new GregorianCalendar();
        return calendar.get(Calendar.YEAR) + "" + (calendar.get(Calendar.MONTH) + 1) + "" + calendar.get(Calendar.DAY_OF_MONTH) + "" + calendar.get(Calendar.HOUR_OF_DAY) + "" + calendar.get(Calendar.MINUTE) + "" + calendar.get(Calendar.SECOND) + "" + calendar.get(Calendar.MILLISECOND);
    }


    /**
     * 本周周一
     * gaocl
     *
     * @return yyyy-MM-dd
     */
    public static String getMondayOfThisWeek() {
        Calendar c = Calendar.getInstance();
        int day_of_week = c.get(Calendar.DAY_OF_WEEK) - 1;
        if (day_of_week == 0) {
            day_of_week = 7;
        }
        c.add(Calendar.DATE, -day_of_week + 1);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(c.getTime());
    }

    /**
     * 本周周日
     * gaocl
     *
     * @return yyyy-MM-dd
     */
    public static String getSundayOfThisWeek() {
        Calendar c = Calendar.getInstance();
        int day_of_week = c.get(Calendar.DAY_OF_WEEK) - 1;
        if (day_of_week == 0) {
            day_of_week = 7;
        }
        c.add(Calendar.DATE, -day_of_week + 7);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(c.getTime());
    }

    /**
     * 本周周日 + 1天
     * gaocl
     *
     * @return yyyy-MM-dd
     */
    public static String getSundayOfThisWeekAddDay() {
        Calendar c = Calendar.getInstance();
        int day_of_week = c.get(Calendar.DAY_OF_WEEK) - 1;
        if (day_of_week == 0) {
            day_of_week = 7;
        }
        c.add(Calendar.DATE, -day_of_week + 7 + 1);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(c.getTime());
    }
    /* added by zuohq at 2016.10.12 for 增加本月天数功能 begin */

    /**
     * 本月第一天
     * zuohq
     *
     * @return yyyy-MM-dd
     */
    public static String getMonthOfFirstDay() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, 0);
        c.set(Calendar.DAY_OF_MONTH, 1);//设置为1号,当前日期既为本月第一天
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(c.getTime());
    }
    /**
     * 上月第一天
     * zuohq
     *
     * @return yyyy-MM-dd
     */
    public static String getBackMonthOfFirstDay() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -1);
        c.set(Calendar.DAY_OF_MONTH, 1);//设置为1号,当前日期既为本月第一天
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(c.getTime());
    }
    /**
     * 上月最后一天
     * zuohq
     *
     * @return yyyy-MM-dd
     */
    public static String getBackMonthOfLastDay() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -1);
       c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(c.getTime());
    }
    /**
     * 上月天数
     * zuohq
     *
     * @return yyyy-MM-dd
     */
    public static Integer getBackMonthDays() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -1);
        return c.getActualMaximum(Calendar.DAY_OF_MONTH);
    }
    /**
     * 本月最后一天
     * zuohq
     *
     * @return yyyy-MM-dd
     */
    public static String getMonthOfLastDay() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(c.getTime());
    }

    /**
     * 本月最后一天+1
     * zuohq
     *
     * @return yyyy-MM-dd
     */
    public static String getMonthOfLastAddDay() {

        Calendar c = Calendar.getInstance();
        int day_of_month = c.get(Calendar.DAY_OF_MONTH) - 1;
        if (day_of_month == 0) {
            day_of_month = 30;
        }
        c.add(Calendar.DATE, -day_of_month + 30 + 1);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(c.getTime());
    }
    /* added by zuohq at 2016.10.12 for 增加本月天数功能 end */

    /**
     * 指定日期的星期
     * gaocl
     *
     * @param date
     * @return
     */
    public static String getWeek(Date date) {
        String[] weeks = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int weekIndex = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (weekIndex < 0) {
            weekIndex = 0;
        }
        return weeks[weekIndex];
    }

    /**
     * 获取指定日期当月的所有周
     * gaocl
     *
     * @param date
     * @return
     */
    public static List<String> getWeekOfMonth(Date date) {
        List<String> list = new ArrayList<String>();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, 0);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        Date firstDate = cal.getTime();
        int days = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        Date d = firstDate;
        for (int i = 0; i < days; i++) {
            cal.setTime(d);
            d = getAfterDayDate(d, 1);
            int weekIndex = cal.get(Calendar.DAY_OF_WEEK);
            if (weekIndex == 1 || i == days - 1) {
                list.add(dateToString(firstDate) + "|" + dateToString(cal.getTime()));
                firstDate = d;
            }
        }
        return list;
    }

    /**
     * 获取指定日期n天之后的日期
     * gaocl
     *
     * @param date
     * @param days
     * @return
     */
    public static Date getAfterDayDate(Date date, int days) {
        Calendar canlendar = Calendar.getInstance();
        canlendar.setTime(date);
        canlendar.add(Calendar.DATE, days);
        return canlendar.getTime();
    }

    public static String getBeforDayDateStr(Date date, int days) {
        Calendar canlendar = Calendar.getInstance();
        canlendar.setTime(date);
        canlendar.add(Calendar.DATE, -days);
        return new SimpleDateFormat(dateFormat[3]).format(canlendar.getTime());
    }


    /**
     * 获取指定日期当年的所有月
     * gaocl
     *
     * @param date
     * @return
     */
    public static List<String> getMonthOfYear(Date date) {
        List<String> list = new ArrayList<String>();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        for (int i = 0; i < 12; i++) {
            cal.set(Calendar.MONTH, i);
            cal.set(Calendar.DAY_OF_MONTH, 1);
            Date firstDate = cal.getTime();
            int days = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
            cal.set(Calendar.DAY_OF_MONTH, days);
            list.add(dateToString(firstDate) + "|" + dateToString(cal.getTime()));
        }
        return list;
    }

    /**
     * 获取指定日期当年的所有季
     * gaocl
     *
     * @param date
     * @return
     */
    public static List<String> getSeasonOfYear(Date date) {
        List<String> list = new ArrayList<String>();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        for (int i = 0; i < 12; i += 3) {
            cal.set(Calendar.MONTH, i);
            cal.set(Calendar.DAY_OF_MONTH, 1);
            Date firstDate = cal.getTime();
            cal.set(Calendar.MONTH, i + 2);
            int days = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
            cal.set(Calendar.DAY_OF_MONTH, days);
            list.add(dateToString(firstDate) + "|" + dateToString(cal.getTime()));
        }
        return list;
    }

    /**
     * 获取某年第一天日期
     * gaocl
     *
     * @param year 年份
     * @return Date
     */
    public static String getYearFirst(String year) {
        Calendar cal = Calendar.getInstance();
        cal.clear();
        cal.set(Calendar.YEAR, Integer.parseInt(year));
        return toDateStr(cal);
    }

    /**
     * 获取某年最后一天日期
     * gaocl
     *
     * @param year 年份
     * @return Date
     */
    public static String getYearLast(String year) {
        Calendar cal = Calendar.getInstance();
        cal.clear();
        cal.set(Calendar.YEAR, Integer.parseInt(year));
        cal.roll(Calendar.DAY_OF_YEAR, -1);
        return toDateStr(cal);
    }


    public static void main(String[] args) {
        System.out.println(getCountdownTime(getDate()+" 23:59:59"));
        // Calendar c1 = DateUtils.convUtilDateToUtilCalendar(DateUtils.parseDate("2014-05-10 10:52",15));
        // TimeZone tz = TimeZone.getTimeZone("GMT+08:00"); // 获得时区
        // Calendar cal = Calendar.getInstance();
        // cal.set(Calendar.SECOND, 0);
        // cal.setTimeZone(tz); // 设置时区
        // c1.setTimeZone(tz);
        // System.out.println(c1.getTimeInMillis());
        // System.out.println(cal.getTimeInMillis());
        // System.out.println(c1.getTime());
        // System.out.println(cal.getTime());
        // System.out.println(DateUtils.calendarTime(c1, cal));;
        // System.out.println(DateUtils.getCountdownTime("2015-01-25 18:22:00"));
        // System.out.println(DateUtils.getCountdownTime("2015-01-24 18:22:00"));
        // System.out.println(DateUtils.getCountdownTime("2015-01-24 18:15:00"));
        // System.out.println(DateUtils.isOldTime("2015-01-23 19:22:00"));
        // System.out.println(DateUtils.getDiffTimeLessNow("2015-01-23 19:15:00"));

//		Calendar c = Calendar.getInstance();
//		try {
//			c.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse("2015-01-23 19:22:00"));
//		} catch (ParseException e) {
//
//			e.printStackTrace();
//		}
        try {
            //获得当前周数
            Calendar c = Calendar.getInstance();
            int i = c.get(Calendar.WEEK_OF_YEAR);
            System.out.println(i);
            //获得当前月
            int month = c.get(Calendar.MONTH);
            System.out.println(month + 1);
            //年
            int da = c.get(Calendar.YEAR);
            System.out.println(da);
            //一年有多少周
            Calendar cal = Calendar.getInstance();
            cal.setFirstDayOfWeek(Calendar.MONDAY);
            cal.set(Calendar.YEAR, 2017);
            cal.set(Calendar.MONTH, 11);//11表示的是12月
            cal.set(Calendar.DATE, 30);
            int d = cal.get(Calendar.WEEK_OF_YEAR);
            System.out.println(d);

            System.out.println(getSeason(new Date()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* added by 董瑞贤 at 2016.10.20 begin */

    /**
     * 获取前一天
     * 董瑞贤
     *
     * @return "yyyy-MM-dd HH:mm:ss:SSS"
     */
    public static String getLastDayTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        return new SimpleDateFormat(dateFormat[0]).format(calendar.getTime());
    }
    /* added by 董瑞贤 at 2016.10.20 end */


    /**
     * 取得日期：年
     *
     * @param date
     * @return
     */
    public static int getYear(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int year = c.get(Calendar.YEAR);
        return year;
    }

    /**
     * 取得日期：年
     *
     * @param date
     * @return
     */
    public static int getMonth(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int month = c.get(Calendar.MONTH);
        return month + 1;
    }

    /**
     * 取得日期：年
     *
     * @param date
     * @return
     */
    public static int getDay(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int da = c.get(Calendar.DAY_OF_MONTH);
        return da;
    }

    /**
     * 获得一年有多少周
     *
     * @param date
     * @return
     */
    public static int getTotalMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        // int week_of_year = cal.get(Calendar.WEEK_OF_YEAR);
        cal.set(Calendar.MONTH, 11);//11表示的是12月
        cal.set(Calendar.DATE, 30);
        int mouth = cal.get(Calendar.MONTH);
        int d = cal.get(Calendar.WEEK_OF_YEAR);
        if (mouth >= 11) {
            d += 52;
        }
        return d;
    }

    /**
     * 取得当天日期是周几
     *
     * @param date
     * @return
     */
    public static int getWeekDay(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int week_of_year = c.get(Calendar.DAY_OF_WEEK);
        return week_of_year - 1;
    }

    /**
     * 取得一年的第几周
     *
     * @param date
     * @return
     */
    public static int getWeekOfYear(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.setFirstDayOfWeek(Calendar.MONDAY);
        int week_of_year = c.get(Calendar.WEEK_OF_YEAR);
        int mouth = c.get(Calendar.MONTH);
        if (mouth >= 11 && week_of_year <= 1) {
            week_of_year += 52;
        }
        return week_of_year;
    }

    /**
     * 获取当前是第几个季度
     *
     * @param date
     * @return
     */
    public static int getSeason(Date date) {

        int season = 0;

        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int month = c.get(Calendar.MONTH);
        switch (month) {
            case Calendar.JANUARY:
            case Calendar.FEBRUARY:
            case Calendar.MARCH:
                season = 1;
                break;
            case Calendar.APRIL:
            case Calendar.MAY:
            case Calendar.JUNE:
                season = 2;
                break;
            case Calendar.JULY:
            case Calendar.AUGUST:
            case Calendar.SEPTEMBER:
                season = 3;
                break;
            case Calendar.OCTOBER:
            case Calendar.NOVEMBER:
            case Calendar.DECEMBER:
                season = 4;
                break;
            default:
                break;
        }
        return season;
    }


    /**
     * 根据日期取得对应周周一日期
     *
     * @param date
     * @return
     */
    public static Date getMondayOfWeek(Date date) {
        Calendar monday = Calendar.getInstance();
        monday.setTime(date);
        monday.setFirstDayOfWeek(Calendar.MONDAY);
        monday.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        return monday.getTime();
    }

    /**
     * 根据日期取得对应周周日日期
     *
     * @param date
     * @return
     */
    public static Date getSundayOfWeek(Date date) {
        Calendar sunday = Calendar.getInstance();
        sunday.setTime(date);
        sunday.setFirstDayOfWeek(Calendar.MONDAY);
        sunday.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        return sunday.getTime();
    }

    /**
     * 取得月第一天
     *
     * @param date
     * @return
     */
    public static Date getFirstDateOfMonth(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));
        return c.getTime();
    }

    /**
     * 取得月最后一天
     *
     * @param date
     * @return
     */
    public static Date getLastDateOfMonth(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
        return c.getTime();
    }

    /**
     * 取得季度第一天
     *
     * @param date
     * @return
     */
    public static Date getFirstDateOfSeason(Date date) {
        return getFirstDateOfMonth(getSeasonDate(date)[0]);
    }

    /**
     * 取得季度最后一天
     *
     * @param date
     * @return
     */
    public static Date getLastDateOfSeason(Date date) {
        return getLastDateOfMonth(getSeasonDate(date)[2]);
    }

    /**
     * 取得季度天数
     *
     * @param date
     * @return
     */
    public static int getDayOfSeason(Date date) {
        int day = 0;
        Date[] seasonDates = getSeasonDate(date);
        for (Date date2 : seasonDates) {
            day += getDayOfMonth(date2);
        }
        return day;
    }

    /**
     * 取得季度月
     *
     * @param date
     * @return
     */
    public static Date[] getSeasonDate(Date date) {
        Date[] season = new Date[3];

        Calendar c = Calendar.getInstance();
        c.setTime(date);

        int nSeason = getSeason(date);
        if (nSeason == 1) {// 第一季度
            c.set(Calendar.MONTH, Calendar.JANUARY);
            season[0] = c.getTime();
            c.set(Calendar.MONTH, Calendar.FEBRUARY);
            season[1] = c.getTime();
            c.set(Calendar.MONTH, Calendar.MARCH);
            season[2] = c.getTime();
        } else if (nSeason == 2) {// 第二季度
            c.set(Calendar.MONTH, Calendar.APRIL);
            season[0] = c.getTime();
            c.set(Calendar.MONTH, Calendar.MAY);
            season[1] = c.getTime();
            c.set(Calendar.MONTH, Calendar.JUNE);
            season[2] = c.getTime();
        } else if (nSeason == 3) {// 第三季度
            c.set(Calendar.MONTH, Calendar.JULY);
            season[0] = c.getTime();
            c.set(Calendar.MONTH, Calendar.AUGUST);
            season[1] = c.getTime();
            c.set(Calendar.MONTH, Calendar.SEPTEMBER);
            season[2] = c.getTime();
        } else if (nSeason == 4) {// 第四季度
            c.set(Calendar.MONTH, Calendar.OCTOBER);
            season[0] = c.getTime();
            c.set(Calendar.MONTH, Calendar.NOVEMBER);
            season[1] = c.getTime();
            c.set(Calendar.MONTH, Calendar.DECEMBER);
            season[2] = c.getTime();
        }
        return season;
    }

    /**
     * 取得月天数
     *
     * @param date
     * @return
     */
    public static int getDayOfMonth(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.getActualMaximum(Calendar.DAY_OF_MONTH);
    }


    public static String getLastDayOfWeek(int year, int week) {
        Calendar cal = Calendar.getInstance();
        //设置年份
        cal.set(Calendar.YEAR, year);
        //设置周
        cal.set(Calendar.WEEK_OF_YEAR, week);
        //设置该周第一天为星期一
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        //设置最后一天是星期日
        cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek() + 6); // Sunday
        //格式化日期
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String lastDayOfWeek = sdf.format(cal.getTime());
        return lastDayOfWeek;
    }


}




