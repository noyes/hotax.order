package liqiang365.util.date;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DateUtil {
    private final static SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");

    private final static SimpleDateFormat sdfDay = new SimpleDateFormat(
            "yyyy-MM-dd");

    private final static SimpleDateFormat sdfDays = new SimpleDateFormat(
            "yyyyMMdd");

    private final static SimpleDateFormat sdfTime = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss");

    /**
     * 获取YYYY格式
     *
     * @return
     */
    public static String getYear() {
        return sdfYear.format(new Date());
    }

    /**
     * 获取YYYY-MM-DD格式
     *
     * @return
     */
    public static String getDay() {
        return sdfDay.format(new Date());
    }

    /**
     * 获取YYYYMMDD格式
     *
     * @return
     */
    public static String getDays() {
        return sdfDays.format(new Date());
    }

    /**
     * 获取YYYY-MM-DD HH:mm:ss格式
     *
     * @return
     */
    public static String getTime() {
        return sdfTime.format(new Date());
    }

    /**
     * @param s
     * @param e
     * @return boolean
     * @throws
     * @Title: compareDate
     * @Description: (日期比较，如果s>=e 返回true 否则返回false)
     * @author luguosui
     */
    public static boolean compareDate(String s, String e) {
        if (fomatDate(s) == null || fomatDate(e) == null) {
            return false;
        }
        return fomatDate(s).getTime() >= fomatDate(e).getTime();
    }

    public static boolean compareDateTime(String s, String e) {
        if (fomatDateTime(s) == null || fomatDateTime(e) == null) {
            return false;
        }
        return fomatDateTime(s).getTime() >= fomatDateTime(e).getTime();
    }
    /**
     * 格式化日期
     *
     * @return
     */
    public static Date fomatDate(String date) {
        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return fmt.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Date fomatDateTime(String date) {
        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return fmt.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    //获取秒
    public static long getSeconds(String date) {
        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return fmt.parse(date).getTime()/1000;
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }
    /**
     * 校验日期是否合法
     *
     * @return
     */
    public static boolean isValidDate(String s) {
        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        try {
            fmt.parse(s);
            return true;
        } catch (Exception e) {
            // 如果throw java.text.ParseException或者NullPointerException，就说明格式不对
            return false;
        }
    }

    public static int getDiffYear(String startTime, String endTime) {
        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");

        try {
            int years = (int) (((fmt.parse(endTime).getTime() - fmt.parse(startTime).getTime()) / (1000 * 60 * 60 * 24)) / 365);
            return years;
        } catch (Exception e) {
            // 如果throw java.text.ParseException或者NullPointerException，就说明格式不对
            return 0;
        }
    }

    /**
     * <li>功能描述：时间相减得到天数
     *
     * @param beginDateStr
     * @param endDateStr
     * @return long
     * @author Administrator
     */
    public static long getDaySub(String beginDateStr, String endDateStr) {
        long day = 0;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date beginDate = null;
        Date endDate = null;

        try {
            beginDate = format.parse(beginDateStr);
            endDate = format.parse(endDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        day = (endDate.getTime() - beginDate.getTime()) / (24 * 60 * 60 * 1000);
        //System.out.println("相隔的天数="+day);

        return day;
    }

    /**
     * 得到n天之后的日期
     *
     * @param days
     * @return
     */
    public static String getAfterDayDate(String days) {
        int daysInt = Integer.parseInt(days);

        Calendar canlendar = Calendar.getInstance(); // java.util包
        canlendar.add(Calendar.DATE, daysInt); // 日期减 如果不够减会将月变动
        Date date = canlendar.getTime();

        SimpleDateFormat sdfd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateStr = sdfd.format(date);

        return dateStr;
    }

    /**
     * 得到n天之后是周几
     *
     * @param days
     * @return
     */
    public static String getAfterDayWeek(String days) {
        int daysInt = Integer.parseInt(days);

        Calendar canlendar = Calendar.getInstance(); // java.util包
        canlendar.add(Calendar.DATE, daysInt); // 日期减 如果不够减会将月变动
        Date date = canlendar.getTime();

        SimpleDateFormat sdf = new SimpleDateFormat("E");
        String dateStr = sdf.format(date);

        return dateStr;
    }

    public static void main(String[] args) throws Exception{
        List<String> a=new ArrayList<>();
        a.add(null);
        a.add(null);
        Map<String,String> map = new HashMap();
        a.add(map.get("q"));
        System.out.println(a.toString());
//       long i= DateUtil.getDaySub("2019-06-26 12:17:37", DateUtil.getTime());
//        System.out.println(i);
        /*System.out.println(getNowtime());
        System.out.println(getAfterDayWeek("3"));
        String str = "admin,test";
        String [] strArray = str.split(",");
        System.out.println(strArray);*/
        /*Map<String,Object> map = new HashMap<>();
        Map<String,Object> result = new LinkedHashMap<>();
        map.put("A",10);
        map.put("B",9);
        map.put("C",8);
        map.put("D",7);
        map.put("E",6);
        System.out.println("original map :"+map);
        Stream<Map.Entry<String, Object>> st = map.entrySet().stream();
        st.sorted(Comparator.comparing(e -> (Integer)e.getValue())).forEach(e -> result.put(e.getKey(), e.getValue()));
        System.out.println("Sorted Map :"+ result);*/
//        System.out.println(DateUtil.fomatDateTime("2017-11-07 18:00:00").getTime());
//        System.out.println(DateUtil.fomatDateTime("2017-11-07 19:00:00").getTime());
//        System.out.println(DateUtil.compareDateTime("2017-11-07 18:00:00","2017-11-07 20:00:00"));
//        System.out.println(DateUtil.getAddYearTime());
//        System.out.println(Double.MAX_VALUE);
//        System.out.println(System.currentTimeMillis());
//        System.out.println(System.currentTimeMillis()/1000);

//        System.out.println(getSeconds("2019-1-12 00:20:21"));
//        System.out.println(getSeconds("2019-1-12 00:20:22"));
//        System.out.println(getSeconds("2019-3-12 00:20:23"));
//        System.out.println(getSeconds("2019-3-12 00:20:23"));
//        System.out.println(getSeconds("2019-3-12 00:20:23"));
//        System.out.println(getSeconds("2019-3-12 00:20:23"));
//        System.out.println(getSeconds("2019-3-12 00:20:23"));
//        System.out.println(getSeconds("2019-3-12 00:20:23"));
//        System.out.println(getSeconds("2019-3-12 00:20:23"));
//        System.out.println(getSeconds("2019-3-12 00:20:23"));
//        Double d=1600000000-1552321223.0;
//        for (int i = 0; i <10000 ; i++) {
//            System.out.println((i+d)-(1600000000-1552321223.0));
//        }


//       String b= DateUtil.getAfterDayDate("-1");
//        System.out.println(b);
//        System.out.println(fomatDate(b).getTime() == fomatDate("2019-05-21 11:44:48").getTime());

//        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
//        System.out.println(fmt.format(fmt.parse("2019-05-21 11:44:48")));
    }
    /**
     * 得到年月日时分秒
     */
    public static String getNowtime() {
        Calendar calendar = new GregorianCalendar();
        return calendar.get(Calendar.YEAR) + "" + (calendar.get(Calendar.MONTH) + 1) + "" + calendar.get(Calendar.DAY_OF_MONTH) + "" + calendar.get(Calendar.HOUR_OF_DAY) + "" + calendar.get(Calendar.MINUTE) + "" + calendar.get(Calendar.SECOND) + "" + calendar.get(Calendar.MILLISECOND);
    }

    /**
     * 获得时区为CST的时间
     * @return
     */
    public static Date getCSTNowDate(){
        Date tdate = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("CST"));
        DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat1.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        try {
            return dateFormat.parse(dateFormat1.format(tdate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

    /**
     * 当前时间加一年
     * @return
     */
    public static String getAddYearTime(){
        Date date = new Date();
        Calendar  calendar   = new  GregorianCalendar();
        calendar.add(calendar.YEAR, 1);
        date=calendar.getTime();
        return sdfTime.format(date);
    }

    /**
     * 当前时间加n年
     * @return
     */
    public static String getAddYearTime(int year){
        Date date = new Date();
        Calendar  calendar   = new  GregorianCalendar();
        calendar.add(calendar.YEAR, year);
        date=calendar.getTime();
        return sdfTime.format(date);
    }
}
