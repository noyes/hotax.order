package liqiang365.util.date;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MonthUtil {

    public static Integer getMonth(Date date) {
        if(date != null) {
            DateFormat df = new SimpleDateFormat("yyyyMM");
            String d = df.format(date);
            return Integer.valueOf(d);
        }
        return null;
    }

    public static final String[] getDateRange(Date d) {
        String[] range = new String[2];
        Calendar calendar = Calendar.getInstance();
        if(d != null) {
            calendar.setTime(d);
        }
        int minimum = calendar.getActualMinimum(Calendar.DAY_OF_MONTH);
        calendar.set(Calendar.DAY_OF_MONTH, minimum);
        range[0] = DateUtils.dateToString(calendar.getTime(), 3);

        int maximum = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        calendar.set(Calendar.DAY_OF_MONTH, maximum);
        String end = DateUtils.dateToString(calendar.getTime(), 3);
        range[1] = end;
        return range;
    }

    public static Integer getLastMonth(Date time){
        Calendar calendar= Calendar.getInstance();
        calendar.add(Calendar.MONTH, -1);
        return getMonth(calendar.getTime());
    }

    public static void main(String[] args) {
        Date date = new Date();
        System.out.println(getMonth(date));
//
//        System.out.println(JSON.toJSONString(getDateRange(date)));

    }
}
