package liqiang365.util.file;


import com.liqiang365.util.valid.ValidUtil;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class PoiUtil {
    // 默认单元格内容为数字时格式
    private static DecimalFormat df = new DecimalFormat("0");
    // 默认单元格格式化日期字符串
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    // 格式化数字
    private static DecimalFormat nf = new DecimalFormat("0.00");

    public static ArrayList<ArrayList<Object>> readExcel(String filePath, int index, int count) {
        File file = new File(filePath);
        if (file == null) {
            return null;
        }
        if (file.getName().endsWith("xlsx")) {
            // 处理ecxel2007
            return readExcel2007(file, index, count);
        } else {
            // 处理ecxel2003
            return readExcel2003(file, index, count);
        }
    }

    /*
     * @return 将返回结果存储在ArrayList内，存储结构与二位数组类似
     * lists.get(0).get(0)表示过去Excel中0行0列单元格
     */
    public static ArrayList<ArrayList<Object>> readExcel2003(File file, int index, int count) {
        try {
            ArrayList<ArrayList<Object>> rowList = new ArrayList<ArrayList<Object>>();
            ArrayList<Object> colList;
            HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(file));
            HSSFSheet sheet = wb.getSheetAt(0);
            HSSFRow row;
            HSSFCell cell;
            Object value;
            for (int i = sheet.getFirstRowNum(), rowCount = 0; rowCount < sheet.getPhysicalNumberOfRows(); i++) {
                row = sheet.getRow(i);
                colList = new ArrayList<Object>();
                if (row == null) {
                    // 当读取行为空时
                    if (i != sheet.getPhysicalNumberOfRows()) {// 判断是否是最后一行
                        rowList.add(colList);
                    }
                    continue;
                } else {
                    rowCount++;
                }
                for (int j = row.getFirstCellNum(); j < count; j++) {
                    if (i == 0) {
                        continue;
                    }
                    cell = row.getCell(j);
                    if (cell == null || cell.getCellType() == HSSFCell.CELL_TYPE_BLANK) {
                        // 当该单元格为空
                        if (j != row.getLastCellNum()) {// 判断是否是该行中最后一个单元格
                            colList.add("");
                        }
                        continue;
                    }
                    switch (cell.getCellType()) {
                        case XSSFCell.CELL_TYPE_STRING:
                            // System.out.println(i + "行" + j + " 列 is String
                            // type");
                            value = cell.getStringCellValue();
                            break;
                        case XSSFCell.CELL_TYPE_NUMERIC:
                            if ("@".equals(cell.getCellStyle().getDataFormatString())) {
                                value = df.format(cell.getNumericCellValue());
                            } else if ("General".equals(cell.getCellStyle().getDataFormatString())) {
                                value = nf.format(cell.getNumericCellValue());
                            } else {
                                value = sdf.format(HSSFDateUtil.getJavaDate(cell.getNumericCellValue()));
                            }
                            // System.out.println(i + "行" + j + " 列 is Number type ;
                            // DateFormt:" + value.toString());
                            break;
                        case XSSFCell.CELL_TYPE_BOOLEAN:
                            // System.out.println(i + "行" + j + " 列 is Boolean
                            // type");
                            value = Boolean.valueOf(cell.getBooleanCellValue());
                            break;
                        case XSSFCell.CELL_TYPE_BLANK:
                            // System.out.println(i + "行" + j + " 列 is Blank type");
                            value = "";
                            break;
                        default:
                            // System.out.println(i + "行" + j + " 列 is default
                            // type");
                            value = cell.toString();
                    }// end switch
                    if (ValidUtil.isNotEmpty(value)) {
                        colList.add(value);
                    }
                } // end for j
                if (ValidUtil.isNotEmpty(colList)) {
                    rowList.add(colList);
                }
            } // end for i

            return rowList;
        } catch (Exception e) {
            return null;
        }
    }

    public static ArrayList<ArrayList<Object>> readExcel2007(File file, int index, int count) {
        try {
            ArrayList<ArrayList<Object>> rowList = new ArrayList<ArrayList<Object>>();
            ArrayList<Object> colList;
            XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(file));
            XSSFSheet sheet = wb.getSheetAt(index);
            XSSFRow row;
            XSSFCell cell;
            Object value;
            for (int i = sheet.getFirstRowNum(), rowCount = 0; rowCount < sheet.getPhysicalNumberOfRows(); i++) {
                row = sheet.getRow(i);
                colList = new ArrayList<Object>();
                if (row == null) {
                    // 当读取行为空时
                    if (i != sheet.getPhysicalNumberOfRows()) {// 判断是否是最后一行
                        rowList.add(colList);
                    }
                    continue;
                } else {
                    rowCount++;
                }
                for (int j = row.getFirstCellNum(); j < count; j++) {
                    if (i == 0) {
                        continue;
                    }
                    cell = row.getCell(j);
                    if (cell == null || cell.getCellType() == HSSFCell.CELL_TYPE_BLANK) {
                        // 当该单元格为空
                        if (j != row.getLastCellNum()) {// 判断是否是该行中最后一个单元格
                            colList.add("");
                        }
                        continue;
                    }
                    value = getCellFormatValue(cell);
                    if (ValidUtil.isNotEmpty(value)) {
                        colList.add(value);
                    }
                } // end for j
                if (ValidUtil.isNotEmpty(colList)) {
                    rowList.add(colList);
                }
            } // end for i

            return rowList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 根据Cell类型设置数据
     *
     * @param cell
     * @return
     * @author yancong
     */
    private static Object getCellFormatValue(Cell cell) {
        Object cellvalue = "";
        if (cell != null) {
            // 判断当前Cell的Type
            switch (cell.getCellType()) {
                case Cell.CELL_TYPE_NUMERIC:// 如果当前Cell的Type为NUMERIC
                {
                    if ("@".equals(cell.getCellStyle().getDataFormatString())) {
                        cellvalue = df.format(cell.getNumericCellValue());
                        break;
                    } else if ("General".equals(cell.getCellStyle().getDataFormatString())) {
                        cellvalue = nf.format(cell.getNumericCellValue());
                        break;
                    } else {
                        cellvalue = sdf.format(HSSFDateUtil.getJavaDate(cell.getNumericCellValue()));
                        break;
                    }
                }
                case Cell.CELL_TYPE_FORMULA: {
                    // 判断当前的cell是否为Date
                    if (DateUtil.isCellDateFormatted(cell)) {
                        // 如果是Date类型则，转化为Data格式
                        // data格式是带时分秒的：2013-7-10 0:00:00
                        // cellvalue = cell.getDateCellValue().toLocaleString();
                        // data格式是不带带时分秒的：2013-7-10
                        Date date = cell.getDateCellValue();
                        cellvalue = date;
                        break;
                    } else {// 如果是纯数字

                        // 取得当前Cell的数值
                        cellvalue = String.valueOf(cell.getNumericCellValue());
                        break;
                    }
                }
                case Cell.CELL_TYPE_STRING:// 如果当前Cell的Type为STRING
                    // 取得当前的Cell字符串
                    cellvalue = cell.getRichStringCellValue().getString();
                    break;
                default:// 默认的Cell值
                    cellvalue = "";
            }
        } else {
            cellvalue = "";
        }
        return cellvalue;
    }

    public static void main(String[] args) {
        ArrayList<ArrayList<Object>> result = PoiUtil.readExcel("C:/Users/SuMmY/Desktop/李强365最新课程体系.xlsx", 0, 3);
        for (int i = 0; i < result.size(); i++) {
            for (int j = 0; j < result.get(i).size(); j++) {
                System.out.println((i + 1) + "行 " + (j + 1) + "列  " + result.get(i).get(j).toString());
            }
        }
    }

}
