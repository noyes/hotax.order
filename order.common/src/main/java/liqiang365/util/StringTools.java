package liqiang365.util;

import com.liqiang365.util.valid.ValidUtil;

/**
 * @author: hxy
 * @date: 2017/10/24 10:16
 */
public class StringTools {

    public static boolean isNullOrEmpty(String str) {
        return null == str || "".equals(str) || "null".equals(str);
    }

    public static boolean isNullOrEmpty(Object obj) {
        return null == obj || "".equals(obj);
    }

    /**
     * 万
     */
    public static int TEN_THOUSAND = 10000;
    public static int THOUSAND = 1000;

    /**
     * 模糊手机号，将手机号中间四位设置为 ****
     * 非手机号 原值返回
     *
     * @param phone
     * @return
     */
    public static String pollutePhone(String phone) {
        if (ValidUtil.isNotEmpty(phone)) {
            boolean mobile = ValidUtil.isMobile(phone);
            if (mobile) {
                return phone.substring(0, 3) + "****" + phone.substring(7, phone.length());
            }
        }
        return phone;
    }


    /**
     * 输入用户名和用户账号 获取用户显示的用户名
     *
     * @param username 用户名（昵称）
     * @param account  用户账号
     * @return username 不为空 返回username; 否则返回 处理后的account;
     */
    public static String getDisplayUsername(String username, String account) {
        return ValidUtil.isNotEmpty(username) ? username : pollutePhone(account);
    }

    /**
     * 格式化数字
     * 〈〉
     *
     * @param number 数字
     * @Author: susaifei
     * @Date: 2019/6/19 16:06
     */
    public static String format(int number) {
        if (number < TEN_THOUSAND) {
            return number + "";
        }

        int ii = number / TEN_THOUSAND;
        int i = (int) ((number % TEN_THOUSAND) / THOUSAND);
        if (i <= 0) {
            return (ii) + "万";
        } else {
            return Double.valueOf(ii + "." + i) + "万";
        }
    }

}
