package liqiang365.util.auth;

import com.liqiang365.model.entity.MyInfo;
import com.liqiang365.util.valid.ValidUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import java.util.Map;


/**
 * 用户工具类
 */
public class UserUtil {


    /**
     * 获取授权主要对象
     */
    public static Subject getSubject() {
        return SecurityUtils.getSubject();
    }

    /**
     * 获取当前登录者对象
     */
    public static MyInfo getInfo() {
        try {
            Subject subject = SecurityUtils.getSubject();
            String jwt = (String) subject.getPrincipal();
            if (JwtUtil.verifyToken(jwt)) {
                Map<String, Object> userMap = JwtUtil.parseToken(jwt);
                if (ValidUtil.isNotEmpty(userMap)) {
                    MyInfo myInfo = new MyInfo();
                    myInfo.setUserId(userMap.get("userId") + "");
                    myInfo.setUsername(userMap.get("username") + "");
                    myInfo.setNickName(userMap.get("nickName") + "");
                    myInfo.setHeadimgurl(userMap.get("headimgurl") + "");
                    return myInfo;
                }
            }
        }catch (Exception e){
            return null;
        }
        return null;
    }


}
