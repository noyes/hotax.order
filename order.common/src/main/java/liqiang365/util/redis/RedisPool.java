package liqiang365.util.redis;

import hotax.core.util.ValidUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Protocol;

/**
 * Description: Redis Pool
 * Author:     gaocl
 * Date:       2016/12/26
 * Version:    V1.0.0
 * Update:     更新说明
 */
@Component
public class RedisPool  {
    private static JedisPool jedisPool = null;


    /**
     * 获取Jedis实例
     *
     * @return
     */
    public synchronized static Jedis getJedis() {
        try {
            if (jedisPool != null) {
                return jedisPool.getResource();
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Bean
    public  JedisPoolConfig  RedisPool() {
        if (jedisPool == null) {
            System.out.println("=================================>>>REDIS INIT BEGIN:" + RedisConfig.REDIS_HOST_IP);
            JedisPoolConfig config = new JedisPoolConfig();
            //如果赋值为-1，则表示不限制；
            config.setMaxTotal(RedisConfig.REDIS_MAX_TOTAL);
            //控制一个pool最多有多少个状态为idle空闲的的jedis实例
            config.setMaxIdle(RedisConfig.REDIS_MAX_IDLE);
            config.setMaxWaitMillis(RedisConfig.REDIS_MAX_WAIT_MILLS);
            config.setTestOnBorrow(RedisConfig.REDIS_TEST_ON_BORROW);
            if (ValidUtil.isEmpty(RedisConfig.REDIS_PASSWORD)) {
                jedisPool = new JedisPool(config, RedisConfig.REDIS_HOST_IP, RedisConfig.REDIS_HOST_PORT);
            } else {
                jedisPool = new JedisPool(config, RedisConfig.REDIS_HOST_IP, RedisConfig.REDIS_HOST_PORT,
                        Protocol.DEFAULT_TIMEOUT, RedisConfig.REDIS_PASSWORD);
            }
            System.out.println("=================================>>>REDIS INIT END");
            return config;
        }
        return null;
    }
}
