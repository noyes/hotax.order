package liqiang365.util.redis;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Description: Redis Config
 * Author:     gaocl
 * Date:       2016/12/26
 * Version:    V1.0.0
 * Update:     更新说明
 */
@Configuration
@PropertySource(value = {"file:config/hotax-${spring.profiles.active}.properties", "config/hotax-${spring.profiles.active}.properties", "file:config/hotax.properties", "config/hotax.properties" }, ignoreResourceNotFound = true, encoding = "UTF-8")
public class RedisConfig {


    public static String REDIS_HOST_IP;
    public static int REDIS_HOST_PORT;
    public static String REDIS_PASSWORD;
    public static int REDIS_MAX_TOTAL;
    public static int REDIS_MAX_IDLE;
    public static int REDIS_MAX_WAIT_MILLS;
    public static boolean REDIS_TEST_ON_BORROW;

    @Value("${redis.config.host.ip}")
    public  void setRedisHostIp(String redisHostIp) {
        REDIS_HOST_IP = redisHostIp;
    }
    @Value("${redis.config.host.port}")
    public  void setRedisHostPort(int redisHostPort) {
        REDIS_HOST_PORT = redisHostPort;
    }
    @Value("${redis.config.password}")
    public  void setRedisPassword(String redisPassword) {
        REDIS_PASSWORD = redisPassword;
    }
    @Value("${redis.config.maxtotal}")
    public  void setRedisMaxTotal(int redisMaxTotal) {
        REDIS_MAX_TOTAL = redisMaxTotal;
    }
    @Value("${redis.config.maxidle}")
    public  void setRedisMaxIdle(int redisMaxIdle) {
        REDIS_MAX_IDLE = redisMaxIdle;
    }
    @Value("${redis.config.maxwaitmills}")
    public  void setRedisMaxWaitMills(int redisMaxWaitMills) {
        REDIS_MAX_WAIT_MILLS = redisMaxWaitMills;
    }
    @Value("${redis.config.testonborrow}")
    public  void setRedisTestOnBorrow(boolean redisTestOnBorrow) {
        REDIS_TEST_ON_BORROW = redisTestOnBorrow;
    }
}
