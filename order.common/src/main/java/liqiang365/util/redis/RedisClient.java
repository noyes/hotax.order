package liqiang365.util.redis;

import com.liqiang365.util.http.HttpClient;
import hotax.core.util.ValidUtil;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.SortingParams;
import redis.clients.jedis.Tuple;

import java.util.*;

/**
 * Description: RedisClient
 * Author:     gaocl
 * Date:       2016/12/26
 * Version:    V1.0.0
 * Update:     更新说明
 */
public class RedisClient {


    //设置锁的lua脚本
    private static final String SETNX_EXPIRE_SCRIPT =
            "if redis.call('setnx', KEYS[1], KEYS[2]) == 1 then\n"
                    + "return redis.call('expire', KEYS[1], KEYS[3]);\n"
                    + "end\n"
                    + "return nil;";

    private static final String DEL_IFEXIST_SCRIPT =
            "if redis.call('get',KEYS[1]) == KEYS[2] then\n"
                    + "return redis.call('del',KEYS[1])\n"
                    + "else\n"
                    + "return 0;\n"
                    + "end";

    private static final String SET_IFEXIST_SCRIPT =
            "if redis.call('get',KEYS[1]) == KEYS[2] then\n"
                    + "return redis.call('SET', KEYS[1], KEYS[3])\n"
                    + "end\n"
                    + "return nil;";

    /**
     * 下完单，把扣减的库存放到缓存中
     *
     * @param key
     * @param amount
     * @return
     */
    public static String reduceStock(String key, int amount) {
        try {
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("key", key));
            params.add(new BasicNameValuePair("amount", amount + ""));
            return HttpClient.post("http://brsy.liqiang365.app/orderStock/reduce", params);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 去缓存中回滚库存
     *
     * @param key
     * @param amount
     * @return
     */
    public static String plusStock(String key, int amount) {
        try {
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("key", key));
            params.add(new BasicNameValuePair("amount", amount + ""));
            return HttpClient.post("http://brsy.liqiang365.app/orderStock/reduce", params);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * 下单减库存,用来做闪购
     *
     * @param key
     * @return
     */
    public static String OrderCountReduce(String key, int amount) {
        try {
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("key", key));
            params.add(new BasicNameValuePair("amount", amount + ""));
            return HttpClient.post("http://brsy.liqiang365.app/ordercount/reduce", params);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 回滚库存，用来做闪购
     *
     * @param key
     * @param amount
     * @return
     */
    public static String orderCountPlus(String key, int amount) {
        try {
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("key", key));
            params.add(new BasicNameValuePair("amount", amount + ""));
            return HttpClient.post("http://brsy.liqiang365.app/ordercount/plus", params);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取redis value (String)
     *
     * @param key
     * @return
     */
    public static String get(String key) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.get(key) : null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * 通过key删除
     *
     * @param key
     */
    public static void del(String key) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null && ValidUtil.isNotEmpty(key)) {
                jedis.del(key);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * 添加key value
     *
     * @param key
     * @param value
     */
    public static void set(String key, String value) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (ValidUtil.isNotEmpty(value)) {
                if (jedis != null) {
                    jedis.set(key, value);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * 添加key value 并且设置存活时间
     *
     * @param key
     * @param value
     * @param liveTime
     */
    public static void set(String key, String value, int liveTime) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (ValidUtil.isNotEmpty(value)) {
                if (jedis != null) {
                    jedis.set(key, value);
                }
                jedis.expire(key, liveTime);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * 检查key是否已经存在
     *
     * @param key
     * @return
     */
    public static boolean exists(String key) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.exists(key) : null;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }


    /**
     * 设置过期时间
     *
     * @param key
     * @param liveTime 秒
     */
    public static void expire(String key, int liveTime) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null) {
                jedis.expire(key, liveTime);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * 指定具体的过期时间点
     * 例：2020年02月02日02点02分02秒
     *
     * @param key
     * @param timestamp
     */
    public static void expireAt(String key, long timestamp) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null) {
                jedis.expireAt(key, timestamp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * 控制 同一个key 一秒钟只能保存一次
     *
     * @param key
     * @param value
     * @param liveTime
     * @return
     */
    public static boolean setNxt(String key, String value, int liveTime) {
        boolean flag = true;
        Jedis jedis = RedisPool.getJedis();
        try {
            if (ValidUtil.isNotEmpty(value)) {
                if ((jedis != null ? jedis.setnx(key, value).intValue() : 0) == 1) {
                    jedis.expire(key, liveTime);
                    flag = true;
                } else {
                    flag = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            flag = true;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return flag;
    }


    /**
     * 增量+1
     *
     * @param key
     * @return
     */
    public static int incrby(String key) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.incrBy(key, 1).intValue() : 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * 增量+1
     *
     * @param key
     * @return
     */
    public static int incrbyCount(String key, Integer integer) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.incrBy(key, integer).intValue() : 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * 通过正则匹配keys
     *
     * @param pattern
     * @return
     */
    public static Set<String> pattern(String pattern) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.keys(pattern) : null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * 追加字符
     *
     * @param key
     * @param value
     */
    public static void append(String key, String value) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null) {
                jedis.append(key, value);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * 排序(适用set,sortset,list这3种数据类型)
     *
     * @param key
     * @param sortingParams
     */
    public static List<String> sort(String key, SortingParams sortingParams) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.sort(key, sortingParams) : null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }


    /**
     * Map:
     * 添加map
     *
     * @param key
     * @param map
     */
    public static void mapSets(String key, Map<String, String> map) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null) {
                jedis.hmset(key, map);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Map:
     * 添加
     *
     * @param key
     * @param mapKey
     * @param mapValue
     */
    public static void mapSet(String key, String mapKey, String mapValue) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null) {
                jedis.hset(key, mapKey, mapValue);
            }
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Map:
     * 删除指定map集合内的数据
     *
     * @param key
     * @param mapKeys
     */
    public static void mapDels(String key, String... mapKeys) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null) {
                jedis.hdel(key, mapKeys);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Map:
     * 遍历集合
     *
     * @param key
     * @return
     */
    public static List<String> mapGets(String key, String... mapKeys) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.hmget(key, mapKeys) : null;
        } catch (Exception e) {

            e.printStackTrace();
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Map:
     * 获取某个map的value
     *
     * @param key
     * @param mapKey
     * @return
     */
    public static String mapGet(String key, String mapKey) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.hget(key, mapKey) : null;
        } catch (Exception e) {

            e.printStackTrace();
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Map:
     * 遍历key下所有的hash数据
     *
     * @param key
     * @return
     */
    public static List<String> mapGetValueAll(String key) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.hvals(key) : null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Map:
     * 遍历key下所有的hash数据(包含map的key与value值)
     *
     * @param key
     * @return
     */
    public static Map<String, String> mapGetAll(String key) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.hgetAll(key) : null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Map:
     * 遍历key下所有的map key 值
     *
     * @param key
     * @return
     */
    public static Set<String> mapGetKeys(String key) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.hkeys(key) : null;
        } catch (Exception e) {

            e.printStackTrace();
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Map:
     * 获取集合大小
     *
     * @param key
     * @return
     */
    public static Long mapSize(String key) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.hlen(key) : null;
        } catch (Exception e) {

            e.printStackTrace();
            return 0L;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Map:
     * 增量 increment
     *
     * @param key
     * @param mapKey
     * @param increment
     * @return
     */
    public static Long mapIncrby(String key, String mapKey, int increment) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.hincrBy(key, mapKey, increment) : null;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return 0L;
    }

    /**
     * List:
     * list入对列
     *
     * @param key
     * @param value
     */
    public static void lstPush(String key, String value) {
        // 本地缓存后入队列
        Jedis jedis = RedisPool.getJedis();
        try {
            if (ValidUtil.isNotEmpty(value)) {
                if (jedis != null) {
                    jedis.rpush(key, value);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * List:
     * list入对列
     *
     * @param key
     * @param value
     */
    public static void lstLPush(String key, String value) {
        // 本地缓存后入队列
        Jedis jedis = RedisPool.getJedis();
        try {
            if (ValidUtil.isNotEmpty(value)) {
                if (jedis != null) {
                    jedis.lpush(key, value);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * List:
     * 入对列
     *
     * @param key
     * @param value 一次多条
     */
    public static void lstPushs(String key, String... value) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null) {
                jedis.rpush(key, value);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * List:
     * 出对列
     *
     * @param key
     */
    public static String lstPop(String key) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.lpop(key) : null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * List:
     * 移除指定key下size个value元素
     * size=2为从顶部移除值为value的2个元素
     * size=-2为从底部移除值为value的2个元素
     *
     * @param key
     * @param size
     * @param value
     */
    public static void lstRemove(String key, long size, String value) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null) {
                jedis.lrem(key, size, value);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }


    /**
     * List:
     * 获取list列表
     *
     * @param key
     * @param start 0
     * @param end   -1 可查全部
     * @return
     */
    public static List<String> lstAll(String key, long start, long end) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.lrange(key, start, end) : null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }


    /**
     * List:
     * list size
     *
     * @param key
     * @return
     */
    public static Long lstSize(String key) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.llen(key) : null;
        } catch (Exception e) {
            e.printStackTrace();
            return 0L;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * List:
     * 返回列表 key 中，下标为 index 的元素
     *
     * @param key
     * @param index
     * @return
     */
    public static String lstIndex(String key, int index) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.lindex(key, index) : null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * List:
     * 裁剪指定范围的list,也就是只保留这个范围内的数据
     *
     * @param key
     * @param start
     * @param end
     */
    public static void lstTrim(String key, long start, long end) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null) {
                jedis.ltrim(key, start, end);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Set:
     * 添加元素
     *
     * @param key
     * @param values 可多个value
     */
    public static void setAdds(String key, String... values) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null) {
                jedis.sadd(key, values);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Set:
     * 遍历集合
     *
     * @param key
     * @return
     */
    public static Set<String> setGet(String key) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.smembers(key) : null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }


    /**
     * Set:
     * 删除集合里的元素
     *
     * @param key
     * @param value
     */
    public static void setRemove(String key, String value) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null) {
                jedis.srem(key, value);
            }
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Set:
     * 集合大小
     *
     * @param key
     * @return
     */
    public static Long setSize(String key) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.scard(key) : null;
        } catch (Exception e) {

            e.printStackTrace();
            return 0L;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * 批量移除set
     *
     * @param key
     * @param count
     * @return
     */
    public static Set<String> spop(String key, Integer count) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.spop(key, count) : null;
        } catch (Exception e) {

            e.printStackTrace();
            return new HashSet<>();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * Set:
     * 判断集合中某元素是否存在
     *
     * @param key
     * @param value
     * @return
     */
    public static Boolean setExist(String key, String value) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.sismember(key, value) : null;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * 发布消息
     *
     * @param channel
     * @param message
     * @return
     */
    public static Long pub(String channel, String message) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.publish(channel, message) : null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * @param key
     * @return
     */
    public static Long getKeyLiveTime(String key) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.ttl(key) : null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * setnx带过期时间原子操作
     *
     * @param key     键名
     * @param value   键值
     * @param seconds 单位秒
     * @return 成功返回true, 失败false
     */
    public static boolean setNXEXByScript(String key, String value, int seconds) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? (jedis.eval(SETNX_EXPIRE_SCRIPT, 3, key, value, seconds + "") != null) : false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * 仅当存在同名key并且value也一致时删除key
     *
     * @param key   键名
     * @param value 键值
     * @return 成功返回true, 失败false
     */
    public static boolean delIfExiseByScript(String key, String value) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null) {
                Object result = jedis.eval(DEL_IFEXIST_SCRIPT, 2, key, value);
                return result == null ? false : (Integer.valueOf(result.toString()) != 0);
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * 仅当存在同名key并且value也一致时设置key
     *
     * @param key      键名
     * @param oldValue 原键值
     * @param newValue 新键值
     * @return 成功返回true, 失败false
     */
    public static boolean setIfExiseByScript(String key, String oldValue, String newValue) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null) {
                Object result = jedis.eval(SET_IFEXIST_SCRIPT, 3, key, oldValue, newValue);
                return result == null ? false : ("OK".equals(result));
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }


    /**
     * 设置新值并返回旧值
     *
     * @param key   键名
     * @param value 新值
     * @return 成功返回旧值, 失败返回null
     */
    public static String getSet(String key, String value) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.getSet(key, value) : null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }


    /**
     * 有序集合（Zsort）
     * 向有序集合添加一个或多个成员，或者更新已存在成员的分数
     *
     * @param key
     * @param score
     * @param member
     */
    public static void zadd(String key, double score, String member) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null) {
                jedis.zadd(key, score, member);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * 有序集合（Zsort）
     * 返回有序集中指定成员的排名 无成员返回null
     *
     * @param key
     * @param member
     */
    public static Long zrank(String key, String member) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null) {
                jedis.zrank(key, member);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return null;
    }

    /**
     * 有序集合（Zsort）
     * 返回有序集中，成员的分数值 无成员返回null
     *
     * @param key
     * @param member
     */
    public static Double zscore(String key, String member) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null) {
                return jedis.zscore(key, member);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return null;
    }

    /**
     * 有序集合（Zsort）
     * 更新元素 score += score
     *
     * @param key
     * @param score
     * @param member
     */
    public static void zincrby(String key, double score, String member) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null) {
                jedis.zincrby(key, score, member);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }


    /**
     * 有序集合（Zsort）
     * 获取前几名 从大到小排序 如前两名 start = 0 end =1
     *
     * @param key
     * @param start
     * @param end
     */
    public static Set<Tuple> zrevrangeWithScores(String key, long start, long end) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null) {
                return jedis.zrevrangeWithScores(key, start, end);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return null;
    }

    /**
     * 有序集合（Zsort）
     * 获取前几名 从大到小排序 如前两名 start = 0 end =1
     *
     * @param key
     * @param start
     * @param end
     */
    public static Set<String> zrevrange(String key, long start, long end) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null) {
                return jedis.zrevrange(key, start, end);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return null;
    }

    /**
     * 有序集合（Zsort）
     * 获取有序集合的成员数
     *
     * @param key
     */
    public static Long zcard(String key) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null) {
                return jedis.zcard(key);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return null;
    }

    /**
     * 有序集合（Zsort）
     * 获取前几名 从小到大排序 如前两名 start = 0 end =1
     *
     * @param key
     * @param start
     * @param end
     */
    public static Set<String> zrange(String key, long start, long end) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null) {
                return jedis.zrange(key, start, end);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return null;
    }


    /**
     * 有序集合（Zsort）
     * 移除有序集合中的一个或多个成员
     *
     * @param key
     * @param members
     */
    public static void zrem(String key, String... members) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null) {
                jedis.zrem(key, members);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }


    /**
     * 将哈希表 key 中的域 field 的值设为 value 。
     * 如果 key 不存在，一个新的哈希表被创建并进行 HSET 操作。
     * 如果域 field 已经存在于哈希表中，旧值将被覆盖
     * <p>
     * 如果 field 是哈希表中的一个新建域，并且值设置成功，返回 1 。
     * 如果哈希表中域 field 已经存在且旧值已被新值覆盖，返回 0
     *
     * @param key
     * @param field
     * @param value
     */
    public static Long hset(String key, String field, String value) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null) {
                return jedis.hset(key, field, value);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return 0L;
    }

    /**
     * 返回哈希表 key 中给定域 field 的值
     *
     * @param key
     * @param field
     */
    public static String hget(String key, String field) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null) {
                return jedis.hget(key, field);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return null;
    }

    /**
     * 返回哈希表 key 中给定域 field 的值
     *
     * @param key
     * @param field
     */
    public static Long hsetnx(String key, String field, String value) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null) {
                return jedis.hsetnx(key, field, value);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return 0L;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return 0L;
    }

    /**
     * 返回哈希表 key 中给定域 field 是否存在
     *
     * @param key
     * @param field
     */
    public static boolean hexists(String key, String field) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null) {
                return jedis.hexists(key, field);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return false;
    }


    /**
     * 删除哈希表 key 中给定域 field
     *
     * @param key
     * @param field
     */
    public static Long hdel(String key, String field) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null) {
                return jedis.hdel(key, field);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return 0L;
    }


    /**
     * 删除哈希表 key 中给定域 field
     *
     * @param key
     */
    public static List<String> hvals(String key) {
        Jedis jedis = RedisPool.getJedis();
        try {
            if (jedis != null) {
                return jedis.hvals(key);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return null;
    }

    /**
     * 添加指定元素到 HyperLogLog 中
     *
     * @param key
     * @return null = 添加失败（异常）；1=添加成功；0=添加失败（已存在）
     */
    public static Long pfadd(String key, String... elements) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.pfadd(key, elements) : null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            close(jedis);
        }
    }

    /**
     * 返回给定的 HyperLogLog 的基数估算值
     *
     * @param keys key1, key2, key3 ...
     * @return null = 返回失败（异常）；
     */
    public static Long pfcount(String... keys) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.pfcount(keys) : null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            close(jedis);
        }
    }

    /**
     * 合并给定的 HyperLogLog 的基数估算值
     *
     * @param destkey    目标
     * @param sourcekeys 源
     * @return null = 返回失败（异常）；OK=合并成功
     */
    public static String pfmerge(String destkey, String... sourcekeys) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.pfmerge(destkey, sourcekeys) : null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            close(jedis);
        }
    }


    /**
     * 位图
     *
     * @param key    目标
     * @param offset 索引位置
     * @param value  值；
     * @return 设置前该位置的原始值 null = 返回失败（异常）；
     */
    public static Boolean setbit(String key, long offset, boolean value) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.setbit(key, offset, value) : null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            close(jedis);
        }
    }


    /**
     * 关闭 jedis
     *
     * @param jedis
     */
    public static void close(Jedis jedis) {
        try {
            if (jedis != null) {
                jedis.close();
            }
        } catch (Exception e) {
        }

    }


    /**
     * @param key
     * @param count 返回元素个数
     * @return 存储的元素集合
     */
    public static List<String> sRandMember(String key, int count) {
        Jedis jedis = RedisPool.getJedis();
        try {
            return jedis != null ? jedis.srandmember(key, count) : null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            close(jedis);
        }
    }


    public static void main(String[] args) {
        System.out.println(get("aa"));
    }
}
