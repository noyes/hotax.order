package liqiang365.util.redis;//package com.liqiang365.util.redis;


import org.redisson.Redisson;
import org.redisson.api.RAtomicLong;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

import java.util.concurrent.TimeUnit;

/**
 * Created by java on 2017/11/14.
 */
public class RedissonManager {
    private static final String RAtomicName = "genId_";
    private static final String LOCK_TITLE = "redisLock_";
    private static Config config = new Config();
    private static RedissonClient redisson = null;


    public static void init() {
        try {
            config.useSingleServer().setAddress("http://" + RedisConfig.REDIS_HOST_IP + ":" + RedisConfig.REDIS_HOST_PORT)
                    .setIdleConnectionTimeout(RedisConfig.REDIS_MAX_IDLE)
                    .setConnectionPoolSize(RedisConfig.REDIS_MAX_IDLE)
                    .setConnectTimeout(RedisConfig.REDIS_MAX_WAIT_MILLS)
                    .setPingConnection(RedisConfig.REDIS_TEST_ON_BORROW);
                    redisson = Redisson.create(config);
            //清空自增的ID数字
            RAtomicLong atomicLong = redisson.getAtomicLong(RAtomicName);
            atomicLong.set(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static RedissonClient getRedisson(){
        return redisson;
    }

    public static Long nextID(){
        RAtomicLong atomicLong = getRedisson().getAtomicLong(RAtomicName);
        atomicLong.incrementAndGet();
        return atomicLong.get();
    }

    public static RLock lock(String lockName, long timeout, TimeUnit timeUnit){
        if(redisson ==null){
            init();
        }
        String key = LOCK_TITLE + lockName;
        RLock mylock = redisson.getLock(key);
        mylock.lock(timeout, timeUnit); //lock提供带timeout参数，timeout结束强制解锁，防止死锁
        System.err.println("======lock======"+Thread.currentThread().getName());
        return mylock;
    }

    public static RLock unlock(String lockName){
        if(redisson ==null){
            init();
        }
        String key = LOCK_TITLE + lockName;
        RLock mylock = redisson.getLock(key);
        mylock.unlock();
        System.err.println("======unlock======"+Thread.currentThread().getName());
        return mylock;
    }


}
