package liqiang365.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.liqiang365.dao.BaseMapper;
import com.liqiang365.model.entity.BaseEntity;
import com.liqiang365.util.CommonUtil;
import com.liqiang365.util.date.DateUtil;
import com.liqiang365.util.lang.UUIDUtils;
import com.liqiang365.util.valid.ValidUtil;
import hotax.core.ReturnBean;
import hotax.core.ReturnMap;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * The class Base service.
 *
 * @param <T> the type parameter
 */
public abstract class TkBaseService<T> implements IService<T> {

    /**
     * The Logger.
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * The Mapper.
     */
    @Autowired
    protected BaseMapper<T> mapper;


    /**
     * Gets mapper.
     *
     * @return the mapper
     */
    public BaseMapper<T> getMapper() {
        return mapper;
    }

    /**
     * Select list.
     *
     * @param record the record
     * @return the list
     */
    @Override
    public List<T> select(T record) {
        return mapper.select(record);
    }

    /**
     * Select by key t.
     *
     * @param key the key
     * @return the t
     */
    @Override
    public T selectByKey(Object key) {
        return mapper.selectByPrimaryKey(key);
    }

    /**
     * Select all list.
     *
     * @return the list
     */
    @Override
    public List<T> selectAll() {
        return mapper.selectAll();
    }

    /**
     * Select one t.
     *
     * @param record the record
     * @return the t
     */
    @Override
    public T selectOne(T record) {
        return mapper.selectOne(record);
    }

    /**
     * Select count int.
     *
     * @param record the record
     * @return the int
     */
    @Override
    public int selectCount(T record) {
        return mapper.selectCount(record);
    }

    /**
     * Select by example list.
     *
     * @param example the example
     * @return the list
     */
    @Override
    public List<T> selectByExample(Object example) {
        return mapper.selectByExample(example);
    }

    /**
     * Save int.保存一个实体，null的属性不会保存，会使用数据库默认值
     *
     * @param record the record
     * @return the int
     */
    @Override
    public int save(T record) {
        return mapper.insertSelective(record);
    }

    /**
     * Batch save int.
     *
     * @param list the list
     * @return the int
     */
    @Override
    public int batchSave(List<T> list) {
        return mapper.insertBatch(list);
    }

    /**
     * Update int.
     *
     * @param entity the entity
     * @return the int
     */
    @Override
    public int update(T entity) {
        return mapper.updateByPrimaryKeySelective(entity);
    }

    /**
     * Delete int.
     *
     * @param record the record
     * @return the int
     */
    @Override
    public int delete(T record) {
        return mapper.delete(record);
    }

    /**
     * Delete by key int.
     *
     * @param key the key
     * @return the int
     */
    @Override
    public int deleteByKey(Object key) {
        return mapper.deleteByPrimaryKey(key);
    }

    /**
     * Batch delete int.
     *
     * @param list the list
     * @return the int
     */
    @Override
    public int batchDelete(List<T> list) {
        int result = 0;
        for (T record : list) {
            int count = mapper.delete(record);
            if (count < 1) {
                logger.error("删除数据失败");
            }
            result += count;
        }
        return result;
    }

    /**
     * Select count by example int.
     *
     * @param example the example
     * @return the int
     */
    @Override
    public int selectCountByExample(Object example) {
        return mapper.selectCountByExample(example);
    }

    /**
     * Update by example int.
     *
     * @param record  the record
     * @param example the example
     * @return the int
     */
    @Override
    public int updateByExample(T record, Object example) {
        return mapper.updateByExampleSelective(record, example);
    }

    /**
     * Delete by example int.
     *
     * @param example the example
     * @return the int
     */
    @Override
    public int deleteByExample(Object example) {
        return mapper.deleteByPrimaryKey(example);
    }

    /**
     * Select by row bounds list.
     *
     * @param record    the record
     * @param rowBounds the row bounds
     * @return the list
     */
    @Override
    public List<T> selectByRowBounds(T record, RowBounds rowBounds) {
        return mapper.selectByRowBounds(record, rowBounds);
    }

    /**
     * Select by example and row bounds list.
     *
     * @param example   the example
     * @param rowBounds the row bounds
     * @return the list
     */
    @Override
    public List<T> selectByExampleAndRowBounds(Object example, RowBounds rowBounds) {
        return mapper.selectByExampleAndRowBounds(example, rowBounds);
    }

    public String getTid(){
        return CommonUtil.getTid();
    }

    @Override
    public ReturnBean insert(T t) {
        BaseEntity baseEntity = (BaseEntity)t;
        baseEntity.setCreateTime(DateUtil.getTime());
        baseEntity.setUpdateTime(DateUtil.getTime());
        baseEntity.setId(UUIDUtils.get32UUID());
        baseEntity.setTid(getTid());
        int count = mapper.insertSelective(t);
        if(count>0){
            return ReturnMap.returnSucc("操作成功");
        }
        return ReturnMap.returnFail("操作失败");
    }
    @Override
    public ReturnBean get(String key){
        return ReturnMap.returnSucc(mapper.selectByPrimaryKey(key));
    }
    @Override
    public ReturnBean remove(String key){
        T t = mapper.selectByPrimaryKey(key);
        if(null != t){
            BaseEntity baseEntity = (BaseEntity)t;
            baseEntity.setDeleteState("1");
            baseEntity.setUpdateTime(DateUtil.getTime());
            int count = mapper.updateByPrimaryKeySelective(t);
            if(count>0) {
                return ReturnMap.returnSucc("操作成功");
            }
        }
        return ReturnMap.returnFail("操作失败");
    }
    @Override
    public ReturnBean list(T t,Integer pageNum,Integer pageSize){
        Example example = new Example(t.getClass());
        example.createCriteria().andEqualTo("deleteState","0")
        .andEqualTo(t);
        if(t.toString().indexOf(", seq=")>-1){
            example.orderBy("seq");
        }
        example.orderBy("createTime").desc();

        if(ValidUtil.isNotEmpty(pageNum)&&
            ValidUtil.isNotEmpty(pageSize)){
            PageHelper.startPage(pageNum,pageSize);
            return ReturnMap.returnSucc(new PageInfo<>(mapper.selectByExample(example)));
        }
        return ReturnMap.returnSucc(mapper.selectByExample(example));
    }

}
